/* 
 * JS used to handle form validation & requests for user signup
 * @created on   23/11/2014
 */
$(document).ready(function(){
   /**
    * Form validation
    */
   $('#signupForm').validate({
      rules:{
        username:{
            required:true,
            maxlength:15,
            minlength:3
        },
        user_email:{
            required:true,
            email:true
        },
        password:{
            required:true,
            maxlength:15,
            minlength:3
        },
        cnfrm_password:{
            required:true,
            equalTo: "#password"
        }
      },
      messages:{
        username:{
            required:"Username is required",
            maxlength:"Maximum length is 15 characters",
            minlength:"Minimum length is 3 characters"
        },
        user_email:{
            required:"Email is required",
            email:"Enter a valid email"
        },
        password:{
            required:"Password is required",
            maxlength:"Maximum length is 15 characters",
            minlength:"Minimum length is 3 characters"
        },
        cnfrm_password:{
            required:"Confirm Password is required",
            equalTo: "Must be same as Password"
        }  
      },
      submitHandler:function(form){
          var str = $('#signupForm').serialize();
          $.post(APP_URL+"signup", str, function(response){
              $('#err_username, #head_msg').empty();
              if(response.status == 200){
                  $('#head_msg').html("<div class='alert alert-success fade in'>\n\
                        <strong>"+response.message+"</strong></div>");
                  $(':input', '#signupForm')
                                    .not(':button, :submit, :reset, :hidden')
                                    .val('')
                                    .removeAttr('checked')
                                    .removeAttr('selected');
              }else if(response.status == 400){
                  $('#err_username').html(response.error.username);
                  $('#head_msg').html("<div class='alert alert-danger fade in'>\n\
                        <strong>Please Correct errors !</strong></div>");
              }else{
                 $('#head_msg').html("<div class='alert alert-danger fade in'>\n\
                        <strong>"+response.message+"</strong></div>"); 
              }
          }, 'json');
      }
   });
});


