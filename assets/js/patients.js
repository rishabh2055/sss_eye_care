/* 
 * JS used to handle form validation & requests for patients list page
 * @created on   23/11/2014
 */
$(document).ready(function () {
    $("#appt_date").datepicker({
        changeMonth: true,
        changeYear: true,
        dateFormat: 'yy-mm-dd',
        yearRange: '-100:+0'
    });
    $('#patient_form').hide();
    /**
     * Click on edit button to edit patient details
     */
    $('.edit-patient').on('click', function () {
        patientId = $(this).attr('name');
        //send ajax request to fetch patient details
        $.post('patients', {patientId: patientId}, function (response) {
            if (response.status == 200) {
                var patient = response.patients[0];
                $('#patient_name').val(patient.name);
                if (patient.gender == 'male') {
                    $('input[name=gender]#male').prop('checked', true);
                } else if (patient.gender == 'female') {
                    $('input[name=gender]#female').prop('checked', true);
                }
                $('#complaints_duration_month').val(patient.complaint_month);
                $('#complaints_duration_year').val(patient.complaint_year);
                if (patient.highbp == 'yes') {
                    $('input[name=highbp]#yes_highbp').prop('checked', true);
                } else if (patient.highbp == 'no') {
                    $('input[name=highbp]#no_highbp').prop('checked', true);
                }
                if (patient.lowbp == 'yes') {
                    $('input[name=lowbp]#yes_lowbp').prop('checked', true);
                } else if (patient.lowbp == 'no') {
                    $('input[name=lowbp]#no_lowbp').prop('checked', true);
                }
                $('#patient_age').val(patient.age);
                if (patient.complaints == 'Left Eye') {
                    $('input[name=complaints]#left_eye').prop('checked', true);
                } else if (patient.complaints == 'Right Eye') {
                    $('input[name=complaints]#right_eye').prop('checked', true);
                }
                if (patient.diabities == 'yes') {
                    $('input[name=diabities]#yes_diabities').prop('checked', true);
                    $('#diabities_duration_section').show();
                    $('#diabities_duration_month').val(patient.diabities_month);
                    $('#diabities_duration_year').val(patient.diabities_year);
                } else if (patient.diabities == 'no') {
                    $('input[name=diabities]#no_diabities').prop('checked', true);
                    $('#diabetes_duration_section').hide();
                }
                if (patient.complaints == 'Left Eye') {
                    $('input[name=complaints]#left_eye').prop('checked', true);
                } else if (patient.complaints == 'Right Eye') {
                    $('input[name=complaints]#right_eye').prop('checked', true);
                }
                if (patient.complaints == 'Left Eye') {
                    $('input[name=complaints]#left_eye').prop('checked', true);
                } else if (patient.complaints == 'Right Eye') {
                    $('input[name=complaints]#right_eye').prop('checked', true);
                }
                $('#patient_table').hide();
                $('#patient_form').show();
                if (patient.complaints == 'Left Eye') {
                    $('input[name=complaints]#left_eye').prop('checked', true);
                } else if (patient.complaints == 'Right Eye') {
                    $('input[name=complaints]#right_eye').prop('checked', true);
                }
                if (patient.typhoid == 'yes') {
                    $('input[name=typhoid]#yes_typhoid').prop('checked', true);
                } else if (patient.typhoid == 'no') {
                    $('input[name=typhoid]#no_typhoid').prop('checked', true);
                }
                $('#doctor_remarks').val(patient.doctor_remarks);
                $('#appt_date').val(patient.appt_date);
                $('#patient_phone').val(patient.phone);
                $('#patient_email').val(patient.email);
                if (patient.appt_time != null) {
                    if (patient.appt_time == '1') {
                        $('input[name=appt_time]#morning').prop('checked', true);
                    } else if (patient.typhoid == '3') {
                        $('input[name=appt_time]#evening').prop('checked', true);
                    }
                }
                $('#patientId').val(patient.id);
                $('#patient_table').hide();
                $('#patient_form').show();
            } else {

            }
        }, 'json');
    });

    //---------------------------------------------------------------------------
    /**
     * Click on back button
     */
    $('.back-patient-table').on('click', function () {
        $('#patient_form').hide();
        $('#patient_table').show();
    });

    //--------------------------------------------------------------------------
    /**
     * Form validation
     */
    $('#patientForm').validate({
        rules: {
            patient_name: {
                required: true,
                maxlength: 30,
                minlength: 3
            },
            patient_age: {
                required: true
            },
            patient_phone: {
                required: true,
                number: true,
                minlength: 10,
                maxlength: 15
            },
            patient_email: {
                email: true
            },
            complaints: {
                required: true
            },
            diabities: {
                required: true
            },
            highbp: {
                required: true
            },
            lowbp: {
                required: true
            },
            gender: {
                required: true
            },
            typhoid: {
                required: true
            },
            doctor_remarks: {
                maxlength: 500
            }
        },
        messages: {
            patient_name: {
                required: "Patient Name is required",
                maxlength: "Maximum length is 30 characters",
                minlength: "Minimum length is 3 characters"
            },
            patient_age: {
                required: "Patient Age is required"
            },
            patient_phone: {
                required: "Patient Phone is required",
                number: "Enter numbers only",
                minlength: "No more than 10 digits",
                maxlength: "Maximum length is 15 digits"
            },
            patient_email: {
                email: "Enter a valid email"
            },
            complaints: {
                required: "Patient Complaints is required"
            },
            diabetes: {
                required: "Diabities is required"
            },
            highbp: {
                required: "High Blood Pressure is required"
            },
            lowbp: {
                required: "Low Blood Pressure is required"
            },
            gender: {
                required: "Patient Gender is required"
            },
            typhoid: {
                required: "Typhoid Problems is required"
            },
            doctor_remarks: {
                maxlength: "Maximum length is 500 characters"
            }
        },
        submitHandler: function (form) {
            var appt_date = $('#appt_date').val();
            var appt_time = $('input[name=appt_time]:checked').val();
            if (appt_date != '' && appt_time == undefined) {
                alert('here');
                $('#time_error').html("Appointment Time is required").show();
                return false;
            } else {
                var str = $('#patientForm').serialize();
                $.post(APP_URL + "patient_details", str, function (response) {
                    $('#head_msg').empty();
                    if (response.status == 200) {
                        var months = ($('#complaints_duration_month').val() != 0) ? $('#complaints_duration_month').val() + ' Months' : '';
                        var year = ($('#complaints_duration_year').val() != 0) ? $('#complaints_duration_year').val() + ' Years' : '';
                        $('.edit-patient[name=' + $("#patientId").val() + ']').closest('tr').find('td:eq(1)').text($('#patient_name').val());
                        $('.edit-patient[name=' + $("#patientId").val() + ']').closest('tr').find('td:eq(2)').text($('#patient_age').val());
                        $('.edit-patient[name=' + $("#patientId").val() + ']').closest('tr').find('td:eq(3)').text(capitalize($('input[name=gender]:checked').val()));
                        $('.edit-patient[name=' + $("#patientId").val() + ']').closest('tr').find('td:eq(4)').text(capitalize($('input[name=complaints]:checked').val().split('_')[0]) + ' ' + capitalize($('input[name=complaints]:checked').val().split('_')[1]));
                        $('.edit-patient[name=' + $("#patientId").val() + ']').closest('tr').find('td:eq(5)').text(months + ' ' + year);
                        $('.edit-patient[name=' + $("#patientId").val() + ']').closest('tr').find('td:eq(6)').text(capitalize($('input[name=diabities]:checked').val()));
                        $('#head_msg').html("<div class='alert alert-success fade in'>\n\
                        <strong>Patient Details Updated Successfully !</strong></div>");
//                        $(':input', '#patientForm')
//                                .not(':button, :submit, :reset, :hidden')
//                                .val('')
//                                .removeAttr('checked')
//                                .removeAttr('selected');
                        $('#patient_form').hide();
                        $('#patient_table').show();
                    } else {
                        $('#head_msg').html("<div class='alert alert-danger fade in'>\n\
                        <strong>" + response.message + "</strong></div>");
                    }
                }, 'json');
            }

        }
    });

    //--------------------------------------------------------------------------
    /**
     * Function used to return first letter capital of a text
     */
    function capitalize(s)
    {
        return s.charAt(0).toUpperCase() + s.slice(1);
    }

    //--------------------------------------------------------------------------
    /**
     * Change in diabeties radio button
     */
    $('input[name=diabities]').on('change', function () {
        alert($(this).val());
        if ($(this).val() == 'yes') {
            $('#diabities_duration_section').show();
        } else {
            $('#diabities_duration_section').hide();
            $('#diabities_duration_month').val(0);
            $('#diabities_duration_year').val(0);
        }
    });

    //--------------------------------------------------------------------------
    /**
     * Click on delete button to delete patient details
     */
    $('.delete-patient').on('click', function () {
        patientId = $(this).attr('name');
        //send ajax request to fetch patient details
        $.post('delete_patient', {patientId: patientId}, function (response) {
            if (response.status == 200) {
                var i = 1;
                $('#patientDetailsTable').empty();
                $.each(response.patients, function(key, value){
                   $('#patientDetailsTable').append('<tr><td class="text-center">'+i+'</td>\n\
                                <td class="text-center">'+value['name']+'</td>\n\
                                <td class="text-center">'+value['age']+'</td>\n\
                                <td class="text-center">'+capitalize(value['gender'])+'</td>\n\
                                <td class="text-center">'+value['complaints']+'</td>\n\
                                <td class="text-center">'+value['complain_duration']+'</td>\n\
                                <td class="text-center">'+capitalize(value['diabities'])+'</td>\n\
                                <td class="text-center"><a href="javascript:void(0)" name='+value['id']+' class="btn btn-success btn-xs edit-patient">Edit</a>\n\
                                <a href="javascript:void(0)" name='+value['id']+' class="btn btn-default btn-xs delete-patient">Delete</a>\n\
                                </td></tr>'); 
                });
            } else {

            }
        }, 'json');
    });

    //--------------------------------------------------------------------------
    /**
     * Change in Appointment Date
     */
    $('#appt_date').on('change', function () {
        var appt_date = $(this).val();
        var appt_time = $('input[name=appt_time]:checked').val();
        if (appt_date != '' && appt_time == undefined) {
            $('#time_error').html("Appointment Time is required");
        } else {
            $('#time_error').empty();
        }
    });

    //--------------------------------------------------------------------------
    /**
     * Change in appointment time
     */
    $('input[name=appt_time]').on('change', function () {
        $('#time_error').empty();
    });
});