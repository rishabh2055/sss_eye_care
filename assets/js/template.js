jQuery(document).ready(function ($) {

    $(".headroom").headroom({
        "tolerance": 20,
        "offset": 50,
        "classes": {
            "initial": "animated",
            "pinned": "slideDown",
            "unpinned": "slideUp"
        }
    });

    $('.carousel').carousel();

    //--------------------------------------------------------------------------
    /**
     * Mousehover on services name t show service related image
     * @returns {undefined}
     */
    var moveDown = 0;
    $('.diseases-ul li').hover(function (e) {

        var target = '#' + ($(this).attr('data-popbox'));
        var image = $(this).find('a').attr('value');
        $('#pop1 img').attr('src', 'assets/images/diseases/'+image+'.jpg');
        $(target).show();
        moveDown = ($(target).outerHeight() / 2);
    }, function () {
        var target = '#' + ($(this).attr('data-popbox'));
        if (!($(".diseases-ul li").hasClass("show"))) {
            $(target).hide();
        }
    });

    $('.diseases-ul li').mousemove(function (e) {
        var target = '#' + ($(this).attr('data-popbox'));

        topD = e.pageY - parseInt(moveDown);
        maxBottom = parseInt(e.pageY + parseInt(moveDown) + 20);
        windowBottom = parseInt(parseInt($(document).scrollTop()) + parseInt($(window).height()));
        maxTop = topD;
        windowTop = parseInt($(document).scrollTop());
        if (maxBottom > windowBottom) {
            topD = windowBottom - $(target).outerHeight() - 20;
        } else if (maxTop < windowTop) {
            topD = windowTop + 20;
        }

        $(target).css('top', topD).css('left', '420px');
    });
//    $('.diseases-ul li').click(function (e) {
//        var target = '#' + ($(this).attr('data-popbox'));
//        if (!($(this).hasClass("show"))) {
//            $(target).show();
//        }
//        $(this).toggleClass("show");
//    });



});