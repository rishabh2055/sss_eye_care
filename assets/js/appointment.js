/* 
 * JS used to handle form validation & requests for user appointments
 * @created on   23/11/2014
 */
$(document).ready(function () {
    $("#appt_date").datepicker({
        changeMonth: true,
        changeYear: true,
        dateFormat: 'yy-mm-dd',
        yearRange: '-100:+0'
    });
    /**
     * Form validation
     */
    $('#makeAppointment').validate({
        rules: {
            full_name: {
                required: true,
                maxlength: 30,
                minlength: 3
            },
            user_email: {
                required: true,
                email: true
            },
            user_mobile: {
                required: true,
                number: true,
                maxlength: 15,
                minlength: 10
            },
            appt_date: {
                required: true
            },
            appt_time: {
                required: true
            }
        },
        messages: {
            full_name: {
                required: "Type your Name",
                maxlength: "Maximum length is 30 characters",
                minlength: "Minimum length is 3 characters"
            },
            user_email: {
                required: "Type your Email",
                email: "Enter a valid email"
            },
            user_mobile: {
                required: "Type your Mobile No.",
                number: "Enter numbers only",
                maxlength: "Maximum length is 15 characters",
                minlength: "Minimum length is 3 characters"
            },
            appt_date: {
                required: "Select an Appointment Date"
            },
            appt_time: {
                required: "Select an Appointment Time"
            }
        },
        submitHandler: function (form) {
            var str = $('#makeAppointment').serialize();
            $.post(APP_URL + "make_appointment", str, function (response) {
                $('#err_username, #head_msg').empty();
                if (response.status == 200) {
                    $('#head_msg').html("<div class='alert alert-success fade in'>\n\
                        <strong>Appointment Details added successfully !</strong></div>");
                    $(':input', '#makeAppointment')
                            .not(':button, :submit, :reset, :hidden')
                            .val('')
                            .removeAttr('checked')
                            .removeAttr('selected');
                } else {
                    $('#head_msg').html("<div class='alert alert-danger fade in'>\n\
                        <strong>" + response.message + "</strong></div>");
                }
            }, 'json');
        }
    });

    //---------------------------------------------------------------------------
    /**
     * Click on send message button button to send message for appointment details
     */
    $('.send-message').on('click', function () {
        var mobile_info = $(this).attr('value');
        id = $(this).attr('name');
        var mobile_array = mobile_info.split(',');
        patient_type = mobile_array[1];
        var appt_date = $(this).closest('tr').find('td:eq(3)').text();
        var appt_time = $(this).closest('tr').find('td:eq(4)').text();
        $('#message_to').val(mobile_array[0]).attr('readonly', 'readonly');
        $('#message').val("Dear Patient, Your next appointment will be due on "+appt_date+" between "+appt_time+"\n\n From Sharan Eye Care, Allahabad");
        if ($(this).closest('tr').find('td:eq(6)').text() == 'Sent') {
            var confirm = window.confirm("Message already sent to this patient. Are you sure you want to send message again !!");
            if (confirm) {         
                $('#messagePopup').modal();
            }
        } else {
            $('#messagePopup').modal();
        }

    });

    //--------------------------------------------------------------------------
    $('#sendMessageForm').validate({
        submitHandler: function (form) {
            var mobile = $('#message_to').val();
            var message = $('#message').val();
            // send request to send message to patient
            $.post(APP_URL + 'send_message', {
                mobile: mobile,
                patient_type: patient_type,
                id: id,
                message: message
            }, function (response) {
                $('#messagePopup').modal('hide');
                if (response.status == 1701) {
                    $('#head_msg').html("<div class='alert alert-success fade in'>\n\
                        <strong>Message is successfully delivered !</strong></div>");
                } else if (response.status == 1025) {
                    $('#head_msg').html("<div class='alert alert-danger fade in'>\n\
                        <strong>Insufficient Credit ! Please Purchase messages.</strong></div>");
                } else if (response.status == 1705) {
                    $('#head_msg').html("<div class='alert alert-danger fade in'>\n\
                        <strong>Plesae Check your message !</strong></div>");
                } else if (response.status == 1706) {
                    $('#head_msg').html("<div class='alert alert-danger fade in'>\n\
                        <strong>Mobile no is invalid !</strong></div>");
                } else {
                    $('#head_msg').html("<div class='alert alert-danger fade in'>\n\
                        <strong>Server Error ! Try again later.</strong></div>");
                }
            }, 'json');
        }
    });
});


