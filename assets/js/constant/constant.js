/* 
 * JS used to define application base url to handle ajax request.
 * @created on 24/09/2014
 */
var hostname = window.location.hostname;
if(hostname == 'localhost'){
    var APP_URL = 'http://localhost/sss_eye_clinic/';
}else{
    var APP_URL = 'http://'+hostname+'/';
}


