/* 
 * JS used to handle form validation & requests for patient details
 * @created on   23/11/2014
 */
$(document).ready(function(){
    $('#patient_name').focus();
    $('#diabetes_duration_section').hide();
    /**
     * Change in diabeties radio button
     */
    $('input[name=diabetes]').on('change', function(){
       if($(this).val() == 'yes'){
         $('#diabetes_duration_section').show();  
       }else{
           $('#diabetes_duration_section').hide();
       } 
    });
   /**
    * Form validation
    */
   $('#patientForm').validate({
      rules:{
        patient_name:{
            required:true,
            maxlength:30,
            minlength:3
        },
        patient_age:{
            number:true,
            required:true,
            maxlength:3
        },
        patient_phone:{
            required:true,
            number:true,
            minlength:10,
            maxlength:15
        },
        patient_email:{
            email:true
        },
        complaints:{
            required:true
        },
        diabities:{
            required:true
        },
        highbp:{
            required:true
        },
        lowbp:{
            required:true
        },
        gender:{
            required:true
        },
        typhoid:{
            required:true
        }
      },
      messages:{
        patient_name:{
            required:"Patient Name is required",
            maxlength:"Maximum length is 30 characters",
            minlength:"Minimum length is 3 characters"
        },
        patient_age:{
            number: "Enter numbers only",
            required:"Patient Age is required",
            maxlength:"Maximum length is 3 digits"
        },
        patient_phone:{
            required:"Patient Phone is required",
            number:"Enter numbers only",
            minlength:"No more than 10 digits",
            maxlength:"Maximum length is 15 digits"
        },
        patient_email:{
            email:"Enter a valid email"
        },
        complaints:{
            required:"Patient Complaints is required"
        },
        diabetes:{
            required:"Diabities is required"
        },
        highbp:{
            required:"High Blood Pressure is required"
        } ,
        lowbp:{
            required:"Low Blood Pressure is required"
        } ,
        gender:{
            required:"Patient Gender is required"
        },
        typhoid:{
            required:"Typhoid Problems is required"
        } 
      },
      submitHandler:function(form){
          var str = $('#patientForm').serialize();
          $.post(APP_URL+"patient_details", str, function(response){
              $('#head_msg').empty();
              if(response.status == 200){
                  $('#head_msg').html("<div class='alert alert-success fade in'>\n\
                        <strong>"+response.message+"</strong></div>");
                  $(':input', '#patientForm')
                                    .not(':button, :submit, :reset, :hidden')
                                    .val('')
                                    .removeAttr('checked')
                                    .removeAttr('selected');
              }else{
                 $('#head_msg').html("<div class='alert alert-danger fade in'>\n\
                        <strong>"+response.message+"</strong></div>"); 
              }
          }, 'json');
      }
   });
});


