-- phpMyAdmin SQL Dump
-- version 4.2.7.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Dec 08, 2014 at 06:44 PM
-- Server version: 5.6.20
-- PHP Version: 5.5.15

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `sss_eye_care`
--

-- --------------------------------------------------------

--
-- Table structure for table `appointment`
--

CREATE TABLE IF NOT EXISTS `appointment` (
`id` int(10) unsigned NOT NULL,
  `full_name` varchar(120) COLLATE utf8_unicode_ci NOT NULL,
  `user_email` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `user_mobile` varchar(15) COLLATE utf8_unicode_ci NOT NULL,
  `appointment_date` date NOT NULL,
  `FK_doctor_time_id` int(10) unsigned DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=3 ;

--
-- Dumping data for table `appointment`
--

INSERT INTO `appointment` (`id`, `full_name`, `user_email`, `user_mobile`, `appointment_date`, `FK_doctor_time_id`, `created_at`, `updated_at`) VALUES
(2, 'Abhishek Singh', 'abhi.singh@gmail.com', '1234567897', '2014-12-10', 1, '2014-12-08 12:06:42', '2014-12-08 12:06:42');

-- --------------------------------------------------------

--
-- Table structure for table `doctor_time`
--

CREATE TABLE IF NOT EXISTS `doctor_time` (
`id` int(10) unsigned NOT NULL,
  `time_between` varchar(150) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=4 ;

--
-- Dumping data for table `doctor_time`
--

INSERT INTO `doctor_time` (`id`, `time_between`, `created_at`, `updated_at`) VALUES
(1, '10 A.M. To 11 A.M.', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2, '12 P.M. To 1 P.M.', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3, '4 P.M. To 6 P.M.', '0000-00-00 00:00:00', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE IF NOT EXISTS `migrations` (
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`migration`, `batch`) VALUES
('2014_11_23_054719_create_users_table', 1),
('2014_11_26_163459_create_appointment_table', 2),
('2014_11_26_164514_create_doctor_time_table', 2),
('2014_11_26_165109_create_add_fk_to_appointment_time_table', 2),
('2014_11_27_165624_create_patient_details_table', 3);

-- --------------------------------------------------------

--
-- Table structure for table `patient_details`
--

CREATE TABLE IF NOT EXISTS `patient_details` (
`id` int(10) unsigned NOT NULL,
  `patient_name` varchar(150) COLLATE utf8_unicode_ci NOT NULL,
  `patient_age` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `patient_gender` enum('male','female') COLLATE utf8_unicode_ci DEFAULT NULL,
  `patient_phone` varchar(15) COLLATE utf8_unicode_ci NOT NULL,
  `patient_email` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `patient_complaints` enum('left_eye','right_eye') COLLATE utf8_unicode_ci DEFAULT NULL,
  `patient_complaint_duration_month` int(10) unsigned NOT NULL,
  `patient_complaint_duration_year` int(10) unsigned NOT NULL,
  `diabities` enum('yes','no') COLLATE utf8_unicode_ci DEFAULT NULL,
  `diabities_duration_month` int(10) unsigned NOT NULL,
  `diabities_duration_year` int(10) unsigned NOT NULL,
  `high_blood_pressure` enum('yes','no') COLLATE utf8_unicode_ci DEFAULT NULL,
  `low_blood_pressure` enum('yes','no') COLLATE utf8_unicode_ci DEFAULT NULL,
  `typhoid_problem` enum('yes','no') COLLATE utf8_unicode_ci DEFAULT NULL,
  `doctor_remarks` text COLLATE utf8_unicode_ci,
  `appt_date` date DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `FK_doctor_time_id` int(10) DEFAULT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=4 ;

--
-- Dumping data for table `patient_details`
--

INSERT INTO `patient_details` (`id`, `patient_name`, `patient_age`, `patient_gender`, `patient_phone`, `patient_email`, `patient_complaints`, `patient_complaint_duration_month`, `patient_complaint_duration_year`, `diabities`, `diabities_duration_month`, `diabities_duration_year`, `high_blood_pressure`, `low_blood_pressure`, `typhoid_problem`, `doctor_remarks`, `appt_date`, `created_at`, `updated_at`, `FK_doctor_time_id`) VALUES
(1, 'Aman Kumar Singh', '26', 'male', '3456789567', '', 'left_eye', 4, 2, 'no', 0, 0, 'no', 'no', 'yes', 'dfgfhdfhdfhdgjdjghjfgjhg', '2014-12-12', '2014-11-27 12:20:30', '2014-12-08 12:13:24', 3),
(2, 'Ramesh Singh', '25', 'male', '2342423435', '', 'left_eye', 4, 1, 'yes', 2, 0, 'yes', 'yes', 'yes', 'askjdh fadjfksdjjsgkdldfhgldfhgdf', '2014-12-14', '2014-12-03 12:15:09', '2014-12-08 12:10:51', 3),
(3, 'Ramesh Singh', '25', 'male', '3456789567', '', 'right_eye', 2, 0, 'yes', 3, 1, 'yes', 'yes', 'yes', 'sdgdgsfdfjdvyetytyertytry', '2014-12-11', '2014-12-04 11:17:48', '2014-12-08 12:09:49', 1);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
`id` int(10) unsigned NOT NULL,
  `username` varchar(15) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `status` enum('active','blocked','deleted','pending') COLLATE utf8_unicode_ci DEFAULT 'active',
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `remember_token` varchar(30) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=3 ;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `username`, `email`, `password`, `status`, `created_at`, `updated_at`, `remember_token`) VALUES
(1, 'admin', 'rishabh.chitranshi88@gmail.com', '$2y$10$o2xCXSf3qSgswsyM7j6cteUX7zu3wYiEzW3yeN4nsNxUoea16IVI2', 'active', '0000-00-00 00:00:00', '2014-12-08 11:59:16', 'nuCrezzZHSnogp3Aqv10LJBCdUM9SK'),
(2, 'admin1', 'rishabh.chitranshi88@gmail.com', '$2y$10$XkSKi4Z2bw6TndyQYkRzL.m0wSOUHcyIc5qHdcizJEEmqFykExTtm', 'active', '2014-11-24 11:44:16', '2014-11-24 13:01:33', 'jP9z4lojh5AkH0HNFoi1T1CI28OV85');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `appointment`
--
ALTER TABLE `appointment`
 ADD PRIMARY KEY (`id`), ADD KEY `appointment_fk_doctor_time_id_foreign` (`FK_doctor_time_id`);

--
-- Indexes for table `doctor_time`
--
ALTER TABLE `doctor_time`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `patient_details`
--
ALTER TABLE `patient_details`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
 ADD PRIMARY KEY (`id`), ADD UNIQUE KEY `users_username_unique` (`username`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `appointment`
--
ALTER TABLE `appointment`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `doctor_time`
--
ALTER TABLE `doctor_time`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `patient_details`
--
ALTER TABLE `patient_details`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `appointment`
--
ALTER TABLE `appointment`
ADD CONSTRAINT `appointment_fk_doctor_time_id_foreign` FOREIGN KEY (`FK_doctor_time_id`) REFERENCES `doctor_time` (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
