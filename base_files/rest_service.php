<?php

/**
 * Rest_service class
 * Class used to handle request and response to get 
 * unique ITS no from third party server API.
 * @version    0.0.1
 * @since      0.0.1
 * @accss      public
 */
class Rest_service {

    //----------------------------------------------------------    
    /**
     * Constructor Method
     * 
     * @since       0.0.1
     * @version     0.0.1
     */
    public function __construct() {
        $this->_request_type = $this->_check_request_method();
        $this->_call_requested_function();
    }

    //----------------------------------------------------------

    /*
     * Checks which method has been used to make the request.
     * 
     * @version     0.0.1
     * @since       0.0.1
     */
    function _check_request_method() {
        $method = strtolower($_SERVER['REQUEST_METHOD']);
        if (in_array($method, array('get', 'delete', 'post', 'put'))) {
            return $method;
        }
        return 'get';
    }

    //----------------------------------------------------------

    /**
     * 
     */
    function _call_requested_function() {
        #processing the arguments based on the type of request
        $_SERVER['REQUEST_URI_PATH'] = parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH);
        $segments = explode('/', $_SERVER['REQUEST_URI_PATH']);
        switch ($this->_request_type) {
            case 'get':
                #converting the passed arguments into an associative array               
                $data = $segments[4];
                break;
            case 'post':
                return 'Can not call this ';
                break;
            case 'put':
                $data = file_get_contents("php://input");
                $data = json_decode($data);
                break;
            case 'delete':
                $data = file_get_contents("php://input");
                $data = json_decode($data);
            default:
                break;
        }

        #inserting the data entered by the user in the POST array
        if (!empty($data)) {
            foreach ($data as $key => $value) {
                $_POST[$key] = $value;
            }
        }


        #reading the object name for which the call is made
        $object_name = $segments[3];
        $controller_method = $object_name . '_' . $this->_request_type;

        #calling the target function
        call_user_func(array($this, $controller_method), $arr_argument = '');
    }

}
