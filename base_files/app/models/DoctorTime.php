<?php

/*
 * Model class DoctorTime
 * 
 * @package             sss_eye_clinic
 * @version             1.0.0
 * @since               v1.0.0
 * @author              Trick Softech Solution
 */

class DoctorTime extends BaseModel {

    /**
     * Table used by this model class
     * 
     * @access              protected
     * @var                 string
     * @since               v1.0.0
     */
    protected $table = 'doctor_time';

    /**
     * Columns which cannot be mass assigned
     * 
     * @access              protected
     * @ver                 array
     * @since               v1.0.0
     */
    protected $guarded = array('id');
}