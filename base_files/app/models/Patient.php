<?php

/*
 * Model class Patient
 * 
 * @package             sss_eye_clinic
 * @version             1.0.0
 * @since               v1.0.0
 * @author              Trick Softech Solution
 */

class Patient extends BaseModel {

    /**
     * Table used by this model class
     * 
     * @access              protected
     * @var                 string
     * @since               v1.0.0
     */
    protected $table = 'patient_details';

    /**
     * Columns which cannot be mass assigned
     * 
     * @access              protected
     * @ver                 array
     * @since               v1.0.0
     */
    protected $guarded = array('id');

    /**
     * Function used to submit patient details
     * 
     * @since       v1.0.0
     * @access      public
     * @var         array
     */
    public static function submitPatientDetails($arrData) {
        if (isset($arrData['patientId'])) {
            $patientObj = self::find($arrData['patientId']);
            if (!empty($patientObj)) {
                $patientObj->patient_name = $arrData['patient_name'];
                $patientObj->patient_age = $arrData['patient_age'];
                $patientObj->patient_phone = $arrData['patient_phone'];
                $patientObj->patient_email = $arrData['patient_email'];
                $patientObj->patient_gender = $arrData['gender'];
                $patientObj->patient_complaints = $arrData['complaints'];
                $patientObj->patient_complaint_duration_month = $arrData['complaints_duration_month'];
                $patientObj->patient_complaint_duration_year = $arrData['complaints_duration_year'];
                $patientObj->diabities = $arrData['diabities'];
                $patientObj->diabities_duration_month = $arrData['diabities_duration_month'];
                $patientObj->diabities_duration_year = $arrData['diabities_duration_year'];
                $patientObj->high_blood_pressure = $arrData['highbp'];
                $patientObj->low_blood_pressure = $arrData['lowbp'];
                $patientObj->typhoid_problem = $arrData['typhoid'];
                $patientObj->doctor_remarks = $arrData['doctor_remarks'];
                $patientObj->appt_date = $arrData['appt_date'];
                $patientObj->FK_doctor_time_id = $arrData['appt_time'];
                return $patientObj->save();
            }
        } else {
            $patientObj = new Patient();
            $patientObj->patient_name = $arrData['patient_name'];
            $patientObj->patient_age = $arrData['patient_age'];
            $patientObj->patient_phone = $arrData['patient_phone'];
            $patientObj->patient_email = $arrData['patient_email'];
            $patientObj->patient_gender = $arrData['gender'];
            $patientObj->patient_complaints = $arrData['complaints'];
            $patientObj->patient_complaint_duration_month = $arrData['complaints_duration_month'];
            $patientObj->patient_complaint_duration_year = $arrData['complaints_duration_year'];
            $patientObj->diabities = $arrData['diabetes'];
            $patientObj->diabities_duration_month = $arrData['diabetes_duration_month'];
            $patientObj->diabities_duration_year = $arrData['diabetes_duration_year'];
            $patientObj->high_blood_pressure = $arrData['highbp'];
            $patientObj->low_blood_pressure = $arrData['lowbp'];
            $patientObj->typhoid_problem = $arrData['typhoid'];
            return $patientObj->save();
        }
    }

    //--------------------------------------------------------------------------
    /**
     * Function used to fetch patient details
     * 
     * @access      public
     * @since       v1.0.0
     * @var         array
     */
    public static function getPatientsDetails($arrData) {
        if (isset($arrData['patientId'])) {
            $patientDetails = self::where('id', '=', $arrData['patientId'])->orderBy('created_at', '=', 'DESC')->get();
        } else {
            $patientDetails = self::orderBy('created_at', '=', 'DESC')->get();
        }
        $patientArray = array();
        if (!empty($patientDetails)) {
            foreach ($patientDetails as $value) {
                if ($value->patient_complaint_duration_year != 0) {
                    $value['complain_duration'] = $value->patient_complaint_duration_month . " Months " . $value->patient_complaint_duration_year . "Years";
                } else {
                    $value['complain_duration'] = $value->patient_complaint_duration_month . " Months";
                }
                $complaint = ($value->patient_complaints == 'left_eye') ? 'Left Eye' : 'Right Eye';
                $patientArray[] = array(
                    'id' => $value->id,
                    'name' => $value->patient_name,
                    'age' => $value->patient_age,
                    'gender' => $value->patient_gender,
                    'complaints' => $complaint,
                    'complaint_month' => $value->patient_complaint_duration_month,
                    'complaint_year' => $value->patient_complaint_duration_year,
                    'complain_duration' => $value['complain_duration'],
                    'diabities' => $value->diabities,
                    'diabities_month' => $value->diabities_duration_month,
                    'diabities_year' => $value->diabities_duration_year,
                    'highbp' => $value->high_blood_pressure,
                    'lowbp' => $value->low_blood_pressure,
                    'typhoid' => $value->typhoid_problem,
                    'doctor_remarks' => $value->doctor_remarks,
                    'appt_date' => $value->appt_date,
                    'appt_time' => $value->FK_doctor_time_id,
                    'phone' => $value->patient_phone,
                    'email' => $value->patient_email
                );
            }
        } else {
            $patientArray = array();
        }
        return $patientArray;
    }
    
    //--------------------------------------------------------------------------
    /**
     * Function used to delete patient details
     * 
     * @since       v1.0.0
     * @access      public
     * @var         array
     */
    public static function deletePatient($arrData){
        $patientObj = self::find($arrData['patientId']);
        $deletePatient = $patientObj->delete();
        if($deletePatient){
            return self::getPatientsDetails(array());
        }       
    }

}
