<?php

use Illuminate\Auth\UserTrait;
use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableTrait;
use Illuminate\Auth\Reminders\RemindableInterface;
/**
 * Model class having methods to perform DB interaction
 *
 * @version     0.0.1
 * @since 	0.0.1
 * @access 	public
 * */
class User extends Eloquent implements UserInterface, RemindableInterface {

    use UserTrait,RemindableTrait;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'users';

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = array('password', 'remember_token');

    /**
     * Get the unique identifier for the user.
     *
     * @return mixed
     */
    public function getAuthIdentifier() {
        return $this->getKey();
    }

    /**
     * Get the password for the user.
     *
     * @return string
     */
    public function getAuthPassword() {
        return $this->password;
    }
	
	//---------------------------------------------------------------------------
	
	/**
	 * Function used to insert new user into database
	 * 
	 * @static
	 * @access	public
	 * @param	integer		
	 * @return	boolean
	 */
	public static function insertNewUser($arrData) {
            $objUser = new User();
            $objUser->username = $arrData['username'];
            $objUser->email = $arrData['user_email'];
            $objUser->password = Hash::make($arrData['password']);
            return $objUser->save();
	}

}

// End of User class
// End of User.php file
