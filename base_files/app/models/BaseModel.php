<?php
/**
 * Model class BaseModel.
 *
 * @package		sss_eye_clinic
 * @version     1.0.0
 * @since		v1.0.0
 * @author		Tric Softech Solution
 */
class BaseModel extends Eloquent {
	
	/**
	 * Array to store validation rules
	 * 
	 * @access	protected
	 * @var		array
	 * @since	1.0.0
	 */
	protected $arrValidationRules = array();
	
	/**
	 * Variable to store error object
	 * 
	 * @access	protected
	 * @since	1.0.0
	 */
	protected $errorMessages;
	
	//-----------------------------------------------------------
	
	/**
	 * Validates the passed data as per the 
	 * set validation rules.
	 * 
	 * @access	public
	 * @var		array 	$data	data to be validated	
	 * @since	v1.0.0
	 */
	public function validate($data) {
		//creating a new validator object
		$v = Validator::make($data, $this->arrValidationRules);
		
		//checking for failure
		if($v->fails()){
			//validation failed
			$this->errorMessages = $v->messages();
			return FALSE;
		}
		//validation passed
		return TRUE;
	}
	
	//-----------------------------------------------------------
	
	/**
	 * Returns the error message object.
	 * 
	 * @access	public
	 * @return	object
	 * @since	v1.0.0
	 */
	public function getErrorMessageObject(){
		return $this->errorMessages;
	}
    
    //--------------------------------------------------------------------------
    
    /**
     * Method to convert object to array.
     * 
	 * @static
     * @access    public
     * @since     v1.0.0
     * @param	  object	$obj
     * @return	  array
     */

    public static function object_to_array($obj) {
        if (is_object($obj))
            $obj = (array) $obj;
        if (is_array($obj)) {
            $new = array();
            foreach ($obj as $key => $val) {
                $new[$key] = BaseModel::object_to_array($val);
            }
        } else
            $new = $obj;
        return $new;
    }
}

// End of BaseModel class
// End of BaseModel.php file