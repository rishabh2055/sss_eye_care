<?php

/*
 * Model class Appointment
 * 
 * @package             sss_eye_clinic
 * @version             1.0.0
 * @since               v1.0.0
 * @author              Trick Softech Solution
 */

class Appointment extends BaseModel {

    /**
     * Table used by this model class
     * 
     * @access              protected
     * @var                 string
     * @since               v1.0.0
     */
    protected $table = 'appointment';

    /**
     * Columns which cannot be mass assigned
     * 
     * @access              protected
     * @ver                 array
     * @since               v1.0.0
     */
    protected $guarded = array('id');

    /**
     * Function used to submit attendance details
     * 
     * @since       v1.0.0
     * @access      public
     * @var         array
     */
    public static function addNewOnlineAppointmentDetails($arrData) {
        $apptObj = new Appointment();
        $apptObj->full_name = $arrData['full_name'];
        $apptObj->user_email = $arrData['user_email'];
        $apptObj->user_mobile = $arrData['user_mobile'];
        $apptObj->appointment_date = $arrData['appt_date'];
        $apptObj->FK_doctor_time_id = $arrData['appt_time'];
        return $apptObj->save();
    }

    //--------------------------------------------------------------------------
    /**
     * Function used to fetch appointment details from database
     * 
     * @access       public
     * @since        v1.0.0
     * @var          array
     */
    public static function getAppointments() {
        $appointments = Appointment::select('full_name', 'user_mobile', 'appointment_date', 'time_between')
                        ->join('doctor_time', 'doctor_time.id', '=', 'appointment.FK_doctor_time_id')
                        ->orderBy('appointment_date', 'DESC')->get();
        $appointmentArray = array();
        if (!empty($appointments)) {
            foreach ($appointments as $value) {
                $appointmentArray[] = array(
                    'name' => $value->full_name,
                    'phone' => $value->user_mobile,
                    'date' => $value->appointment_date,
                    'time' => $value->time_between
                );
            }
        } else {
            $appointmentArray = array();
        }
        return $appointmentArray;
    }

    //--------------------------------------------------------------------------
    /**
     * Function used to fetch upcoming appointment details from database
     * 
     * @access       public
     * @since        v1.0.0
     * @var          array
     */
    public static function getUpcomingAppointments() {
        $online_appointments = Appointment::select('appointment.id', 'full_name', 'user_mobile', 'appointment_date', 'time_between', 'sms_status')
                        ->join('doctor_time', 'doctor_time.id', '=', 'appointment.FK_doctor_time_id')
                        ->whereRaw('appointment_date BETWEEN CURDATE() AND CURDATE() + interval 7 day')
                        ->orderBy('appointment_date', 'DESC')->get();
        $onlineAppointmentArray = array();
        if (!empty($online_appointments)) {
            foreach ($online_appointments as $value) {
                $onlineAppointmentArray[] = array(
                    'id' => $value->id,
                    'name' => $value->full_name,
                    'phone' => $value->user_mobile,
                    'date' => date("D M j, Y", strtotime($value->appointment_date)),
                    'time' => $value->time_between,
                    'appointment' => 'Online',
                    'sms_status' => $value->sms_status
                );
            }
        } else {
            $onlineAppointmentArray = array();
        }
        $walkin_appointments = Patient::select('patient_details.id', 'patient_name', 'patient_phone', 'appt_date', 'time_between', 'sms_status')
                        ->join('doctor_time', 'doctor_time.id', '=', 'patient_details.FK_doctor_time_id')
                        ->whereRaw('appt_date BETWEEN CURDATE() AND CURDATE() + interval 7 day')
                        ->orderBy('appt_date', 'DESC')->get();
        $walkinAppointmentArray = array();
        if (!empty($walkin_appointments)) {
            foreach ($walkin_appointments as $value) {
                $walkinAppointmentArray[] = array(
                    'id' => $value->id,
                    'name' => $value->patient_name,
                    'phone' => $value->patient_phone,
                    'date' => date("D M j, Y", strtotime($value->appt_date)),
                    'time' => $value->time_between,
                    'appointment' => 'Walk-in',
                    'sms_status' => $value->sms_status
                );
            }
        } else {
            $walkinAppointmentArray = array();
        }
        return array_merge($onlineAppointmentArray, $walkinAppointmentArray);
    }

    //--------------------------------------------------------------------------
    /**
     * Function used to send message to patient
     * 
     * @since        v1.0.0
     * @access       public
     * @var          array
     */
    public static function sendMessageToPatient($arrData) {
        $send_sms = new SenderMessageAPI(SMS_URL, SMS_PORT, SMS_USER, SMS_PSWD, SMS_SRC, $arrData['message'], $arrData['mobile'], '1', '1');
        $sms_status = $send_sms->Submit();
        if($sms_status == '1701'){
            // update sms status in database
            if($arrData['patient_type'] == 'Online'){
                // update sms status in appointment table
                $obj_appt = Appointment::find($arrData['id']);
                $obj_appt->sms_status = 'Sent';
                if($obj_appt->save()){
                    return $sms_status;
                }
            }else{
                // update sms status in patient details table
                $obj_patient = Patient::find($arrData['id']);
                $obj_patient->sms_status = 'Sent';
                if($obj_patient->save()){
                    return $sms_status;
                }
            }
        }else{
            return $sms_status;
        }
    }

}
