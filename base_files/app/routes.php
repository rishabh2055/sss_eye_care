<?php

/*
  |--------------------------------------------------------------------------
  | Application Routes
  |--------------------------------------------------------------------------
  |
  | Here is where you can register all of the routes for an application.
  | It's a breeze. Simply tell Laravel the URIs it should respond to
  | and give it the Closure to execute when that URI is requested.
  |
 */
Route::group(array('before' => 'guest'), function() {    
    Route::get('/', 'HomeController@index');
    Route::get('home', 'HomeController@index');   
    Route::get('contact', 'HomeController@contact');
    Route::get('about', 'HomeController@about');
    Route::get('services', 'HomeController@services');
    Route::get('facilities', 'HomeController@facilities');
    Route::get('photo_gallery', 'HomeController@photoGallery');
    Route::get('our_tips', 'HomeController@ourTips');
    Route::get('login', 'AuthorizationController@showLogin');
    Route::post('login', 'AuthorizationController@doLogin');
    Route::post('make_appointment', 'AppointmentController@submitAppointment');
    Route::post('send_mail', 'HomeController@sendMail');
});

Route::group(array('before' => 'auth'), function() {
    Route::get('logout', 'AuthorizationController@doLogout');
    Route::get('signup', 'HomeController@signup');
    Route::post('signup', 'AuthorizationController@registerNewUser');
    Route::get('patient_details', 'PatientInformationController@index');
    Route::post('patient_details', 'PatientInformationController@submitPatientDetails');
    Route::get('online_appointments', 'AppointmentController@onlineAppointments');
    Route::get('upcoming_appointments', 'AppointmentController@upcomingAppointments');
    Route::post('send_message', 'AppointmentController@sendMessage');
    Route::get('patients', 'PatientInformationController@patients');
    Route::post('patients', 'PatientInformationController@patientDetailsByPatientId');
    Route::post('delete_patient', 'PatientInformationController@deletePatient');
});
