<?php
/**
 * Seeder class TimezonesTableSeeder
 * 
 * @package		tms
 * @since		v1.0.0
 * @version		1.0.0
 * @author		Ahex Technologies <info@ahex.co.in>
 */
 class TimezonesTableSeeder extends Seeder {
 	
	public function run() {
		//data to be entered in the table
		$arrData = array(
						array(
							'id' => 1,
							'timezone' => 'Undefined',
							'utc_offset' => 'NA'
						)
					);
		//adding data to table
		DB::table('timezones')->insert($arrData);
	}
 }
//end of class TimezonesTableSeeder
//end of file TimezonesTableSeeder.php