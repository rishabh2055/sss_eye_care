<?php
/**
 * Seeder class ResidentialStatusTableSeeder
 * 
 * @package		tms
 * @version		1.0.0
 * @since		v1.0.0
 * @author		Ahex Technologies <info@ahex.co.in>
 */
 class ResidentialStatusTableSeeder extends Seeder {
 	
	public function run() {
		//data to be entered in the table
		$arrData = array(
						array(
							'id' => 1,
							'name' => 'as a Citizen'
						),
						array(
							'id' => 2,
							'name' => 'as a Permanent Resident'
						),
						array(
							'id' => 3,
							'name' => 'on a Work Visa'
						),
						array(
							'id' => 4,
							'name' => 'on a Study Visa'
						),
						array(
							'id' => 5,
							'name' => 'on a Tourist Visa'
						)
					);
		//adding data to table
		DB::table('residential_status')->insert($arrData);
	}
 }
 //end of class ResidentialStatusSeeder
 //end of file ResidentialStatusSeeder.php
