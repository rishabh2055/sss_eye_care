<?php
/**
 * Seeder class InterestsTableSeeder
 * 
 * @package		tms
 * @since		1.0.0
 * @author		Ahex Technologies <info@ahex.co.in>
 */
 Class InterestsTableSeeder extends Seeder {
 	
	
	public function run() {
		//data to be entered in the table
		$arrData = array(
						array(
							'id' => 1,
							'name' => 'Arts & Crafts (Drawing, Painting etc.)'
						),
						array(
							'id' => 2,
							'name' => 'Collectables'
						),
						array(
							'id' => 3,
							'name' => 'Community Work'
						),
						array(
							'id' => 4,
							'name' => 'Computers (Surfing, Gaming etc.)'
						),
						array(
							'id' => 5,
							'name' => 'Cooking'
						),
						array(
							'id' => 6,
							'name' => 'Entertainment (TV, Movies, Music etc.)'
						),
						array(
							'id' => 7,
							'name' => 'Gardening'
						),
						array(
							'id' => 8,
							'name' => 'Indoor Games'
						),
						array(
							'id' => 9,
							'name' => 'Outdoors (Trekking, Camping etc.)'
						),
						array(
							'id' => 10,
							'name' => 'Photography'
						),
						array(
							'id' => 11,
							'name' => 'Physical Activity (Exercising etc.)'
						),
						array(
							'id' => 12,
							'name' => 'Reading'
						),
						array(
							'id' => 13,
							'name' => 'Sewing (Knitting, Crochet etc.)'
						),
						array(
							'id' => 14,
							'name' => 'Shoping'
						),
						array(
							'id' => 15,
							'name' => 'Socializing'
						),
						array(
							'id' => 16,
							'name' => 'Sports'
						),
						array(
							'id' => 17,
							'name' => 'Travelling'
						),
						array(
							'id' => 18,
							'name' => 'Writing (Poetry, Articles etc.)'
						)
					);
		//adding data to table
		DB::table('interests')->insert($arrData);
	}
 	
 }
//end of class InterestsTableSeeder
//end of file InterestsTableSeeder.php