<?php
/**
 * Seeder class OccupationsTableSeeder
 * 
 * @package		tms
 * @since		1.0.0
 * @author		Ahex Technologies <info@ahex.co.in>
 */
 Class OccupationsTableSeeder extends Seeder {
 	
	
	public function run() {
		//data to be entered in the table
		$arrData = array(
						array(
							'id' => 1,
							'name' => 'Currently Not Working',
							'occupation_category_id' => 5
						),
						array(
							'id' => 2,
							'name' => 'Kothar Mubarak',
							'occupation_category_id' => 1
						),
						array(
							'id' => 3,
							'name' => 'Al Jamea tus Saifiyah',
							'occupation_category_id' => 1
						),
						array(
							'id' => 4,
							'name' => 'Ummal Kiraam',
							'occupation_category_id' => 1
						),
						array(
							'id' => 5,
							'name' => 'Muraqebeen',
							'occupation_category_id' => 1
						),
						array(
							'id' => 6,
							'name' => 'Moallemeen',
							'occupation_category_id' => 1
						),
						array(
							'id' => 7,
							'name' => 'Mazaraat Muqaddasaat',
							'occupation_category_id' => 1
						),
						array(
							'id' => 8,
							'name' => 'Accountant',
							'occupation_category_id' => 3
						),
						array(
							'id' => 9,
							'name' => 'Architect',
							'occupation_category_id' => 3
						),
						array(
							'id' => 10,
							'name' => 'Dentist',
							'occupation_category_id' => 3
						),
						array(
							'id' => 11,
							'name' => 'Doctor',
							'occupation_category_id' => 3
						),
						array(
							'id' => 12,
							'name' => 'Engineer',
							'occupation_category_id' => 3
						),
						array(
							'id' => 13,
							'name' => 'Lawyer',
							'occupation_category_id' => 3
						),
						array(
							'id' => 14,
							'name' => 'Professor/Teacher',
							'occupation_category_id' => 3
						),
						array(
							'id' => 15,
							'name' => 'Other',
							'occupation_category_id' => 3
						),
						array(
							'id' => 16,
							'name' => 'Junior employee in a small organization',
							'occupation_category_id' => 4
						),
						array(
							'id' => 17,
							'name' => 'Junior employee in a large organization',
							'occupation_category_id' => 4
						),
						array(
							'id' => 18,
							'name' => 'Mid-level employee in a small organization',
							'occupation_category_id' => 4
						),
						array(
							'id' => 19,
							'name' => 'Mid-level employee in a large Organization',
							'occupation_category_id' => 4
						),
						array(
							'id' => 20,
							'name' => 'Senior employee in a small organization',
							'occupation_category_id' => 4
						),
						array(
							'id' => 21,
							'name' => 'Small',
							'occupation_category_id' => 2
						),
						array(
							'id' => 22,
							'name' => 'Medium',
							'occupation_category_id' => 2
						),
						array(
							'id' => 23,
							'name' => 'Large',
							'occupation_category_id' => 2
						),
						array(
							'id' => 24,
							'name' => 'Very Large',
							'occupation_category_id' => 2
						),						
					);
		//adding data to table
		DB::table('occupations')->insert($arrData);
	}
 	
 }
//end of class OccupationsTableSeeder
//end of file OccupationsTableSeeder.php