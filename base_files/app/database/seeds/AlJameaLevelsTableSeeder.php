<?php
/**
 * Seeder class AlJameaLevelsTableSeeder
 * 
 * @package		tms
 * @since		1.0.0
 * @author		Ahex Technologies <info@ahex.co.in>
 */
 Class AlJameaLevelsTableSeeder extends Seeder {
 	
	
	public function run() {
		//data to be entered in the table
		$arrData = array(
						array(
							'id' => 1,
							'name' => 'Not Attended'
						),
						array(
							'id' => 2,
							'name' => 'Completed Darajah 1'
						),
						array(
							'id' => 3,
							'name' => 'Completed Darajah 2'
						),
						array(
							'id' => 4,
							'name' => 'Completed Darajah 3'
						),
						array(
							'id' => 5,
							'name' => 'Completed Darajah 4'
						),
						array(
							'id' => 6,
							'name' => 'Completed Darajah 5'
						),
						array(
							'id' => 7,
							'name' => 'Completed Darajah 6'
						),
						array(
							'id' => 8,
							'name' => 'Completed Darajah 7'
						),
						array(
							'id' => 9,
							'name' => 'Completed Darajah 8'
						),
						array(
							'id' => 10,
							'name' => 'Completed Darajah 9'
						),
						array(
							'id' => 11,
							'name' => 'Completed Darajah 10'
						),
						array(
							'id' => 12,
							'name' => 'Completed Darajah 11'
						),
					);
		//adding data to table
		DB::table('al_jamea_levels')->insert($arrData);
	}
 	
 }
//end of class AlJameaLevelsTableSeeder
//end of file AlJameaLevelsTableSeeder.php