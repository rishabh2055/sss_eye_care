<?php
/**
 * Seeder class ActivitiesTableSeeder
 * 
 * @package		tms
 * @since		1.0.0
 * @author		Ahex Technologies <info@ahex.co.in>
 */
 Class ActivitiesTableSeeder extends Seeder {
 	
	
	public function run() {
		//data to be entered in the table
		$arrData = array(
						array(
							'name' => 'search'
						)
					);
		//adding data to table
		DB::table('activities')->insert($arrData);
	}
 	
 }
//end of class ActivitiesTableSeeder
//end of file ActivitiesTableSeeder.php