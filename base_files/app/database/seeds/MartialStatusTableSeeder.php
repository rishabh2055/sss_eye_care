<?php
/**
 * Seeder class MartialStatusTableSeeder
 * 
 * @package		tms
 * @since		1.0.0
 * @author		Ahex Technologies <info@ahex.co.in>
 */
 Class MartialStatusTableSeeder extends Seeder {
 	
	
	public function run() {
		//data to be entered in the table
		$arrData = array(
						array(
							'id' => 1,
							'name' => 'Single'
						),
						array(
							'id' => 2,
							'name' => 'Widowed'
						),
						array(
							'id' => 3,
							'name' => 'Divorced'
						),
						array(
							'id' => 4,
							'name' => 'Divorced after Nikah'
						),
						array(
							'id' => 5,
							'name' => 'Engaged'
						),
						array(
							'id' => 6,
							'name' => 'Married'
						)
					);
		//adding data to table
		DB::table('martial_status')->insert($arrData);
	}
 	
 }
//end of class MartialStatusTableSeeder
//end of file MartialStatusTableSeeder.php