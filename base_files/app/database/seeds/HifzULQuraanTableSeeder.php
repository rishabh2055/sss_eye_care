<?php
/**
 * Seeder class HifzULQuraanTableSeeder
 * 
 * @package		tms
 * @since		1.0.0
 * @author		Ahex Technologies <info@ahex.co.in>
 */
 Class HifzULQuraanTableSeeder extends Seeder {
 	
	
	public function run() {
		//data to be entered in the table
		$arrData = array(
						array(
							'id' => 1,
							'name' => 'Not Doing Hifz'
						),
						array(
							'id' => 2,
							'name' => 'Currently Doing Hifz'
						),
						array(
							'id' => 3,
							'name' => 'Hafiz ul Quraan'
						),
					);
		//adding data to table
		DB::table('hifz_ul_quraan')->insert($arrData);
	}
 	
 }
//end of class HifzULQuraanTableSeeder
//end of file HifzULQuraanTableSeeder.php