<?php

/**
 * Seeder class PlacesTableSeeder
 * 
 * @version		1.0.0
 * @since		1.0.0
 */
class PlacesTableSeeder extends Seeder {

    public function run() {
        //data to be entered in the table
        $arrData = array(
        array('-', 1, 1),
        array('Adelaide', 18, 1),
        array('Brisbane', 18, 1),
        array('Canberra', 18, 1),
        array('Melbourne', 18, 1),
        array('Perth', 18, 1),
        array('Sydney', 18, 1),
        array('Bahrain', 19, 1),
        array('Chittagong', 13, 1),
        array('Dhaka', 13, 1),
        array('Gaborone', 20, 1),
        array('Calgary', 11, 1),
        array('Edmonton', 11, 1),
        array('Mississauga', 11, 1),
        array('Montreal', 11, 1),
        array('Ottawa', 11, 1),
        array('Toronto', 11, 1),
        array('Hong Kong', 21, 1),
        array('Moroni', 22, 1),
        array('Cairo', 23, 1),
        array('Adis Ababa', 39, 1),
        array('Angers', 14, 1),
        array('Grenoble', 14, 1),
        array('Limoges', 14, 1),
        array('Marsielle', 14, 1),
        array('Merignac', 14, 1),
        array('Nantes', 14, 1),
        array('Paris', 14, 1),
        array('Strasbourg', 14, 1),
        array('Toulon', 14, 1),
        array('Germany', 40, 1),
        array('Aarun', 2, 1),
        array('Agar', 2, 1),
        array('Ahmedabad', 2, 1),
        array('Ahmednagar', 2, 1),
        array('Ahwa', 2, 1),
        array('Ajmer', 2, 1),
        array('Aklera', 2, 1),
        array('Akluj', 2, 1),
        array('Akodia', 2, 1),
        array('Akola', 2, 1),
        array('Alibaug', 2, 1),
        array('Alirajpur', 2, 1),
        array('Alleppy', 2, 1),
        array('Alote', 2, 1),
        array('Amalner', 2, 1),
        array('Amalsad', 2, 1),
        array('Amarband', 2, 1),
        array('Amba', 2, 1),
        array('Ambadaberi', 2, 1),
        array('Ambikapur', 2, 1),
        array('Amet', 2, 1),
        array('Amjhera', 2, 1),
        array('Amkhut', 2, 1),
        array('Amla', 2, 1),
        array('Amran', 2, 1),
        array('Amravati', 2, 1),
        array('Amreli', 2, 1),
        array('Anand', 2, 1),
        array('Andheri', 2, 1),
        array('Anjaniya', 2, 1),
        array('Anjar', 2, 1),
        array('Anklav', 2, 1),
        array('Ankleshwar', 2, 1),
        array('Anta', 2, 1),
        array('Arvi', 2, 1),
        array('Asansol', 2, 1),
        array('Ashoknagar', 2, 1),
        array('Ashta', 2, 1),
        array('Aurangabad', 2, 1),
        array('Babra', 2, 1),
        array('Badgaon', 2, 1),
        array('Bari Sadri', 2, 1),
        array('Badlapur', 2, 1),
        array('Badnawar', 2, 1),
        array('Bagasra', 2, 1),
        array('Bagli', 2, 1),
        array('Bagod', 2, 1),
        array('Bakaner', 2, 1),
        array('Bakhatgarh', 2, 1),
        array('Balaghat', 2, 1),
        array('Balambha', 2, 1),
        array('Balasinor', 2, 1),
        array('Balharsha', 2, 1),
        array('Bamania', 2, 1),
        array('Bandibar', 2, 1),
        array('Bandra', 2, 1),
        array('Bangalore', 2, 1),
        array('Bankheri', 2, 1),
        array('Banswara', 2, 1),
        array('Baramati', 2, 1),
        array('Baran', 2, 1),
        array('Barda', 2, 1),
        array('Bardeli', 2, 1),
        array('Bardoli', 2, 1),
        array('Barlewa', 2, 1),
        array('Barmandal', 2, 1),
        array('Barnagar', 2, 1),
        array('Barod', 2, 1),
        array('Baroda', 2, 1),
        array('Baropathak', 2, 1),
        array('Barsi', 2, 1),
        array('Barwaha', 2, 1),
        array('Barwala', 2, 1),
        array('Barwani', 2, 1),
        array('Basim', 2, 1),
        array('Baug', 2, 1),
        array('Behrampura', 2, 1),
        array('Belgaum', 2, 1),
        array('Berasia', 2, 1),
        array('Betma', 2, 1),
        array('Betul', 2, 1),
        array('Bhabhra', 2, 1),
        array('Bhadrewadi', 2, 1),
        array('Bhadsoda', 2, 1),
        array('Bhamgarh', 2, 1),
        array('Bhanpura', 2, 1),
        array('Bhanvad', 2, 1),
        array('Bharuch', 2, 1),
        array('Bhatapara', 2, 1),
        array('Bhauria', 2, 1),
        array('Bhavnagar', 2, 1),
        array('Bhawani Mandi', 2, 1),
        array('Bhayandar East', 2, 1),
        array('Bhayandar West', 2, 1),
        array('Bhesola', 2, 1),
        array('Bhestan', 2, 1),
        array('Bhigwan', 2, 1),
        array('Bhikapur', 2, 1),
        array('Bhilad', 2, 1),
        array('Bhilai', 2, 1),
        array('Bhilwara', 2, 1),
        array('Bhindara', 2, 1),
        array('Bhinder', 2, 1),
        array('Bhiwandi', 2, 1),
        array('Bholamal', 2, 1),
        array('Bhonrasa', 2, 1),
        array('Bhopal', 2, 1),
        array('Bhubaneshwar', 2, 1),
        array('Bhuj', 2, 1),
        array('Bhusawal', 2, 1),
        array('Biaora', 2, 1),
        array('Bijapur', 2, 1),
        array('Bilaspur', 2, 1),
        array('Bilimora', 2, 1),
        array('Bina', 2, 1),
        array('Bodwad', 2, 1),
        array('Bokarata', 2, 1),
        array('Boradi', 2, 1),
        array('Borivali', 2, 1),
        array('Borlai', 2, 1),
        array('Borsad', 2, 1),
        array('Botad', 2, 1),
        array('Brahmnganj', 2, 1),
        array('Bundi', 2, 1),
        array('Burhanpur', 2, 1),
        array('Calicut', 2, 1),
        array('Chabra', 2, 1),
        array('Chacharia', 2, 1),
        array('Chalala', 2, 1),
        array('Champa', 2, 1),
        array('Chandpur', 2, 1),
        array('Chandrapur', 2, 1),
        array('Chandur Bazar', 2, 1),
        array('Chandur Station', 2, 1),
        array('Chandwara', 2, 1),
        array('Chariya', 2, 1),
        array('Chas', 2, 1),
        array('Chau Mahla', 2, 1),
        array('Chechat', 2, 1),
        array('Chennai', 2, 1),
        array('Chhapara', 2, 1),
        array('Chhapi', 2, 1),
        array('Chhapra', 2, 1),
        array('Chhati', 2, 1),
        array('Chhota Udaipur', 2, 1),
        array('Chhoti Sadri', 2, 1),
        array('Chikhli', 2, 1),
        array('Chital', 2, 1),
        array('Chittorgarh', 2, 1),
        array('Chokwana', 2, 1),
        array('Chopda', 2, 1),
        array('Chotila', 2, 1),
        array('Palitana.Chowk', 2, 1),
        array('Chuda', 2, 1),
        array('Chuli', 2, 1),
        array('Coimbatore', 2, 1),
        array('Cuttack', 2, 1),
        array('Dag', 2, 1),
        array('Dahanu', 2, 1),
        array('Dahi', 2, 1),
        array('Dahivel', 2, 1),
        array('Dahod', 2, 1),
        array('Dakor', 2, 1),
        array('Daman', 2, 1),
        array('Damnagar', 2, 1),
        array('Damoh', 2, 1),
        array('Darwa', 2, 1),
        array('Deesa', 2, 1),
        array('Delmal', 2, 1),
        array('Deolali Camp', 2, 1),
        array('Depalpur', 2, 1),
        array('Deswalia', 2, 1),
        array('Devgadh Baria', 2, 1),
        array('Devhut', 2, 1),
        array('Dewas', 2, 1),
        array('Dhamangaon', 2, 1),
        array('Dhamnod', 2, 1),
        array('Dhamtari', 2, 1),
        array('Dhandhalpur', 2, 1),
        array('Dhanduka', 2, 1),
        array('Dhanera', 2, 1),
        array('Dhar', 2, 1),
        array('Dharampuri', 2, 1),
        array('Dharamrai', 2, 1),
        array('Dharangaon', 2, 1),
        array('Dhari', 2, 1),
        array('Dhariya', 2, 1),
        array('Dhariyawad', 2, 1),
        array('Dharni', 2, 1),
        array('Dharsekra', 2, 1),
        array('Dhinoj', 2, 1),
        array('Dholera', 2, 1),
        array('Dholka', 2, 1),
        array('Dhoraji', 2, 1),
        array('Dhrangadhra', 2, 1),
        array('Dhrol', 2, 1),
        array('Dhrufaniya', 2, 1),
        array('Dhule', 2, 1),
        array('Dombivali', 2, 1),
        array('Dondaicha', 2, 1),
        array('Dongaon', 2, 1),
        array('Dudhiya', 2, 1),
        array('Duna', 2, 1),
        array('Dungarpur', 2, 1),
        array('Dungawa', 2, 1),
        array('Dungra', 2, 1),
        array('Durg', 2, 1),
        array('Duwara', 2, 1),
        array('Edlabad', 2, 1),
        array('Eral', 2, 1),
        array('Erandol', 2, 1),
        array('Eranpur', 2, 1),
        array('Erode', 2, 1),
        array('Faizpur', 2, 1),
        array('Fatehnagar', 2, 1),
        array('Fatehpura', 2, 1),
        array('Gadarwara', 2, 1),
        array('Gadhakada', 2, 1),
        array('Galiakot', 2, 1),
        array('Gandhidham', 2, 1),
        array('Gandhinagar', 2, 1),
        array('Gandhi Nagar (Indore)', 2, 1),
        array('Gandhwani', 2, 1),
        array('Gangadhar', 2, 1),
        array('Gangardi', 2, 1),
        array('Ganj Basoda', 2, 1),
        array('Garbada (Dohad)', 2, 1),
        array('Garbada (Jamnagar)', 2, 1),
        array('Gariyadhar', 2, 1),
        array('Garoth', 2, 1),
        array('Gautampura', 2, 1),
        array('Ghagha', 2, 1),
        array('Ghogha', 2, 1),
        array('Ghoti', 2, 1),
        array('Goa', 2, 1),
        array('Godhra', 2, 1),
        array('Gojri', 2, 1),
        array('Gondal', 2, 1),
        array('Gondia', 2, 1),
        array('Gopalganj', 2, 1),
        array('Gorakhpur', 2, 1),
        array('Goregaon', 2, 1),
        array('Guna', 2, 1),
        array('Gurgaon', 2, 1),
        array('Guwhati', 2, 1),
        array('Halvad', 2, 1),
        array('Hanol', 2, 1),
        array('Harda', 2, 1),
        array('Harij', 2, 1),
        array('Harinagar', 2, 1),
        array('Hatpipalya', 2, 1),
        array('Hiladar', 2, 1),
        array('Hinganghat', 2, 1),
        array('Hingoli', 2, 1),
        array('Hoshangabad', 2, 1),
        array('Hosur', 2, 1),
        array('Hubli', 2, 1),
        array('Hudyana', 2, 1),
        array('Husaini Alam', 2, 1),
        array('Hyderabad', 2, 1),
        array('Ichhawar', 2, 1),
        array('Indore', 2, 1),
        array('Itarsi', 2, 1),
        array('Jabalpur', 2, 1),
        array('Jalgaon', 2, 1),
        array('Jalgaon Jamod', 2, 1),
        array('Jalna', 2, 1),
        array('Jam Khambhaliya', 2, 1),
        array('Jamnagar', 2, 1),
        array('Jamner', 2, 1),
        array('Jamshedpur', 2, 1),
        array('Jaora', 2, 1),
        array('Jasdan', 2, 1),
        array('Jashupura', 2, 1),
        array('Jawad', 2, 1),
        array('Jeeran', 2, 1),
        array('Jesar', 2, 1),
        array('Jesawada', 2, 1),
        array('Jetpur', 2, 1),
        array('Jhabua', 2, 1),
        array('Jhalod', 2, 1),
        array('Jhalrapatan', 2, 1),
        array('Jitni', 2, 1),
        array('Jobat', 2, 1),
        array('Jodhpur', 2, 1),
        array('Jodia', 2, 1),
        array('Jogeshwari', 2, 1),
        array('Joj', 2, 1),
        array('Julwania', 2, 1),
        array('Junagadh', 2, 1),
        array('Kaalibavdi', 2, 1),
        array('Kadwal', 2, 1),
        array('Kairasra', 2, 1),
        array('Kakarwas', 2, 1),
        array('Kakinada', 2, 1),
        array('Kalavad', 2, 1),
        array('Kali Mhodi', 2, 1),
        array('Kalisindh', 2, 1),
        array('Kalwani', 2, 1),
        array('Kalyan', 2, 1),
        array('Kalyanpur', 2, 1),
        array('Kamlapur', 2, 1),
        array('Kandanpur', 2, 1),
        array('Kandivali', 2, 1),
        array('Kankarwadi', 2, 1),
        array('Kankroli', 2, 1),
        array('Kannad', 2, 1),
        array('Kannur', 2, 1),
        array('Kanore', 2, 1),
        array('Kapadwanj', 2, 1),
        array('Kapasan', 2, 1),
        array('Karjat', 2, 1),
        array('Karoni', 2, 1),
        array('Kasarwadi', 2, 1),
        array('Kasrawad', 2, 1),
        array('Katabari', 2, 1),
        array('Kathana', 2, 1),
        array('Kauka', 2, 1),
        array('Kavant', 2, 1),
        array('Kesoor', 2, 1),
        array('Kesuvi', 2, 1),
        array('Khachrod', 2, 1),
        array('Khadki', 2, 1),
        array('Khakhal', 2, 1),
        array('Khambat', 2, 1),
        array('Khamgaon', 2, 1),
        array('Khandwa', 2, 1),
        array('Khanpur', 2, 1),
        array('Khar', 2, 1),
        array('Khargone', 2, 1),
        array('Khariar Road', 2, 1),
        array('Khatli', 2, 1),
        array('Khattali Chhoti', 2, 1),
        array('Khattalwada', 2, 1),
        array('Khera', 2, 1),
        array('Kherabad', 2, 1),
        array('Khergam', 2, 1),
        array('Khetia', 2, 1),
        array('Khilchipur', 2, 1),
        array('Khirkiya', 2, 1),
        array('Khopoli', 2, 1),
        array('Khujner', 2, 1),
        array('Kinholi', 2, 1),
        array('Kochi', 2, 1),
        array('Kodenar', 2, 1),
        array('Kolhapur', 2, 1),
        array('Kolkata', 2, 1),
        array('Komba', 2, 1),
        array('Korba', 2, 1),
        array('Korid', 2, 1),
        array('Kota', 2, 1),
        array('Kotada Pitha', 2, 1),
        array('Kotda Sangani', 2, 1),
        array('Kotikhera', 2, 1),
        array('Kotma', 2, 1),
        array('Kuha', 2, 1),
        array('Kukshi', 2, 1),
        array('Kunkavav', 2, 1),
        array('Kurla', 2, 1),
        array('Kurnool', 2, 1),
        array('Kushalgarh', 2, 1),
        array('Kutiyana', 2, 1),
        array('Kuvadava', 2, 1),
        array('Kuwawar', 2, 1),
        array('Lakhatar', 2, 1),
        array('Lathi', 2, 1),
        array('Lilya Mota', 2, 1),
        array('Limbdi (Jhalod)', 2, 1),
        array('Limbdi', 2, 1),
        array('Limkheda', 2, 1),
        array('Lohari', 2, 1),
        array('Lonad', 2, 1),
        array('Lonavala', 2, 1),
        array('Loni', 2, 1),
        array('Lucknow', 2, 1),
        array('Lunawada', 2, 1),
        array('Madrani', 2, 1),
        array('Maheshwar', 2, 1),
        array('Mahidpur', 2, 1),
        array('Mahuda', 2, 1),
        array('Mahuva', 2, 1),
        array('Maksi', 2, 1),
        array('Malad', 2, 1),
        array('Malad West', 2, 1),
        array('Malegoan', 2, 1),
        array('Malkapur', 2, 1),
        array('Manasa', 2, 1),
        array('Manawar', 2, 1),
        array('Mandar', 2, 1),
        array('Mandi Bamora', 2, 1),
        array('Mandleshwar', 2, 1),
        array('Mandsaur', 2, 1),
        array('Mandvi', 2, 1),
        array('Mandvi (Saghir)', 2, 1),
        array('Mandwara', 2, 1),
        array('Manendragarh', 2, 1),
        array('Mangalore', 2, 1),
        array('Mangaoli', 2, 1),
        array('Manmad', 2, 1),
        array('Manpur', 2, 1),
        array('Marol', 2, 1),
        array('Matheran', 2, 1),
        array('Mavli', 2, 1),
        array('Meghnagar', 2, 1),
        array('Mehkar', 2, 1),
        array('Mehsana', 2, 1),
        array('Mendarda', 2, 1),
        array('Metoda', 2, 1),
        array('Mhow', 2, 1),
        array('Minda', 2, 1),
        array('Minimata', 2, 1),
        array('Mira Road', 2, 1),
        array('Mitahpur', 2, 1),
        array('Mojpur', 2, 1),
        array('Morbi', 2, 1),
        array('Mordi', 2, 1),
        array('Mubarakpur', 2, 1),
        array('Muli', 2, 1),
        array('Multai', 2, 1),
        array('Mumbai', 2, 1),
        array('Mumbra', 2, 1),
        array('Mundi', 2, 1),
        array('Mundra', 2, 1),
        array('Mungeli', 2, 1),
        array('Murtizapur', 2, 1),
        array('Muzaffarpur', 2, 1),
        array('Mysore', 2, 1),
        array('Nadiad', 2, 1),
        array('Nagda', 2, 1),
        array('Nagothane', 2, 1),
        array('Nagpur', 2, 1),
        array('Nakalwari', 2, 1),
        array('Nalasopara', 2, 1),
        array('Nalchha', 2, 1),
        array('Naliya', 2, 1),
        array('Nalkhera', 2, 1),
        array('Nampur', 2, 1),
        array('Khanpur (Lunavada)', 2, 1),
        array('Nanded', 2, 1),
        array('Nandurbar', 2, 1),
        array('Nanpur', 2, 1),
        array('Nardana', 2, 1),
        array('Narmada Nagar', 2, 1),
        array('Narsinghgarh', 2, 1),
        array('Nasik', 2, 1),
        array('Nasirpur', 2, 1),
        array('Nathdwara', 2, 1),
        array('Navsari', 2, 1),
        array('Neemuch', 2, 1),
        array('Neemuch Old City', 2, 1),
        array('Nenoda', 2, 1),
        array('Neral', 2, 1),
        array('New Delhi', 2, 1),
        array('Niagarh', 2, 1),
        array('Nimbaheda', 2, 1),
        array('Nivala', 2, 1),
        array('Odha', 2, 1),
        array('Ojhara', 2, 1),
        array('Olda', 2, 1),
        array('Pachora', 2, 1),
        array('Pachore', 2, 1),
        array('Padiyal', 2, 1),
        array('Padla', 2, 1),
        array('Paithan', 2, 1),
        array('Palanpur', 2, 1),
        array('Palasi', 2, 1),
        array('Palghar', 2, 1),
        array('Palitana', 2, 1),
        array('Paliyad', 2, 1),
        array('Palsod', 2, 1),
        array('Panchgani', 2, 1),
        array('Panchmari', 2, 1),
        array('Pandhana', 2, 1),
        array('Pandharpur', 2, 1),
        array('Pansemal', 2, 1),
        array('Panvel', 2, 1),
        array('Para', 2, 1),
        array('Paratwada', 2, 1),
        array('Parbhani', 2, 1),
        array('Pardha', 2, 1),
        array('Pardhari', 2, 1),
        array('Pardi', 2, 1),
        array('Parola', 2, 1),
        array('Parsola', 2, 1),
        array('Partapur', 2, 1),
        array('Patan', 2, 1),
        array('Pati', 2, 1),
        array('Patna', 2, 1),
        array('Pavi Jetpur', 2, 1),
        array('Pen', 2, 1),
        array('Petlad', 2, 1),
        array('Petlawad', 2, 1),
        array('Phaltan', 2, 1),
        array('Pheli', 2, 1),
        array('Pimpalner', 2, 1),
        array('Pipalrawan', 2, 1),
        array('Pipariya', 2, 1),
        array('Piplya Mandi', 2, 1),
        array('Pisawada', 2, 1),
        array('Pitol', 2, 1),
        array('Porbandar', 2, 1),
        array('Prakasha', 2, 1),
        array('Pratapgarh', 2, 1),
        array('Puducherry', 2, 1),
        array('Pulgaon', 2, 1),
        array('Pune', 2, 1),
        array('Pusad', 2, 1),
        array('Quilon', 2, 1),
        array('Radhanpur', 2, 1),
        array('Raigarh', 2, 1),
        array('Raipur', 2, 1),
        array('Raisen', 2, 1),
        array('Rajapara', 2, 1),
        array('Rajgarh (Biaroa)', 2, 1),
        array('Rajgarh', 2, 1),
        array('Rajim', 2, 1),
        array('Rajistapur', 2, 1),
        array('Rajkot', 2, 1),
        array('Rajnandgaon', 2, 1),
        array('Rajod', 2, 1),
        array('Rajpur', 2, 1),
        array('Rajula', 2, 1),
        array('Ralegaon', 2, 1),
        array('Ramganj Mandi', 2, 1),
        array('Rampura', 2, 1),
        array('Ranala', 2, 1),
        array('Ranapur', 2, 1),
        array('Ranchi', 2, 1),
        array('Randhikpur', 2, 1),
        array('Rangpur', 2, 1),
        array('Ranpur', 2, 1),
        array('Rao', 2, 1),
        array('Raoti', 2, 1),
        array('Ratangarh', 2, 1),
        array('Ratlam', 2, 1),
        array('Raver', 2, 1),
        array('Rawatbhata', 2, 1),
        array('Roha', 2, 1),
        array('Rourkela', 2, 1),
        array('Rugnathpura', 2, 1),
        array('Sagar', 2, 1),
        array('Sagwara', 2, 1),
        array('Sailana', 2, 1),
        array('Sajangarh', 2, 1),
        array('Saldi', 2, 1),
        array('Salem', 2, 1),
        array('Salumber', 2, 1),
        array('Sambalpur', 2, 1),
        array('Sami', 2, 1),
        array('Sanavad', 2, 1),
        array('Sangamner', 2, 1),
        array('Sangod', 2, 1),
        array('Sanjan', 2, 1),
        array('Sanjeli', 2, 1),
        array('Santacruz', 2, 1),
        array('Santrampur', 2, 1),
        array('Sarangpur', 2, 1),
        array('Sarasiya', 2, 1),
        array('Sardhar', 2, 1),
        array('Sarsai', 2, 1),
        array('Satana', 2, 1),
        array('Satara', 2, 1),
        array('Savda', 2, 1),
        array('Savli', 2, 1),
        array('Sawarkundla', 2, 1),
        array('Sayla', 2, 1),
        array('Secunderabad', 2, 1),
        array('Sehore', 2, 1),
        array('Sehra', 2, 1),
        array('Selkuwwa', 2, 1),
        array('Selona', 2, 1),
        array('Selvas', 2, 1),
        array('Selwa', 2, 1),
        array('Selwad', 2, 1),
        array('Sendhwa', 2, 1),
        array('Seoni', 2, 1),
        array('Sevaliya', 2, 1),
        array('Shahada', 2, 1),
        array('Shahapur', 2, 1),
        array('Shahdol', 2, 1),
        array('Shajapur', 2, 1),
        array('Shamgarh', 2, 1),
        array('Shapar', 2, 1),
        array('Sheopur', 2, 1),
        array('Shia', 2, 1),
        array('Shihor', 2, 1),
        array('Shirpur', 2, 1),
        array('Shivrajgadh', 2, 1),
        array('Shivrajpur', 2, 1),
        array('Sholapur', 2, 1),
        array('Shujalpur', 2, 1),
        array('Shurwa', 2, 1),
        array('Shyampur', 2, 1),
        array('Sidhpur', 2, 1),
        array('Sindkheda', 2, 1),
        array('Singhana', 2, 1),
        array('Sironj', 2, 1),
        array('Sitamarhi', 2, 1),
        array('Sitamau', 2, 1),
        array('Siyaganj', 2, 1),
        array('Sodi', 2, 1),
        array('Sohagpur', 2, 1),
        array('Songir', 2, 1),
        array('Sonkatch', 2, 1),
        array('Sudarshan', 2, 1),
        array('Sukhsar', 2, 1),
        array('Sulgaon', 2, 1),
        array('Sunel', 2, 1),
        array('Surat', 2, 1),
        array('Surendranagar', 2, 1),
        array('Susner', 2, 1),
        array('Suwasra', 2, 1),
        array('Taherabad', 2, 1),
        array('Takhatpur', 2, 1),
        array('Tal', 2, 1),
        array('Talaja', 2, 1),
        array('Talanpur', 2, 1),
        array('Taloda', 2, 1),
        array('Talwadi', 2, 1),
        array('Talwara', 2, 1),
        array('Talwada Deb', 2, 1),
        array('Tanda', 2, 1),
        array('Tankara', 2, 1),
        array('Tarana', 2, 1),
        array('Thancha', 2, 1),
        array('Thandla', 2, 1),
        array('Thane', 2, 1),
        array('Thangadh', 2, 1),
        array('Thasra', 2, 1),
        array('Thikri', 2, 1),
        array('Tiruchirappalli', 2, 1),
        array('Tiruppur', 2, 1),
        array('Totana', 2, 1),
        array('Trivandrum', 2, 1),
        array('Udaigarh', 2, 1),
        array('Udaipur', 2, 1),
        array('Udhna', 2, 1),
        array('Ujjain', 2, 1),
        array('Umbergaon', 2, 1),
        array('Umrali', 2, 1),
        array('Umreth', 2, 1),
        array('Una', 2, 1),
        array('Undel', 2, 1),
        array('Upleta', 2, 1),
        array('Vaaroth', 2, 1),
        array('Vaki', 2, 1),
        array('Vallabhipur', 2, 1),
        array('Valpura', 2, 1),
        array('Valsad', 2, 1),
        array('Vanthli', 2, 1),
        array('Vapi', 2, 1),
        array('Varjhar', 2, 1),
        array('Vasai', 2, 1),
        array('Vasda', 2, 1),
        array('Vashi', 2, 1),
        array('Vasind', 2, 1),
        array('Veraval', 2, 1),
        array('Versova', 2, 1),
        array('Vidisha', 2, 1),
        array('Vijipur', 2, 1),
        array('Vikhroli', 2, 1),
        array('Vinchhiya', 2, 1),
        array('Viramgam', 2, 1),
        array('Virar', 2, 1),
        array('Virdi', 2, 1),
        array('Vishakhapatnam', 2, 1),
        array('Visavadar', 2, 1),
        array('Visnagar', 2, 1),
        array('Wadhwan', 2, 1),
        array('Waghjipur', 2, 1),
        array('Wankaner', 2, 1),
        array('Wardha', 2, 1),
        array('Warid', 2, 1),
        array('Warora', 2, 1),
        array('Yaval', 2, 1),
        array('Yavatmal', 2, 1),
        array('Yeola', 2, 1),
        array('Zirapur', 2, 1),
        array('Bangli', 24, 1),
        array('Denpasar', 24, 1),
        array('Gianyar', 24, 1),
        array('Jakarta', 24, 1),
        array('Karangasem', 24, 1),
        array('Klunkung', 24, 1),
        array('Makasar', 24, 1),
        array('Palu', 24, 1),
        array('Pontianak', 24, 1),
        array('Singaraja', 24, 1),
        array('Surabaya', 24, 1),
        array('Tabanan', 24, 1),
        array('Karbala', 25, 1),
        array('Dublin', 41, 1),
        array('Japan', 26, 1),
        array('Kobe', 26, 1),
        array('Eldoret', 6, 1),
        array('Kisumu', 6, 1),
        array('Kitui', 6, 1),
        array('Lamu', 6, 1),
        array('Malindi', 6, 1),
        array('Mariakani', 6, 1),
        array('Mombasa', 6, 1),
        array('Nairobi', 6, 1),
        array('Nakuru', 6, 1),
        array('Rabai', 6, 1),
        array('Abraq Khaitan', 15, 1),
        array('Al Mirqab', 15, 1),
        array('Fahaheel', 15, 1),
        array('Kuwait City', 15, 1),
        array('Ambilobe', 7, 1),
        array('Antananarivo', 7, 1),
        array('Antsiranana', 7, 1),
        array('Diego Suarez', 7, 1),
        array('Fort Dauphin', 7, 1),
        array('Mahajanga', 7, 1),
        array('Marunchette', 7, 1),
        array('Metran', 7, 1),
        array('Morondava', 7, 1),
        array('Nosy-Be', 7, 1),
        array('Sambava', 7, 1),
        array('Tamatave', 7, 1),
        array('Tulear', 7, 1),
        array('Alor Setar', 16, 1),
        array('Klang', 16, 1),
        array('Kuala Lumpur', 16, 1),
        array('Penang', 16, 1),
        array('Sungai Petani', 16, 1),
        array('Mauritius', 42, 1),
        array('Mayotte', 27, 1),
        array('Yangon', 28, 1),
        array('Netherlands', 43, 1),
        array('Auckland', 29, 1),
        array('Norway', 30, 1),
        array('Muscat', 31, 1),
        array('Abottabad', 4, 1),
        array('Hyderabad (Sindh)', 4, 1),
        array('Karachi', 4, 1),
        array('Lahore', 4, 1),
        array('Multan', 4, 1),
        array('Peshawar', 4, 1),
        array('Quetta', 4, 1),
        array('Rawalpindi', 4, 1),
        array('Portugal', 44, 1),
        array('Doha', 32, 1),
        array('Reunion', 17, 1),
        array('Dammam', 10, 1),
        array('Jeddah', 10, 1),
        array('Mecca', 10, 1),
        array('Medina', 10, 1),
        array('Riyadh', 10, 1),
        array('Singapore', 33, 1),
        array('South Africa', 34, 1),
        array('Spain', 45, 1),
        array('Colombo', 35, 1),
        array('Jaffna', 35, 1),
        array('Jonkoping', 36, 1),
        array('Mariestad', 36, 1),
        array('MÃ¶lndal', 36, 1),
        array('Switzerland', 46, 1),
        array('Arusha', 8, 1),
        array('Dar es Salaam', 8, 1),
        array('Iringa', 8, 1),
        array('Korogwe', 8, 1),
        array('Morogoro', 8, 1),
        array('Moshi', 8, 1),
        array('Mwanza', 8, 1),
        array('Tanga', 8, 1),
        array('Zambia', 8, 1),
        array('Zanzibar', 8, 1),
        array('Bangkok', 37, 1),
        array('Abu Dhabi', 12, 1),
        array('Ajman', 12, 1),
        array('Alain', 12, 1),
        array('Dubai', 12, 1),
        array('Fujairah', 12, 1),
        array('Ras al Khaima', 12, 1),
        array('Sharjah', 12, 1),
        array('Um Al Quwain', 12, 1),
        array('Birmingham', 9, 1),
        array('Bradford', 9, 1),
        array('Cardiff', 9, 1),
        array('Croydon', 9, 1),
        array('Leicester', 9, 1),
        array('London', 9, 1),
        array('Manchester', 9, 1),
        array('Nottingham', 9, 1),
        array('Atlanta', 3, 1),
        array('Austin', 3, 1),
        array('Bakersfield', 3, 1),
        array('Boston', 3, 1),
        array('Buffalo', 3, 1),
        array('Charlotte', 3, 1),
        array('Chicago', 3, 1),
        array('Chicago (Northwest Side)', 3, 1),
        array('Clark County', 3, 1),
        array('Columbia', 3, 1),
        array('Columbus', 3, 1),
        array('Connecticut', 3, 1),
        array('Dallas', 3, 1),
        array('Detroit', 3, 1),
        array('Houston', 3, 1),
        array('Los Angeles', 3, 1),
        array('Miami', 3, 1),
        array('Minneapolis', 3, 1),
        array('New Jersey', 3, 1),
        array('New York', 3, 1),
        array('Orange County', 3, 1),
        array('Philadelphia', 3, 1),
        array('Pocono Mountains', 3, 1),
        array('Portland', 3, 1),
        array('Raleigh', 3, 1),
        array('San Antonio', 3, 1),
        array('San Diego', 3, 1),
        array('San Francisco', 3, 1),
        array('San Jose', 3, 1),
        array('Seattle', 3, 1),
        array('Spartanburg', 3, 1),
        array('Tampa', 3, 1),
        array('Vancouver', 3, 1),
        array('Virginia', 3, 1),
        array('Washington DC', 3, 1),
        array('Uganda', 38, 1),
        array('Aamal', 5, 1),
        array('Aden', 5, 1),
        array('Akematul Meqaab', 5, 1),
        array('Akematul Qazi', 5, 1),
        array('Al Hudaydah', 5, 1),
        array('Bani Murra', 5, 1),
        array('Bani Saifee', 5, 1),
        array('Dar Bani Ahlas', 5, 1),
        array('Darshaal', 5, 1),
        array('Hadarain', 5, 1),
        array('Hutayb', 5, 1),
        array('Jabal', 5, 1),
        array('Jarma', 5, 1),
        array('Jibla', 5, 1),
        array('Khadar', 5, 1),
        array('Manakha', 5, 1),
        array('Manqadah', 5, 1),
        array('Mazabaha', 5, 1),
        array('Obarat', 5, 1),
        array('Saihaan', 5, 1),
        array('Sanaa', 5, 1),
        array('Saoot', 5, 1),
        array('Shiryaf', 5, 1),
        array('Zabid', 5, 1),
        array('Zahra', 5, 1),
        array('Zimbabwe', 47, 1),
        );
        //adding data to table
        DB::table('places')->insert($arrData);
    }

}

//end of class CountryTableSeeder
//end of file CountryTableSeeder.php