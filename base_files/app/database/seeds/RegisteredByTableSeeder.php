<?php
/**
 * Seeder class RegisteredByTableSeeder
 * 
 * @package		tms
 * @since		1.0.0
 * @author		Ahex Technologies <info@ahex.co.in>
 */
 Class RegisteredByTableSeeder extends Seeder {
 	
	
	public function run() {
		//data to be entered in the table
		$arrData = array(
						array(
							'id'	=>	1,
							'name'	=> 'Self (Candidate)',
						),
						array(
							'id'	=>	2,
							'name'	=> 'Parent',
						),
						array(
							'id'	=>	3,
							'name'	=> 'Brother/Sister',
						),
						array(
							'id'	=>	4,
							'name'	=> 'Friend',
						),
						array(
							'id'	=>	5,
							'name'	=> 'Guardian',
						),
						array(
							'id'	=>	6,
							'name'	=> 'A Relative',
						)
					);
		//adding data to table
		DB::table('registered_by')->insert($arrData);
	}
 	
 }
//end of class RegisteredByTableSeeder
//end of file RegisteredByTableSeeder.php