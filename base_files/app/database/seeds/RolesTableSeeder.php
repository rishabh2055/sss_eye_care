<?php

class RolesTableSeeder extends Seeder {

    public function run() {

        $arrData = array(
            array(
                'id' => 1,
                'name' => 'Administrator'
            ),
            array(
                'id' => 2,
                'name' => 'Co-ordinator'
            ),
            array(
                'id' => 3,
                'name' => 'Counsellor'
            ),
            array(
                'id' => 4,
                'name' => 'Registrar'
            ),
            array(
                'id' => 5,
                'name' => 'Engaged'
            ),
            array(
                'id' => 6,
                'name' => 'Photographer'
            ),
            array(
                'id' => 7,
                'name' => 'Data Entry Operator'
            )
        );
        //adding data to table
        DB::table('roles')->insert($arrData);
    }

}
