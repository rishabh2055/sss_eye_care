<?php
/**
 * Seeder class CountryTableSeeder
 * 
 * @version		1.0.0
 * @since		1.0.0
 */
 class CountriesTableSeeder extends Seeder {
 	public function run() {
		//data to be entered in the table
		$arrData = array(
						array(
							'id' => 1,
							'name' => '-'
						),
						array(
							'id' => 2,
							'name' => 'India'
						),
						array(
							'id' => 3,
							'name' => 'U.S.A.'
						),
						array(
							'id' => 4,
							'name' => 'Pakistan'
						),
						array(
							'id' => 5,
							'name' => 'Yemen'
						),
						array(
							'id' => 6,
							'name' => 'Kenya'
						),
						array(
							'id' => 7,
							'name' => 'Madagascar'
						),
						array(
							'id' => 8,
							'name' => 'Tanzania'
						),
						array(
							'id' => 9,
							'name' => 'U.K.'
						),
						array(
							'id' => 10,
							'name' => 'Saudi Arabia'
						),
						array(
							'id' => 11,
							'name' => 'Canada'
						),
						array(
							'id' => 12,
							'name' => 'U.A.E.'
						),
						array(
							'id' => 13,
							'name' => 'Bangladesh'
						),
						array(
							'id' => 14,
							'name' => 'France'
						),
						array(
							'id' => 15,
							'name' => 'Kuwait'
						),
						array(
							'id' => 16,
							'name' => 'Malaysia'
						),
						array(
							'id' => 17,
							'name' => 'Réunion'
						),
						array(
							'id' => 18,
							'name' => 'Australia'
						),
						array(
							'id' => 19,
							'name' => 'Bahrain'
						),
						array(
							'id' => 20,
							'name' => 'Botswana'
						),
						array(
							'id' => 21,
							'name' => 'China'
						),
						array(
							'id' => 22,
							'name' => 'Comoros'
						),
						array(
							'id' => 23,
							'name' => 'Egypt'
						),
						array(
							'id' => 24,
							'name' => 'Indonesia'
						),
						array(
							'id' => 25,
							'name' => 'Iraq'
						),
						array(
							'id' => 26,
							'name' => 'Japan'
						),
						array(
							'id' => 27,
							'name' => 'Mayotte'
						),
						array(
							'id' => 28,
							'name' => 'Myanmmar'
						),
						array(
							'id' => 29,
							'name' => 'New Zealand'
						),
						array(
							'id' => 30,
							'name' => 'Norway'
						),
						array(
							'id' => 31,
							'name' => 'Oman'
						),
						array(
							'id' => 32,
							'name' => 'Qatar'
						),
						array(
							'id' => 33,
							'name' => 'Singapore'
						),
						array(
							'id' => 34,
							'name' => 'South Africa'
						),
						array(
							'id' => 35,
							'name' => 'Sri Lanka'
						),
						array(
							'id' => 36,
							'name' => 'Sweden'
						),
						array(
							'id' => 37,
							'name' => 'Thailand'
						),
						array(
							'id' => 38,
							'name' => 'Uganda'
						),
						array(
							'id' => 39,
							'name' => 'Ethiopia'
						),
						array(
							'id' => 40,
							'name' => 'Germany'
						),
						array(
							'id' => 41,
							'name' => 'Ireland'
						),
						array(
							'id' => 42,
							'name' => 'Mauritius'
						),
						array(
							'id' => 43,
							'name' => 'Netherlands'
						),
						array(
							'id' => 44,
							'name' => 'Portugal'
						),
						array(
							'id' => 45,
							'name' => 'Spain'
						),
						array(
							'id' => 46,
							'name' => 'Switzerland'
						),
						array(
							'id' => 47,
							'name' => 'Zimbabwe'
						),
					);
		//adding data to table
		DB::table('countries')->insert($arrData);
	}
 }
//end of class CountryTableSeeder
//end of file CountryTableSeeder.php