<?php
/**
 * Seeder class WatanRegionsTableSeeder.
 * 
 * @package		tms
 * @since		v1.0.0
 * @version		1.0.0
 * @author		Ahex Technologies <info@ahex.co.in>
 */
 class WatanRegionsTableSeeder extends Seeder {
 	public function run() {
		//data to be entered in the table
		$arrData = array(
						array(
							'id' => 1,
							'name' => 'Other'
						),
						array(
							'id' => 2,
							'name' => 'India'
						),
						array(
							'id' => 3,
							'name' => 'East Africa'
						),
						array(
							'id' => 4,
							'name' => 'Pakistan'
						),
						array(
							'id' => 5,
							'name' => 'Yemen'
						)
					);
		//adding data to table
		DB::table('watan_regions')->insert($arrData);
	}
 }
//end of class WatanRegionsTableSeeder
//end of file WatanRegionsTableSeeder.php