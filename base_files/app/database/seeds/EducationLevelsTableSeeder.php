<?php
/**
 * Seeder class EducationLevelsTableSeeder
 * 
 * @package		tms
 * @since		1.0.0
 * @author		Ahex Technologies <info@ahex.co.in>
 */
 Class EducationLevelsTableSeeder extends Seeder {
 	
	
	public function run() {
		//data to be entered in the table
		$arrData = array(
						array(
							'id' => 1,
							'name' => 'Primary Schooling',
							'rank' => 1
						),
						array(
							'id' => 2,
							'name' => 'Secondry Schooling',
							'rank' => 2
						),
						array(
							'id' => 3,
							'name' => 'Under Graduation',
							'rank' => 3
						),
						array(
							'id' => 4,
							'name' => 'Graduation',
							'rank' => 4
						),
						array(
							'id' => 5,
							'name' => 'Diploma Course',
							'rank' => 4
						),
						array(
							'id' => 6,
							'name' => 'Post Graduation',
							'rank' => 5
						),
						array(
							'id' => 7,
							'name' => 'Doctrate',
							'rank' => 6
						),
					);
		//adding data to table
		DB::table('education_levels')->insert($arrData);
	}
 	
 }
//end of class EducationLevelsTableSeeder
//end of file EducationLevelsTableSeeder.php