<?php
/**
 * Seeder class JamiyatsTableSeeder
 * 
 * @package		tms
 * @since		1.0.0
 * @author		Ahex Technologies <info@ahex.co.in>
 */
 Class JamiyatsTableSeeder extends Seeder {
 	
	
	public function run() {
		//data to be entered in the table
		$arrData = array(
						array(
							'id' => 1,
							'name' => '-'
						),
						array(
							'id' => 2,
							'name' => 'Marol'
						),
						array(
							'id' => 3,
							'name' => 'Mumbai'
						),
						array(
							'id' => 4,
							'name' => 'Nagpur'
						),
						array(
							'id' => 5,
							'name' => 'Pune'
						),
						array(
							'id' => 6,
							'name' => 'Nasik'
						),
						array(
							'id' => 7,
							'name' => 'Burhanpur'
						),
						array(
							'id' => 8,
							'name' => 'Bhopal'
						),
						array(
							'id' => 9,
							'name' => 'Ahemdabad'
						),
						array(
							'id' => 10,
							'name' => 'Surat'
						),
						array(
							'id' => 11,
							'name' => 'Jamnagar'
						),
						array(
							'id' => 12,
							'name' => 'Rampura'
						),
						array(
							'id' => 13,
							'name' => 'Dohad'
						),
						array(
							'id' => 14,
							'name' => 'Taherabad'
						),
						array(
							'id' => 15,
							'name' => 'Kolkata'
						),
						array(
							'id' => 16,
							'name' => 'Madras'
						),
						array(
							'id' => 17,
							'name' => 'Indore'
						),
						array(
							'id' => 18,
							'name' => 'Ujjain'
						),
						array(
							'id' => 19,
							'name' => 'Far East'
						),
						array(
							'id' => 20,
							'name' => 'East Africa'
						),
						array(
							'id' => 21,
							'name' => 'U.S.A.'
						),
						array(
							'id' => 22,
							'name' => 'Al Khaleej'
						),
						array(
							'id' => 23,
							'name' => 'Pakistan'
						),
						array(
							'id' => 24,
							'name' => 'Indian Ocean'
						),
						array(
							'id' => 25,
							'name' => 'U.K.'
						)
					);
		//adding data to table
		DB::table('jamiyats')->insert($arrData);
	}
 	
 }
//end of class JamiyatsTableSeeder
//end of file JamiyatsTableSeeder.php