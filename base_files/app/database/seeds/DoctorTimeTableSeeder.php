<?php

/**
 * Seeder class DoctorTimeTableSeeder
 * 
 * @package		sss_eye_care
 * @since		v1.0.0
 * @version		1.0.0
 * @author		Trick Softech Solution
 */
class DoctorTimeTableSeeder extends Seeder {

    public function run() {
        //data to be entered in the table
        $arrData = array(
            array(
                'id' => 1,
                'time_between' => '10 A.M. To 11 A.M.'
            ),
            array(
                'id' => 2,
                'time_between' => '12 P.M. To 1 P.M.'
            ),
            array(
                'id' => 3,
                'time_between' => '4 P.M. To 6 P.M.'
            )
        );
        //adding data to table
        DB::table('doctor_time')->insert($arrData);
    }

}

//end of class TimezonesTableSeeder
//end of file TimezonesTableSeeder.php