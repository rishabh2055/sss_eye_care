<?php

class ActionTableSeeder extends Seeder {

    public function run() {
        DB::table('actions')->delete();
        Action::create(array(
            'name' => 'Users',
            'uri' => 'users',
            'parent_id' => '0',
            'is_menu' => 'yes'
        ));
        Action::create(array(
            'name' => 'Upload Image',
            'uri' => 'upload_image',
            'parent_id' => '0',
            'is_menu' => 'no'
        ));
        Action::create(array(
            'name' => 'Update User',
            'uri' => 'update_user',
            'parent_id' => '1'
        ));
        Action::create(array(
            'name' => 'Product',
            'uri' => 'product',
            'parent_id' => '0'
        ));
        Action::create(array(
            'name' => 'Add Product',
            'uri' => 'add_product',
            'parent_id' => '2'
        ));
        Action::create(array(
            'name' => 'Update Product',
            'uri' => 'update_product',
            'parent_id' => '2'
        ));
        Action::create(array(
            'name' => 'Accounts',
            'uri' => 'account',
            'parent_id' => '0'
        ));
        Action::create(array(
            'name' => 'Suppliers',
            'uri' => 'supplier',
            'parent_id' => '0'
        ));
    }

}
