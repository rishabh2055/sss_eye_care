<?php

/**
 * Seeder class TimezonesTableSeeder
 * 
 * @package		sss_eye_care
 * @since		v1.0.0
 * @version		1.0.0
 * @author		Trick Softech Solution
 */
class UserTableSeeder extends Seeder {

    public function run() {
        //data to be entered in the table
        $arrData = array(
            array(
                'id' => 1,
                'username' => 'admin',
                'email' => 'rishabh.chitranshi88@gmail.com',
                'password' => Hash::make('password')
            )
        );
        //adding data to table
        DB::table('users')->insert($arrData);
    }

}

//end of class TimezonesTableSeeder
//end of file TimezonesTableSeeder.php