<?php
/**
 * Seeder class NameFixesTableSeeder
 * 
 * @package		tms
 * @since		1.0.0
 * @author		Ahex Technologies <info@ahex.co.in>
 */
 Class NameFixesTableSeeder extends Seeder {
 	
	
	public function run() {
		//data to be entered in the table
		$arrData = array(
						array(
							'id' => 1,
							'category' => 'Mumin',
							'prefix' => '',
							'suffix' => 'Bhai'
						),
						array(
							'id' => 2,
							'category' => 'Mumina',
							'prefix' => '',
							'suffix' => 'Ben'
						),
						array(
							'id' => 3,
							'category' => 'QA/BZ',
							'prefix' => '',
							'suffix' => 'Bs'
						),
						array(
							'id' => 4,
							'category' => 'Shaikh Mumin',
							'prefix' => 'Sk',
							'suffix' => 'Bhai'
						),
						array(
							'id' => 5,
							'category' => 'Mafsoosh Mumin',
							'prefix' => 'M',
							'suffix' => 'Bhai'
						),
						array(
							'id' => 6,
							'category' => 'Mafsooh Mumina',
							'prefix' => 'M',
							'suffix' => 'Ben'
						),
						array(
							'id' => 7,
							'category' => 'Doctor Mumin',
							'prefix' => 'Dr',
							'suffix' => 'Bhai'
						),
						array(
							'id' => 8,
							'category' => 'Doctor Mumina',
							'prefix' => 'Dr',
							'suffix' => 'Ben'
						),
						array(
							'id' => 9,
							'category' => 'Doctor QA/BZ',
							'prefix' => 'Dr',
							'suffix' => 'Bs'
						),
						array(
							'id' => 10,
							'category' => 'Doctor Had Mumin',
							'prefix' => 'Dr Sk',
							'suffix' => 'Bhai'
						),
						array(
							'id' => 11,
							'category' => 'Doctor Mafsooh Mumin',
							'prefix' => 'Dr M',
							'suffix' => 'Bhai'
						),
						array(
							'id' => 12,
							'category' => 'Doctor Mafsooh Mumina',
							'prefix' => 'Dr M',
							'suffix' => 'Ben'
						),
						array(
							'id' => 13,
							'category' => 'Shahzada',
							'prefix' => 'Shahzada',
							'suffix' => 'Bs'
						),
						array(
							'id' => 14,
							'category' => 'Had',
							'prefix' => 'Syedi',
							'suffix' => 'Bs'
						)
					);
		//adding data to table
		DB::table('name_fixes')->insert($arrData);
	}
 	
 }
//end of class NameFixesTableSeeder
//end of file NameFixesTableSeeder.php