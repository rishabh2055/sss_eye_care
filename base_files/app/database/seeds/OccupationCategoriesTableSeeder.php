<?php

/**
 * Seeder class OccupationCategoriesTableSeeder
 * 
 * @package		tms
 * @since		1.0.0
 * @author		Ahex Technologies <info@ahex.co.in>
 */
Class OccupationCategoriesTableSeeder extends Seeder {

    public function run() {
        //data to be entered in the table
        $arrData = array(
            array(
                'id' => 1,
                'name' => 'Dawat Khidmat'
            ),
            array(
                'id' => 2,
                'name' => 'Business'
            ),
            array(
                'id' => 3,
                'name' => 'Profession'
            ),
            array(
                'id' => 4,
                'name' => 'Service'
            ),
            array(
                'id' => 5,
                'name' => 'Currently Not Working'
            ),
        );
        //adding data to table
        DB::table('occupation_categories')->insert($arrData);
    }

}

//end of class OccupationCategoriesTableSeeder
//end of file OccupationCategoriesTableSeeder.php