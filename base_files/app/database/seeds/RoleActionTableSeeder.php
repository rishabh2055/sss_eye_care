<?php

class RoleActionTableSeeder extends Seeder {

    public function run() {
        $arrData = array(
            array(
                'role_id' => '2',
                'action_id' => '1'
            ),
            array(
                'role_id' => '2',
                'action_id' => '2'
            ),
            array(
                'role_id' => '2',
                'action_id' => '3'
            ),
            array(
                'role_id' => '2',
                'action_id' => '4'
            ),
            array(
                'role_id' => '2',
                'action_id' => '5'
            ),
            array(
                'role_id' => '3',
                'action_id' => '4'
            ),
            array(
                'role_id' => '3',
                'action_id' => '5'
            ),
            array(
                'role_id' => '3',
                'action_id' => '6'
            ),
            array(
                'role_id' => '4',
                'action_id' => '1'
            ),
            array(
                'role_id' => '4',
                'action_id' => '2'
            ),
            array(
                'role_id' => '4',
                'action_id' => '3'
            )
        );
        //adding data to table
        DB::table('role_action')->insert($arrData);
    }

}
