<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCandidateBasicProfilesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('candidate_basic_profiles', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('its_id',10)->unique();
			$table->integer('name_fix_id')->unsigned();
			$table->string('firstname',128);
			$table->string('lastname',128);
			$table->enum('gender',array('f','m'))->default('m');
			$table->integer('martial_status_id')->unsigned();
			$table->date('date_of_birth');
			$table->integer('jamaat_id')->unsigned();
			$table->integer('candidate_status_id')->unsigned();
			$table->date('status_chaged_on')->nullable();
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('candidate_basic_profiles');
	}

}
