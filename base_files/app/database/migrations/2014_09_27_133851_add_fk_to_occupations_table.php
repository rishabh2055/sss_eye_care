<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFkToOccupationsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('occupations', function(Blueprint $table)
		{
			//adding foreign key reference
			$table->foreign('occupation_category_id')->references('id')->on('occupation_categories');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('occupations', function(Blueprint $table)
		{
			//dropping foerign key references
			$table->dropForeign('occupations_occupation_category_id_foreign');			
			
		});
	}

}
