<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateJamaatsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('jamaats', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('name',64);
			$table->integer('place_id')->unsigned();
			$table->integer('jamiyat_id')->unsigned();
			$table->string('its_name',64);
			$table->enum('its_db_status',array('O','N','C'))->default('O');			
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('jamaats');
	}

}
