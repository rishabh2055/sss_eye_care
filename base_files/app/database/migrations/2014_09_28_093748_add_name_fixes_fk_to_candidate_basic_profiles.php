<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddNameFixesFkToCandidateBasicProfiles extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('candidate_basic_profiles', function(Blueprint $table)
		{
			//adding foreign key references
			$table->foreign('name_fix_id')->references('id')->on('name_fixes');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('candidate_basic_profiles', function(Blueprint $table)
		{
			//dropping the foreign key refrences
			$table->dropForeign('candidate_basic_profiles_name_fix_id_foreign');
		});
	}

}
