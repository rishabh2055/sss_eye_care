<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFkToCounselorCandidateTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('counselor_candidate', function(Blueprint $table)
		{
			//adding foreign key references
            $table->foreign('counselor_id')->references('id')->on('users');
			$table->foreign('candidate_id')->references('id')->on('candidate_basic_profiles');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('counselor_candidate', function(Blueprint $table)
		{
			//dropping the foreign key refrences
            $table->dropForeign('counselor_candidate_counselor_id_foreign');
			$table->dropForeign('counselor_candidate_candidate_id_foreign');
		});
	}

}
