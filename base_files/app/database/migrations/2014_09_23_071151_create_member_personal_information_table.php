<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMemberPersonalInformationTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('member_personal_information', function(Blueprint $table)
		{
			$table->integer('user_id')->unsigned();			
			$table->string('firstname',128);
			$table->string('lastname',128);
			$table->string('full_name_en',255)->nullable();
			$table->string('full_name_ar')->nullable();
			$table->string('jamaat_id',10);
			$table->date('date_of_birth');
			$table->enum('gender',array('f','m'));
			$table->string('mobile',15)->nullable();			
			$table->string('home_town_mobile',15)->nullable();			
			$table->string('landline',15)->nullable();
			$table->string('email',255)->nullable();
			$table->string('address',255)->nullable();
			$table->integer('place_id')->unsigned();
                        $table->date('leave_date');
			$table->timestamps();
			$table->softDeletes();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('users');
	}

}
