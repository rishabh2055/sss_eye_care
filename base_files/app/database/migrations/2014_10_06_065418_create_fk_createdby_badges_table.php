<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFkCreatedbyBadgesTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::table('badges', function(Blueprint $table) {
            //adding foreign key references
            $table->foreign('created_by')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::table('badges', function(Blueprint $table) {
            //dropping the foreign key refrences
            $table->dropForeign('badges_user_id_foreign');
        });
    }

}
