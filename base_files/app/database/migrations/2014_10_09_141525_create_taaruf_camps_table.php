<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTaarufCampsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('taaruf_camps', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('camp_name',255);
			$table->string('camp_location',255);
			$table->date('camp_start_date');
			$table->date('camp_end_date');
			$table->string('camp_password',255);
			$table->string('camp_logo',255);
			$table->enum('status',array('completed','running'))->default('running');	
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('taaruf_camps');
	}

}
