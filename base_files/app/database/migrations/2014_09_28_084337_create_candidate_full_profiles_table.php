<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCandidateFullProfilesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('candidate_full_profiles', function(Blueprint $table)
		{
			$table->integer('candidate_id')->unsigned();
			$table->integer('fathers_name_fix_id')->unsigned()->nullable();
			$table->integer('mothers_name_fix_id')->unsigned()->nullable();
			$table->string('fathers_firstname',64)->nullable();
			$table->string('fathers_lastname',64)->nullable();
			$table->string('mothers_firstname',64)->nullable();
			$table->string('mothers_lastname',64)->nullable();
			$table->tinyInteger('number_of_children')->unsigned()->nullable();
			$table->tinyInteger('height_in_inches')->unsigned()->nullable();
			$table->integer('citizen_country_id')->unsigned()->nullable();
			$table->integer('watan_region_id')->unsigned()->nullable();
			$table->string('watan',64)->nullable();
			$table->string('mobile',15)->nullable();
			$table->string('landline',15)->nullable();
			$table->string('other_phone',15)->nullable();
			$table->string('email',255)->nullable();
			$table->string('address_1',100)->nullable();
			$table->string('address_2',100)->nullable();
			$table->integer('place_id')->unsigned()->nullable();
			$table->integer('residential_status_id')->unsigned()->nullable();
			$table->string('pin_zip_code',20)->nullable();
			$table->integer('hifz_ul_quraan_id')->unsigned()->nullable();
			$table->integer('al_jamea_level_id')->unsigned()->nullable();
			$table->enum('is_still_in_jamea',array('no','yes'))->default('no');
			$table->integer('completed_education_level_id')->unsigned()->nullable();
			$table->integer('pursuing_education_level_id')->unsigned()->nullable();
			$table->integer('pursuing_year')->nullable();
			$table->text('education_details')->nullable();
			$table->integer('occupation_id')->unsigned()->nullable();
			$table->text('occupation_details')->nullable();
			$table->string('voluntary_khidmat_details',255)->nullable();
			$table->text('about_self')->nullable();
			$table->integer('interest_id1')->unsigned()->nullable();
			$table->integer('interest_id2')->unsigned()->nullable();
			$table->integer('interest_id3')->unsigned()->nullable();
			$table->text('spouse_preferences')->nullable();
			$table->text('parent_views')->nullable();
			$table->text('counselor_public_remarks')->nullable();
			$table->text('counselor_private_remarks')->nullable();
			$table->string('famiy_photo_note',255)->nullable();
			$table->integer('registered_by_id')->unsigned()->nullable();
			$table->integer('tnc_id')->unsigned()->nullable();
			$table->enum('group',array('X','Y','Z','A'))->nullable();
			$table->enum('is_walkin',array('n','y'))->nullable();
			$table->integer('walkin_by_member_id')->unsigned()->nullable();
			$table->integer('counselor_member_id')->unsigned()->nullable();
			$table->integer('counselor_file_ordinal')->unsigned()->nullable();
			$table->date('registered_since')->nullable();
			$table->timestamps();			
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('candidate_full_profiles');
	}

}
