<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFkToUsersTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::table('member_personal_information', function(Blueprint $table) {
            //adding foreign key references
            $table->foreign('user_id')->references('id')->on('users');
            $table->foreign('place_id')->references('id')->on('places');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::table('member_personal_information', function(Blueprint $table) {
            //dropping the foreign key refrences
            $table->dropForeign('member_personal_information_user_id_foreign');
            $table->dropForeign('member_personal_information_place_id_foreign');
        });
    }

}
