<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFkToCandidateBasicProfilesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('candidate_basic_profiles', function(Blueprint $table)
		{
			//adding foreign key references
			$table->foreign('martial_status_id')->references('id')->on('martial_status');
			$table->foreign('jamaat_id')->references('id')->on('jamaats');
			$table->foreign('candidate_status_id')->references('id')->on('candidate_status');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('candidate_basic_profiles', function(Blueprint $table)
		{
			//dropping the foreign key refrences
			$table->dropForeign('candidate_basic_profiles_martial_status_id_foreign');
			$table->dropForeign('candidate_basic_profiles_jamaat_id_foreign');
			$table->dropForeign('candidate_basic_profiles_candidate_status_id_foreign');
		});
	}

}
