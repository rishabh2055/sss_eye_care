<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('users', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('its_id',15)->unique();
			$table->string('password',255);
			$table->enum('is_mobile_verified',array('no','yes'))->nullable();
			$table->string('verification_code',10)->nullable();
			$table->enum('alert_preference',array('e','s'))->default('e');
			$table->enum('is_password_reset',array('no','yes'))->nullable();
			$table->enum('status',array('active','blocked','deleted','pending'))->nullable();
			$table->timestamps();
			$table->softDeletes();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('users');
	}

}
