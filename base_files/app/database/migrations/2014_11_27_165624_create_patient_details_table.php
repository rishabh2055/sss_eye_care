<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePatientDetailsTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('patient_details', function(Blueprint $table) {
            $table->increments('id');
            $table->string('patient_name', 150);
            $table->string('patient_age', 20);
            $table->enum('patient_gender', array('yes', 'no'))->nullable();
            $table->enum('patient_complaints', array('left_eye', 'right_eye'))->nullable();
            $table->integer('patient_complaint_duration_month')->unsigned();
            $table->integer('patient_complaint_duration_year')->unsigned();
            $table->enum('diabities', array('yes', 'no'))->nullable();
            $table->integer('diabities_duration_month')->unsigned();
            $table->integer('diabities_duration_year')->unsigned();
            $table->enum('high_blood_pressure', array('yes', 'no'))->nullable();
            $table->enum('low_blood_pressure', array('yes', 'no'))->nullable();
            $table->enum('typhoid_problem', array('yes', 'no'))->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::drop('patient_details');
    }

}
