<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFkToCandidateFullProfiles extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('candidate_full_profiles', function(Blueprint $table)
		{
			//adding foreign key references
			$table->foreign('candidate_id')->references('id')->on('candidate_basic_profiles');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('candidate_full_profiles', function(Blueprint $table)
		{
			//dropping the foreign key refrences
			$table->dropForeign('candidate_full_profiles_candidate_id_foreign');
		});
	}

}
