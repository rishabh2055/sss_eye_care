<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFkToPlacesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('places', function(Blueprint $table)
		{
			//adding the foreign key refrences
			$table->foreign('country_id')->references('id')->on('countries');
			$table->foreign('timezone_id')->references('id')->on('timezones');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('places', function(Blueprint $table)
		{
			//dropping foreign key references
			$table->dropForeign('places_country_id_foreign');
			$table->dropForeign('places_timezone_id_foreign');
		});
	}

}
