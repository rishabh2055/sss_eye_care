<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFkToCandidateMeetingResultsTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::table('candidate_meetings', function(Blueprint $table) {
            //adding foreign key references
            $table->foreign('candidate1_id')->references('id')->on('candidate_basic_profiles');
            $table->foreign('candidate2_id')->references('id')->on('candidate_basic_profiles');
            $table->foreign('counselor_id')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::table('candidate_meetings', function(Blueprint $table)
		{
			//dropping the foreign key refrences
			$table->dropForeign('candidate_meetings_meeting_id_foreign');
            $table->dropForeign('meeting_status__foreign');
			$table->dropForeign('candidate_meetings_meeting_result_id_foreign');
		});
    }

}
