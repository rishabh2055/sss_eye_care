<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCandidateMeetingResultsTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('candidate_meeting_result', function(Blueprint $table) {
            $table->integer('meeting_id')->unsigned();
            $table->integer('meeting_result_id')->unsigned();
            $table->string('meeting_remarks', 250);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
       chema::drop('candidate_meeting_result');
    }

}
