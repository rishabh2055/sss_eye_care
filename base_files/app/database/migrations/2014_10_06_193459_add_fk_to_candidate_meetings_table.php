<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFkToCandidateMeetingsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('candidate_meeting_results', function(Blueprint $table)
		{
			//adding foreign key references
			$table->foreign('meeting_id')->references('id')->on('candidate_meetings');
			$table->foreign('meeting_result_id')->references('id')->on('meeting_result');
			
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('candidate_meetings', function(Blueprint $table)
		{
			//dropping the foreign key refrences
			$table->dropForeign('candidate_meetings_meeting_id_foreign');
            $table->dropForeign('candidate_meetings_candidate2_id_foreign');
			$table->dropForeign('candidate_meetings_meeting_result_id_foreign');
		});
	}

}
