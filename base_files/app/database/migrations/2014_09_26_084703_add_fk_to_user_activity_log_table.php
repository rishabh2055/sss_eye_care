<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFkToUserActivityLogTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('user_activity_log', function(Blueprint $table)
		{
			//adding foreign key references
			$table->foreign('user_id')->references('id')->on('users');
			$table->foreign('activity_id')->references('id')->on('activities');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('user_activity_log', function(Blueprint $table)
		{
			//removing foreign key references
			$table->dropForeign('user_activity_log_user_id_foreign');
			$table->dropForeign('user_activity_log_activity_id_foreign');
		});
	}

}
