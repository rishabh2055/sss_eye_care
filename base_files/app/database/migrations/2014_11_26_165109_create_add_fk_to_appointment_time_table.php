<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAddFkToAppointmentTimeTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::table('appointment', function(Blueprint $table) {
            //adding foreign key references
            $table->foreign('FK_doctor_time_id')->references('id')->on('doctor_time');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::table('appointment', function(Blueprint $table) {
            //removing foreign key references
            $table->dropForeign('appointment_FK_doctor_time_id_foreign');
        });
    }

}
