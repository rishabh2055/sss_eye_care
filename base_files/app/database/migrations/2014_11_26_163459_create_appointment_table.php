<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAppointmentTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('appointment', function(Blueprint $table) {
            $table->increments('id');
            $table->string('full_name', 120);
            $table->string('user_email', 50);
            $table->string('user_mobile', 15);
            $table->date('appointment_date');
            $table->integer('FK_doctor_time_id')->unsigned();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
       Schema::drop('appointment');
    }

}
