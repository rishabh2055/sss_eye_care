<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNameFixesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('name_fixes', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('category',64);
			$table->string('prefix',15)->default('');
			$table->string('suffix',15)->default('');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('name_fixes');
	}

}
