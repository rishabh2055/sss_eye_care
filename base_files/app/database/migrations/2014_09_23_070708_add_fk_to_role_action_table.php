<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFkToRoleActionTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('role_action', function(Blueprint $table)
		{
			//adding foreign key references
			$table->foreign('role_id')->references('id')->on('roles');
			$table->foreign('action_id')->references('id')->on('actions');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('role_action', function(Blueprint $table)
		{
			//dropping foreign key references
			$table->dropForeign('role_action_role_id_foreign');
			$table->dropForeign('role_action_action_id_foreign');
		});
	}

}
