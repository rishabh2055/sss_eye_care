<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFkUserAttendanceTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::table('attendance', function(Blueprint $table) {
            //adding foreign key references
            $table->foreign('user_id')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::table('attendance', function(Blueprint $table) {
            //dropping the foreign key refrences
            $table->dropForeign('attendance_user_id_foreign');
        });
    }

}
