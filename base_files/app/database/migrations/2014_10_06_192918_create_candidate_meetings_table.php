<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCandidateMeetingsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('candidate_meetings', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('candidate1_id')->unsigned();
			$table->integer('candidate2_id')->unsigned();
			$table->integer('counselor_id')->unsigned();
			$table->string('meeting_result',255)->nullable();
			$table->datetime('meeting_time')->nullable();
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('candidate_meetings');
	}

}
