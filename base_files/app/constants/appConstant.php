<?php
/**
 |------------------------------------------------------------------------
 | File to contain constants to be used through out the site.
 |------------------------------------------------------------------------
 **/
define('BASE_PATH', 'http://localhost/tms/');
define('ERR_DEFAULT', 100);
define('SUCCESS', 200);
define('ERR_DATABASE', 300);
define('VALIDATION_FAILED', 400);
define('VALUE_NOT_EXISTS', 800);
define('MEMBER_EXISTS', 900);
define('VALUE_ALREADY_EXISTS', 600);
define('ERR_ROLE_NOT_EXISTS', 99);
define('ACCOUNT_VERIFY_DEADLINE', 4);
define('PASS_RECOVERY_DEADLINE', 2);
define('SERVER_ERROR', 500);
define('TEMP_ACCOUNT_BLOCK_DURATION', 12);

#constant for SMS API username
define('SMS_USER', 'ssseye');

#constant for SMS API password
define('SMS_PSWD', 'spe68ytr');

#constant for SMS API URL
define('SMS_URL', 'http://sms6.routesms.com');
define('SMS_PORT', '8080');

#constant for SMS API source
define('SMS_SRC', 'SSSEYE');

