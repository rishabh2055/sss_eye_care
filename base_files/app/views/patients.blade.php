@extends('layouts/defaultLayout')
@section('content')

<script>
    $(document).ready(function () {
        $('#patientsTable').DataTable();
    })
</script>
<header id="head" class="secondary"></header>

<div class="home-wrapper">
    <div class="container">
        <div class="row">
            <h3>Welcome <?php echo ucfirst(Auth::user()->username); ?></h3>
            <!-- Article main content -->
            <article class="col-xs-12 maincontent">
                <header class="page-header">
                    <h1 class="page-title">Patients</h1>
                </header>
                <div id="head_msg"></div>
                <div class="col-md-12 col-sm-12">
                    <div class="panel panel-default">
                        <div class="panel-body">
                            <div id="patient_table">
                                <table class="table table-bordered table-hover text-center" id="patientsTable">
                                    <thead>
                                        <tr>
                                            <th>S.No.</th>
                                            <th>Name</th>
                                            <th>Age</th>
                                            <th>Gender</th>
                                            <th>Complaints</th>
                                            <th>Complaint Duration</th>
                                            <th>Diabities</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody id="patientDetailsTable">
                                        <?php $i = 1; ?>
                                        @foreach($patients as $value)                                       
                                        <tr>
                                            <td class="text-center">{{$i}}</td>
                                            <td class="text-center">{{$value['name']}}</td>
                                            <td class="text-center">{{$value['age']}}</td>
                                            <td class="text-center">{{ucfirst($value['gender'])}}</td>
                                            <td class="text-center">{{$value['complaints']}}</td>
                                            <td class="text-center">{{$value['complain_duration']}}</td>
                                            <td class="text-center">{{ucfirst($value['diabities'])}}</td>
                                            <td class="text-center"><a href="javascript:void(0)" name='{{$value['id']}}' class="btn btn-success btn-xs edit-patient">Edit</a>
                                                <a href="javascript:void(0)" name='{{$value['id']}}' class="btn btn-default btn-xs delete-patient">Delete</a>
                                            </td>
                                        </tr>
                                        <?php $i++; ?>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                            <div id="patient_form">
                                <form method="post" class="form-horizontal" id="patientForm">
                                    <div class="row">
                                        <div class="col-md-6 col-sm-12">
                                        <div class="form-group">
                                            <label for="name" class="col-sm-4 control-label">Name</label>
                                            <div class="col-sm-8">
                                                <input type="text" class="form-control" id="patient_name" name="patient_name" placeholder="Patient Name" tabindex="1">  
                                                <label class="focused">Name</label>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="name" class="col-sm-4 control-label">Phone No</label>
                                            <div class="col-sm-8">
                                                <input type="text" class="form-control" id="patient_phone" name="patient_phone" placeholder="Patient Phone No" tabindex="3">  
                                                <label class="focused">Phone</label>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="gender" class="col-sm-4 control-label">Gender</label>
                                            <div class="col-sm-8">
                                                <label class="radio-inline">
                                                    <input type="radio" id="male" name="gender" value="male" tabindex="5"> Male
                                                </label>
                                                <label class="radio-inline">
                                                    <input type="radio" id="female" name="gender" value="female" tabindex="6"> Female
                                                </label>
                                                <label for="gender" class="error"></label>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="complaints_duration" class="col-sm-4 control-label">Duration Of Complaints</label>
                                            <div class="col-sm-4">
                                                <select id="complaints_duration_month" name="complaints_duration_month" class="form-control" tabindex="9">
                                                    <option value="0">Months</option>
                                                    <option value="1">1</option>
                                                    <option value="2">2</option>
                                                    <option value="3">3</option>
                                                    <option value="4">4</option>
                                                    <option value="5">5</option>
                                                    <option value="6">6</option>
                                                    <option value="7">7</option>
                                                    <option value="8">8</option>
                                                    <option value="9">9</option>
                                                    <option value="10">10</option>
                                                    <option value="11">11</option>
                                                    <option value="12">12</option>
                                                </select>
                                            </div>
                                            <div class="col-sm-4">
                                                <select id="complaints_duration_year" name="complaints_duration_year" class="form-control" tabindex="10">
                                                    <option value="0">Year</option>
                                                    <option value="1">1</option>
                                                    <option value="2">2</option>
                                                    <option value="3">3</option>
                                                    <option value="4">4</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="highbp" class="col-sm-4 control-label">High Blood Pressure</label>
                                            <div class="col-sm-8">
                                                <label class="radio-inline">
                                                    <input type="radio" id="yes_highbp" name="highbp" value="yes" tabindex="13"> Yes
                                                </label>
                                                <label class="radio-inline">
                                                    <input type="radio" id="no_highbp" name="highbp" value="no" tabindex="14"> No
                                                </label>
                                                <label for="highbp" class="error"></label>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="lowbp" class="col-sm-4 control-label">Low Blood Pressure</label>
                                            <div class="col-sm-8">
                                                <label class="radio-inline">
                                                    <input type="radio" id="yes_lowbp" name="lowbp" value="yes" tabindex="21"> Yes
                                                </label>
                                                <label class="radio-inline">
                                                    <input type="radio" id="no_lowbp" name="lowbp" value="no" tabindex="22"> No
                                                </label>
                                                <label for="lowbp" class="error"></label>
                                            </div>
                                        </div>
                                    </div> 
                                    <div class="col-md-6 col-sm-12">
                                        <div class="form-group">
                                            <label for="user_email" class="col-sm-4 control-label">Age</label>
                                            <div class="col-sm-8">
                                                <input type="text" class="form-control" id="patient_age" name="patient_age" placeholder="Patient Age" tabindex="2">  
                                                <label class="focused">Age</label>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="user_email" class="col-sm-4 control-label">Email</label>
                                            <div class="col-sm-8">
                                                <input type="text" class="form-control" id="patient_email" name="patient_email" placeholder="Patient Email" tabindex="4">  
                                                <label class="focused">Email</label>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="complaints" class="col-sm-4 control-label">Complaints</label>
                                            <div class="col-sm-8">
                                                <label class="radio-inline">
                                                    <input type="radio" id="left_eye" name="complaints" value="left_eye" tabindex="7"> Left Eye
                                                </label>
                                                <label class="radio-inline">
                                                    <input type="radio" id="right_eye" name="complaints" value="right_eye" tabindex="8"> Right Eye
                                                </label>
                                                <label for="complaints" class="error"></label>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="diabetes" class="col-sm-4 control-label">Diabetes</label>
                                            <div class="col-sm-8">
                                                <label class="radio-inline">
                                                    <input type="radio" id="yes_diabities" name="diabities" value="yes" tabindex="11"> Yes
                                                </label>
                                                <label class="radio-inline">
                                                    <input type="radio" id="no_diabities" name="diabities" value="no" tabindex="12"> No
                                                </label>
                                                <label for="diabetes" class="error"></label>
                                            </div>
                                        </div>
                                        <div class="form-group" id="diabities_duration_section">
                                            <label for="since" class="col-sm-4 control-label">Since</label>
                                            <div class="col-sm-4">
                                                <select id="diabities_duration_month" name="diabities_duration_month" class="form-control" tabindex="15">
                                                    <option value="0">Months</option>
                                                    <option value="1">1</option>
                                                    <option value="2">2</option>
                                                    <option value="3">3</option>
                                                    <option value="4">4</option>
                                                    <option value="5">5</option>
                                                    <option value="6">6</option>
                                                    <option value="7">7</option>
                                                    <option value="8">8</option>
                                                    <option value="9">9</option>
                                                    <option value="10">10</option>
                                                    <option value="11">11</option>
                                                    <option value="12">12</option>
                                                </select>
                                            </div>
                                            <div class="col-sm-4">
                                                <select id="diabities_duration_year" name="diabities_duration_year" class="form-control" tabindex="16">
                                                    <option value="0">Year</option>
                                                    <option value="1">1</option>
                                                    <option value="2">2</option>
                                                    <option value="3">3</option>
                                                    <option value="4">4</option>
                                                </select>
                                            </div>
                                        </div>                                       
                                        <div class="form-group">
                                            <label for="typhoid" class="col-sm-4 control-label">Thyroid Problems</label>
                                            <div class="col-sm-8">
                                                <label class="radio-inline">
                                                    <input type="radio" id="yes_typhoid" name="typhoid" value="yes" tabindex="19"> Yes
                                                </label>
                                                <label class="radio-inline">
                                                    <input type="radio" id="no_typhoid" name="typhoid" value="no" tabindex="20"> No
                                                </label>
                                                <label for="typhoid" class="error"></label>
                                            </div>
                                        </div>
                                    </div> 
                                        <div class="col-lg-10 col-md-10 col-sm-12 col-xs-12">
                                            <div class="form-group">
                                                <label for="doctor_remarks" class="col-sm-4 control-label">Doctor Remarks</label>
                                                <div class="col-sm-8">
                                                    <textarea class="form-control" id="doctor_remarks" name="doctor_remarks" tabindex="21"></textarea>
                                                </div>
                                            </div>
                                            <input type="hidden" name="patientId" id="patientId">
                                            <div class="form-group">
                                                <label for="appt_date" class="col-sm-4 control-label">Next Appointment Date</label>
                                                <div class="col-sm-8">
                                                    <input class="form-control" id="appt_date" name="appt_date" tabindex="22">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label for="appt_time" class="col-sm-4 control-label">Appointment Time</label>
                                                <div class="col-sm-8">
                                                    <label class="radio-inline">
                                                        <input type="radio" id="morning" name="appt_time" value="1" tabindex="23"> 10 A.M. To 11 A.M.
                                                    </label>
                                                    <label class="radio-inline">
                                                        <input type="radio" id="evening" name="appt_time" value="3" tabindex="24"> 4 P.M. To 6 P.M.
                                                    </label>
                                                    <label for="appt_time" class="error" id="time_error"></label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <hr>

                                    <div class="row">
                                        <div class="col-lg-12 text-right">
                                            <input class="btn btn-default back-patient-table" type="button" value="Back">
                                            <input class="btn btn-default" type="submit" value="Submit">
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>

                </div>

            </article>
            <!-- /Article -->
        </div>
    </div>
</div>
{{HTML::script('assets/js/patients.js')}}
@stop
