@extends('layouts/defaultLayout')
@section('content')
<!--<header id="head" class="secondary"></header>-->
<!-- container -->
<div class="container">

    <div class="row">
        <!--<h3>Welcome <?php echo ucfirst(Auth::user()->username); ?></h3>-->
        <!-- Article main content -->
        <!--        <article class="col-xs-12 maincontent">-->
        <!--            <header class="page-header">
                        <h1 class="page-title">Registration</h1>
                    </header>-->
        <div id="head_msg"></div>
        <div class="col-md-10 col-sm-8 login-wrapper">
            <div class="panel panel-default">
                <div class="panel-body">
                    <h3 class="text-center">Register a new account</h3>
                    <hr/>
                    <form method="post" class="form-horizontal" id="signupForm">

                        <div class="row">
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <!--<label for="name" class="col-sm-4 control-label">Username</label>-->
                                    <div class="col-sm-12 left-inner-addon">
                                        <i class="fa fa-user"></i>
                                        <input type="text" class="form-control" id="username" name="username" placeholder="Username">  
                                        <label class="focused">Username</label>
                                        <span class="error" id="err_username"></span>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <!--<label for="user_email" class="col-sm-4 control-label">Email</label>-->
                                    <div class="col-sm-12 left-inner-addon">
                                        <i class="fa fa-envelope"></i>
                                        <input type="text" class="form-control" id="user_email" name="user_email" placeholder="Email">  
                                        <label class="focused">Email</label>
                                        <span class="error">{{ $errors->first('user_email') }}</span>
                                    </div>
                                </div>
                            </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-6">
                                    <div class="form-group left-inner-addon">
                                        <!--<label for="password" class="col-sm-4 control-label">Password</label>-->
                                        <div class="col-sm-12">
                                            <i class="fa fa-key"></i>
                                            <input type="password" class="form-control" id="password" name="password" placeholder="Password">
                                            <label class="focused">Password</label>
                                            <span class="error">{{ $errors->first('password') }}</span>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <div class="form-group left-inner-addon">
                                        <!--<label for="cnfrm_password" class="col-sm-4 control-label">Confirm Password</label>-->
                                        <div class="col-sm-12">
                                            <i class="fa fa-key"></i>
                                            <input type="password" class="form-control" id="cnfrm_password" name="cnfrm_password" placeholder="Confirm Password">
                                            <label class="focused">Password</label>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <hr>

                            <div class="row">
                                <div class="col-lg-12 text-right">
                                    <input class="btn btn-default" type="submit" value="Register">
                                </div>
                            </div>
                    </form>
                </div>
            </div>

        </div>

        <!--</article>-->
        <!-- /Article -->

    </div>
</div>	<!-- /container -->
{{HTML::script('assets/js/signup.js')}}
@stop