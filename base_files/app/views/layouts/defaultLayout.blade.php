<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport"    content="width=device-width, initial-scale=1.0">
        <META NAME="description" CONTENT="ratina specialist in Allahabad, eye care in Allahabad, eye clinic in Allahabad, eye hospital in Allahabad">
        <META NAME="keywords" CONTENT="Eye Treatment in Allahabad, Retina Treatment, Eye Surgery, Eye Care Allahabad, Laser Treatment, Eye Specialists in Allahabad, Eye Doctors in Allahabad, Cataract Surgery in Allahabad, Glaucoma Treatment Allahabad, Diabetes Eye Care Allahabad, Top Eye Hospitals, Retinal Surgery, Contact Lens Clinic, eye doctor, eye test, the eye centre, eye cataract, laser eye treatment, laser treatment for eyes, eye laser treatment¸ eye specialist">
        <!-- MetaTags - Online Meta Tag http://www.onlinemetatag.com -->
        <!-- Create your meta tag and increase your website position on search engines! -->
        <meta name="author" content="Trick Softech Solution">

        <title>Sharan Eye Care</title>

        <link rel="shortcut icon" href="assets/images/favicon.ico" type="image/x-icon">
        <link rel="icon" href="assets/images/logo.png" type="image/x-icon">

        <!--{{HTML::style("http://fonts.googleapis.com/css?family=Open+Sans:300,400,700")}}-->
        {{HTML::style("assets/css/bootstrap.min.css")}}
        {{HTML::style("assets/css/font-awesome.min.css")}}
        {{HTML::style("assets/css/animate.css")}}
        {{HTML::style("assets/plugins/jquery/css/jquery-ui.min.css")}}
        {{HTML::style('assets/plugins/data-table/css/jquery.dataTables.min.css')}}
        {{HTML::style('assets/plugins/data-table/css/dataTables.responsive.css')}}
        {{HTML::style("assets/plugins/bs-slider/css/jquery.bxslider.css")}}

        <!-- Custom styles for our template -->
        {{HTML::style("assets/css/bootstrap-theme.css")}}
        {{HTML::style("assets/css/main.css")}}
        {{HTML::style("assets/css/carosel.css")}}
        {{HTML::style("assets/css/template.css")}}

        <!-- Javascript files -->
        {{HTML::script("assets/plugins/jquery/js/jquery-1.10.2.min.js")}}
        {{HTML::script("assets/js/bootstrap.min.js")}}
        {{HTML::script("assets/plugins/jquery/js/jquery-ui.min.js")}}
        {{HTML::script("assets/plugins/jquery/js/jquery.validate.min.js")}}
        {{ HTML::script('assets/plugins/data-table/js/jquery.dataTables.js')}}
        {{ HTML::script('assets/plugins/data-table/js/dataTables.responsive.js')}}
        {{HTML::script("assets/js/constant/constant.js")}}

        {{HTML::script("assets/plugins/bs-slider/js/jquery.bxslider.min.js")}}
        <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!--[if lt IE 9]>
        <script src="assets/js/html5shiv.js"></script>
        <script src="assets/js/respond.min.js"></script>
        <![endif]-->
    </head>

    <body class="home">
        <!-- Fixed navbar -->
        <div class="navbar navbar-inverse navbar-fixed-top headroom" >
            <div class="container">
                <div class="navbar-header">
                    <!-- Button for smallest screens -->
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse"><span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> </button>
                    <a class="navbar-brand" href="{{URL::to('/')}}"><img src="assets/images/logo.png" alt="Progressus HTML5 template"></a>
                </div>
                <div class="navbar-collapse collapse">
                    <ul class="nav navbar-nav pull-right">
                        <?php
                        $url = Request::segment(1);
                        ?>
                        @if(!Auth::user())
                        @if($url == '')
                        <li class="active"><a href="{{URL::to('/')}}">Home</a></li>
                        @else
                        <li><a href="{{URL::to('/')}}">Home</a></li>
                        @endif
                        @if($url == 'about')
                        <li class="active"><a href="{{URL::to('about')}}">About</a></li>
                        @else
                        <li><a href="{{URL::to('about')}}">About</a></li>
                        @endif
                        @if($url == 'services')
                        <li class="active"><a href="{{URL::to('services')}}">Services</a></li>
                        @else
                        <li><a href="{{URL::to('services')}}">Services</a></li>
                        @endif
                        @if($url == 'photo_gallery')
                        <li class="active"><a href="{{URL::to('photo_gallery')}}">Photo Gallery</a></li>
                        @else
                        <li><a href="{{URL::to('photo_gallery')}}">Photo Gallery</a></li>
                        @endif
                        <li><a class="book-appointment" href="#appointmentWrapper">Book Your Appointment</a></li>
                        @if($url == 'our_tips')
                        <li class="active"><a href="{{URL::to('our_tips')}}">Our Tips</a></li>
                        @else
                        <li><a href="{{URL::to('our_tips')}}">Our Tips</a></li>
                        @endif
                        @if($url == 'contact')
                        <li class="active"><a href="{{URL::to('contact')}}">Contact</a></li>
                        @else
                        <li><a href="{{URL::to('contact')}}">Contact</a></li>
                        @endif
                        @if($url == 'login')
                        <li class="active"><a class="btn" href="{{URL::to('login')}}">SIGN IN</a></li>
                        @else
                        <li><a class="btn" href="{{URL::to('login')}}">SIGN IN</a></li>
                        @endif
                        @else
                        @if($url == 'patient_details')
                        <li class="active"><a href="{{URL::to('patient_details')}}">New Patient</a></li>
                        @else
                        <li><a href="{{URL::to('patient_details')}}">New Patient</a></li>
                        @endif
                        @if($url == 'online_appointments')
                        <li class="active"><a href="{{URL::to('online_appointments')}}">Online Appointments</a></li>
                        @else
                        <li><a href="{{URL::to('online_appointments')}}">Online Appointments</a></li>
                        @endif
                        @if($url == 'patients')
                        <li class="active"><a href="{{URL::to('patients')}}">Patients</a></li>
                        @else
                        <li><a href="{{URL::to('patients')}}">Patients</a></li>
                        @endif
                        @if($url == 'upcoming_appointments')
                        <li class="active"><a href="{{URL::to('upcoming_appointments')}}">Upcoming Appointments</a></li>
                        @else
                        <li><a href="{{URL::to('upcoming_appointments')}}">Upcoming Appointments</a></li>
                        @endif
                        @if($url == 'signup')
                        <li class="active"><a class="btn" href="{{URL::to('signup')}}">SIGN UP</a></li>
                        @else
                        <li><a class="btn" href="{{URL::to('signup')}}">SIGN UP</a></li>
                        @endif
                        <li><a class="btn" href="{{URL::to('logout')}}">LOGOUT</a></li>                                                                               
                        @endif
                    </ul>
                </div><!--/.nav-collapse -->
            </div>
        </div> 
        <!-- /.navbar -->

        <div>
            @yield('content')
        </div>

        @include('layouts/defaultFooter')

        {{-- Bootstrap core JavaScript --}}
        {{-- Placed at the end of the document so the pages load faster --}}
        {{ HTML::script('assets/js/bootstrap.min.js')}} 
        <script>
            $(document).ready(function () {

                var height = $(window).height();
                //    alert(height);
                $(".carousel-inner > .item > img").css("height", height);
                $(".carousel .item").css("height", height);

                //smooth scroll to href value
                jQuery(".book-appointment").click(function (event) {
                    event.preventDefault();
                    //calculate destination place
                    var dest = 0;
                    if ($(this.hash).offset().top > $(document).height() - $(window).height()) {
                        dest = $(document).height() - $(window).height();
                    } else {
                        dest = $(this.hash).offset().top;
                    }
                    //go to destination
                    $('html,body').animate({scrollTop: dest}, 1000, 'swing');
                });
            });
        </script>
        @yield('footer')

    </body>
</html>
