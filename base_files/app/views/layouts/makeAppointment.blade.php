<div class="contact-us-wrapper" id="appointmentWrapper">
    <div class="new-overlay">

    </div>
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                    <h2>Book Your Appointment.</h2>
                    <p>Please Fill these details to book your appointment with our doctor's team.</p>
                    <p class="default-red">Clinic closed on each Wednesday & Sunday</p>
                    <div id="head_msg"></div>
                <form method="post" class="form-horizontal" id="makeAppointment">
                    <div class="form-group">
                        <!--<label for="name" class="col-sm-4 control-label">Full Name</label>-->
                        <div class="col-sm-8">
                            <input type="text" class="form-control" id="full_name" name="full_name" placeholder="Full Name">  
                            <label class="focused">Name</label>
                        </div>
                    </div>
                    <div class="form-group">
                        <!--<label for="email" class="col-sm-4 control-label">Email</label>-->
                        <div class="col-sm-8">
                            <input type="text" class="form-control" id="user_email" name="user_email" placeholder="Your Email">
                            <label class="focused">Email</label>
                        </div>
                    </div>
                    <div class="form-group">
                        <!--<label for="mobile" class="col-sm-4 control-label">Contact No.</label>-->
                        <div class="col-sm-8">
                            <input type="text" class="form-control" id="user_mobile" name="user_mobile" placeholder="Your Contact No.">
                            <label class="focused">Contact</label>
                        </div>
                    </div>
                    <div class="form-group">
                        <!--<label for="date" class="col-sm-4 control-label">Appt. Date</label>-->
                        <div class="col-sm-8">
                            <input type="text" class="form-control" id="appt_date" name="appt_date" placeholder="Your Appointment Date">
                            <label class="focused">Date</label>
                        </div>
                    </div>
                    <div class="form-group">
                        <!--<label for="date" class="col-sm-4 control-label">Appt. Time</label>-->
                        <div class="col-sm-8">
<!--                            <i class="fa fa-calendar"></i>-->
                            <select name="appt_time" id="appt_time" class="form-control">
                                <option value="">Select Doctor Time</option>
                                @foreach($doctorTime as $key => $value)
                                <option value="{{$value->id}}">{{$value->time_between}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>

                    <button type="submit" class="btn btn-default">Submit</button>

                </form>

            </div>
        </div>
    </div>
</div>
    {{HTML::script('assets/js/appointment.js')}}