<footer id="footer">

    <div class="footer2">
        <div class="container">
            <div class="row">

                <div class="col-md-6 widget">
                    <div class="widget-body">
                        <p class="simplenav">
                            <a href="{{URL::to('/')}}">Home</a> | 
                            <a href="{{URL::to('about')}}">About</a> |
                            <a href="{{URL::to('/contact')}}">Contact</a> |
                            @if(!Auth::user())
                                <b><a href="{{URL::to('/login')}}">Sign in</a></b>
                            @else
                               <b><a href="{{URL::to('/signup')}}">Sign up</a></b>
                            @endif
                        </p>
                    </div>
                </div>
                <div class="col-md-6 widget">
                    <div class="widget-body">
                        <p class="text-right">
                            Copyright &copy; 2014, SSS Eye Care. Designed by <a href="#" rel="designer">Trick Softech Solution</a> 
                        </p>
                    </div>
                </div>
            </div> <!-- /row of widgets -->
        </div>
    </div>
</footer>

<!-- JavaScript libs are placed at the end of the document so the pages load faster -->
<script src="assets/js/headroom.min.js"></script>
<script src="assets/js/jQuery.headroom.min.js"></script>
<script src="assets/js/template.js"></script>
</body>
</html>