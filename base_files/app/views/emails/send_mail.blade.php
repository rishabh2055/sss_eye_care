<!DOCTYPE html>
<html lang="en-US">
	<head>
		<meta charset="utf-8">
	</head>
	<body>
		<h2>Contact Us Details</h2>
                
		<div>
			You have a message from:
                        <ul class="unstyled">
                            <li>Name: {{$full_name}}</li>
                            <li>Email: {{$user_email}}</li>
                            <li>Phone: {{$user_phone}}</li>
                            <li>Message: {{$messages}}</li>
                        </ul>
		</div>
	</body>
</html>