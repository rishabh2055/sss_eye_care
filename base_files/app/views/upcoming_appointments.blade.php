@extends('layouts/defaultLayout')
@section('content')

<script>
    $(document).ready(function () {
        $('#patientsTable').DataTable();
    })
</script>
<header id="head" class="secondary"></header>

<div class="home-wrapper">
    <div class="container">
        <div class="row">
            <h3>Welcome <?php echo ucfirst(Auth::user()->username); ?></h3>
            <!-- Article main content -->
            <article class="col-xs-12 maincontent">
                <header class="page-header">
                    <h1 class="page-title">Patients</h1>
                </header>
                <div id="head_msg"></div>
                <div class="col-md-12 col-sm-12">
                    <div class="panel panel-default">
                        <div class="panel-body">
                            <table class="table table-bordered table-hover text-center table-responsive" id="patientsTable">
                                <thead>
                                    <tr>
                                        <th>S.No.</th>
                                        <th>Name</th>
                                        <th>Phone No.</th>
                                        <th>Appt. Date</th>
                                        <th>Appt. Time</th>
                                        <th>Patient Type</th>
                                        <th>SMS Status</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php $i = 1; ?>
                                    @foreach($appointments as $value)                                       
                                    <tr>
                                        <td class="text-center">{{$i}}</td>
                                        <td class="text-center">{{$value['name']}}</td>
                                        <td class="text-center">{{$value['phone']}}</td>
                                        <td class="text-center">{{$value['date']}}</td>
                                        <td class="text-center">{{$value['time']}}</td>
                                        <td class="text-center">{{$value['appointment']}}</td>
                                        <td class="text-center">{{$value['sms_status']}}</td>
                                        <td class="text-center"><a href="#messagePopup" data-toggle="modal" value='{{$value['phone']}},{{$value['appointment']}},{{$value['id']}}' class="btn btn-success send-message btn-xs" name="{{$value['id']}}">Send Message</a>
                                        </td>
                                    </tr>
                                    <?php $i++; ?>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>

                </div>

            </article>
            <!-- /Article -->
        </div>
    </div>
</div>

<!-- Send Message Modal -->

<div class="modal fade" id="messagePopup" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title" id="myModalLabel">Your Message</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    {{Form::open(array('class' => 'form-horizontal', 'id' => 'sendMessageForm'))}}
                    <div class="col-lg-12 col-md-12 col-sm-12">
                        <div class="form-group">
                            <label for="to" class="col-sm-4 control-label">To</label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control" id="message_to" name="message_to" value="" tabindex="1">  
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="message" class="col-sm-4 control-label">Message</label>
                            <div class="col-sm-8">
                                <textarea class="form-control" id="message" name="message" value="" tabindex="2" rows="8"></textarea> 
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-12 text-right">
                        <input class="btn btn-default" type="submit" value="Send Message">
                    </div>
                    {{Form::close()}}
                </div>
            </div>
        </div>
    </div>
</div>
{{HTML::script('assets/js/appointment.js')}}
@stop
