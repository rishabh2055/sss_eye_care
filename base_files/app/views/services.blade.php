@extends('layouts/defaultLayout')
@section('content')

<!-- Header -->
<header id="head"><img src="assets/images/our_services.jpg" style="height: 525px;" alt="Services"></header>
<!-- container -->
<section class="breadcrumb-wrapper">
    <div class="container">
        <ol class="breadcrumb">
            <li><a href="{{URL::to('/')}}">Home</a></li>
            <li class="active">Services</li>
        </ol>
    </div>
</section>
<!-- /Header -->
<!-- container -->
<div class="container">
    <header class="page-header">
        <h1 class="page-title default-red">Eye care Service</h1>
    </header>
    <div class="well">
        <p class="para font-open-sans">
            To maintain your eye health and that of your family members', we offer a broad range of ophthalmology and optometry service - please see list below. Remember that a simple eye exam may detect silent diseases such as high blood pressure, diabetes or high cholesterol before they become serious.
        </p>
        <br />
        <p class="para font-open-sans">
            If you are not sure whether you should see an optometrist or an ophthalmologist (eye doctor),
        </p>
        <br />
        <div class="row">
            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                <h2 class="font-open-sans default-blue">Our optometry services include:</h2>
                <ul class="about list-unstyled">
                    <li><i class="fa fa-hand-o-right"></i> Refraction ('eye test')</li>
                    <li><i class="fa fa-hand-o-right"></i> Prescriptions for glasses and contact lenses;</li>
                    <li><i class="fa fa-hand-o-right"></i> Eye health examination</li>
                    <li><i class="fa fa-hand-o-right"></i> Diabetic eye surveillance</li>
                </ul>
            </div>
            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                <img src="assets/images/photos/eye.png" alt="Cornea">
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="row">
                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                    <h2 class="font-open-sans default-blue">Our ophthalmology services include:</h2>
                    <ul class="about list-unstyled">
                        <li><i class="fa fa-hand-o-right"></i> Slit Lamp with chairunit</li>
                        <li><i class="fa fa-hand-o-right"></i> Full Refraction Unit</li>
                        <li><i class="fa fa-hand-o-right"></i> Keratometry</li>
                        <li><i class="fa fa-hand-o-right"></i> A-Scan</li>
                        <li><i class="fa fa-hand-o-right"></i> B-Scan</li>
                        <li><i class="fa fa-hand-o-right"></i> Slit Lamp Biomicroscopy 90/78D</li>
                        <li><i class="fa fa-hand-o-right"></i> Contact Lens/Spectcles</li>
                        <li><i class="fa fa-hand-o-right"></i> Lasik</li>
                        <li><i class="fa fa-hand-o-right"></i> Green Retinal Laser (Medical Retina)</li>
                        <li><i class="fa fa-hand-o-right"></i> Surgical Retina- Zeiss Microscope (xy)</li>
                        <li><i class="fa fa-hand-o-right"></i> BIOM" -autometed</li>
                        <li><i class="fa fa-hand-o-right"></i> Green Endolaser</li>
                        <li><i class="fa fa-hand-o-right"></i> DORC vitrectomy machine Cryotherapy</li>
                    </ul>
                </div>
                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                    <div class="text-right">
                        <img src="assets/images/12/100_1566.JPG" class="servises_images" alt="Services 1">
                        <img src="assets/images/photos/100_1583.JPG" class="servises_images" alt="Services 2">
                    </div>                    
                </div>
            </div>
            <br /><hr>
            <div class="well">
                <h2 class="font-open-sans default-blue">Diseases</h2>
                <p class="para font-open-sans">
                    Most people will have a problem with their eyes at some point in their lives. Most eye problems are not serious and do not require a doctor's care. However, certain eye diseases are serious and can result in blindness if left untreated.
                </p>
                <ul class="about list-unstyled diseases-ul">
                    <li data-popbox="pop1"><i class="fa fa-hand-o-right"></i> <a href="javascript:void(0)" value="cataract">Cataract</a></li>
                    <li data-popbox="pop1"><i class="fa fa-hand-o-right"></i> <a href="javascript:void(0)" value="Glaucoma">Glaucoma</a></li>
                    <li data-popbox="pop1"><i class="fa fa-hand-o-right"></i> <a href="javascript:void(0)" value="corneal-opacity">Corneal opacity</a></li>
                    <li data-popbox="pop1"><i class="fa fa-hand-o-right"></i> <a href="javascript:void(0)" value="eye-injuries">Eye injuries</a></li>
                    <li data-popbox="pop1"><i class="fa fa-hand-o-right"></i> <a href="javascript:void(0)" value="corneal-transplant">Corneal transplant</a></li>
                    <li data-popbox="pop1"><i class="fa fa-hand-o-right"></i> <a href="javascript:void(0)" value="conjunctivitis">Conjunctivitis</a></li>
                    <li data-popbox="pop1"><i class="fa fa-hand-o-right"></i> <a href="javascript:void(0)" value="foreign-bodies-in-eye">Foreign bodies in eye</a></li>
                    <li data-popbox="pop1"><i class="fa fa-hand-o-right"></i> <a href="javascript:void(0)" value="blepherits">Blepherits</a></li>
                    <li data-popbox="pop1"><i class="fa fa-hand-o-right"></i> <a href="javascript:void(0)" value="Pediatric-ophthalmology">Pediatric ophthalmology</a></li>
                    <li data-popbox="pop1"><i class="fa fa-hand-o-right"></i> <a href="javascript:void(0)" value="ROP-(Retinopathy-of-Prematurity)-Screaming-&-Management-for-new-borns">ROP (Retinopathy of Prematurity) Screaming & Management for new borns</a></li>
                    <li data-popbox="pop1"><i class="fa fa-hand-o-right"></i> <a href="javascript:void(0)" value="Squint">Squint </a></li>
                    <li data-popbox="pop1"><i class="fa fa-hand-o-right"></i> <a href="javascript:void(0)" value="Dry-Eye">Dry Eye</a></li>
                    <li data-popbox="pop1"><i class="fa fa-hand-o-right"></i> <a href="javascript:void(0)" value="Diabetic-Retinopathy">Diabetic Retinopathy</a></li>
                    <li data-popbox="pop1"><i class="fa fa-hand-o-right"></i> <a href="javascript:void(0)" value="Hypertensive-Retinopathy">Hypertensive Retinopathy</a></li>
                    <li data-popbox="pop1"><i class="fa fa-hand-o-right"></i> <a href="javascript:void(0)" value="Retinal-Detachment">Retinal Detachment</a></li>
                    <li data-popbox="pop1"><i class="fa fa-hand-o-right"></i> <a href="javascript:void(0)" value="Post-Cataract-Surgery-Complications">Post Cataract Surgery Complications</a></li>
                </ul>
            </div>
        </div>
    </div>
    <div id="pop1" class="popbox">
        <img alt="Service Names">
    </div>

</div>	<!-- /container -->

@include('layouts/makeAppointment')
@stop

