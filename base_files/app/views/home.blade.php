@extends('layouts/defaultLayout')
@section('content')

<header id="head" class="secondary"></header>
<section class="breadcrumb-wrapper">
    <div class="container">
        <ol class="breadcrumb">
            <li><a href="{{URL::to('/home')}}">Home</a></li>
        </ol>              
    </div>
</section>

<div class="home-wrapper">
    <div class="container">
        <div class="row">
            <h3>Welcome <?php echo ucfirst(Auth::user()->username); ?></h3>
        </div>
    </div>
</div>
@stop