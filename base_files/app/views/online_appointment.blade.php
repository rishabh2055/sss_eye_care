@extends('layouts/defaultLayout')
@section('content')
<script>
    $(document).ready(function(){
        $('#appointmentTable').DataTable();
    })
</script>
<header id="head" class="secondary"></header>

<div class="home-wrapper">
    <div class="container">
        <div class="row">
            <h3>Welcome <?php echo ucfirst(Auth::user()->username); ?></h3>
            <!-- Article main content -->
            <article class="col-xs-12 maincontent">
                <header class="page-header">
                    <h1 class="page-title">Online Appointments</h1>
                </header>
                <div id="head_msg"></div>
                <div class="col-md-12 col-sm-12">
                    <div class="panel panel-default">
                        <div class="panel-body">
                            <table class="table-responsive" id="appointmentTable">
                                <thead>
                                    <tr>
                                        <th>S.No.</th>
                                        <th>Name</th>
                                        <th>Contact No.</th>
                                        <th>Appointment Date</th>
                                        <th>Appointment Time</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php $i = 1; ?>
                                    @foreach($appointments as $value)
                                    <tr>
                                        <td>{{$i}}</td>
                                        <td>{{$value['name']}}</td>
                                        <td>{{$value['phone']}}</td>
                                        <td>{{$value['date']}}</td>
                                        <td>{{$value['time']}}</td>
                                    </tr>
                                    <?php $i++; ?>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>

                </div>

            </article>
            <!-- /Article -->
        </div>
    </div>
</div>
{{HTML::script('assets/js/patient_details.js')}}
@stop