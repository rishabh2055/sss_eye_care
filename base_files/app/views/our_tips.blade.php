@extends('layouts/defaultLayout')
@section('content')
<header id="head"><img src="assets/images/eye_care.jpg" style="height: 525px;" alt="Our Tips"></header>
<!-- container -->
<section class="breadcrumb-wrapper">
    <div class="container">
        <ol class="breadcrumb">
            <li><a href="{{URL::to('/')}}">Home</a></li>
            <li class="active">Our Tips</li>
        </ol>
    </div>
</section>

<section class="your-eyes">
    <div class="container">
        <header class="page-header">
            <h1 class="page-title default-red">How to Take Care of Your Eyes</h1>
        </header>
        <div class="row">
            <div class="col-lg-12">
                <p class="para font-open-sans">Diet and regular upkeep are two important factors in taking care of your eyes. Eye
                    exercises can restore your sight. Your eyes are the window to the world. It's important that you </p>
                <img class="img-responsive" src="assets/images/img-2.png" alt="Our Tips 1"/>
            </div>
        </div>
    </div>
</section>

<section class="eat-lots-wrapper">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <p class="para font-open-sans">
                    <span class="default-blue bold">Eat lots of fruits and veggies!</span>
                    Carrots, loaded with beta carotene are especially helpful in maintaining healthy eyes. That's because beta carotene is an antioxidant that reduces the risk of macular degeneration
                </p>
                <p class="para font-open-sans">	Broccoli, bell peppers, and brussels sprouts are also great for your eyes,
                    containing lots of vitamin C. Okay, they're not the greatest tasting 
                    vegetables in the world (or maybe you love them, in which case good for
                    you!) so dip them in hummus or other yummy yogurt spreads to make
                    them palatable</p>
                <p class="para font-open-sans">Wild salmon and sardines are also healthy options for your eyes. That's because they contain omega-3 fats, which are the good fats for your body. These omega-3s protect tiny blood vessels in the eyes</p>
                <p class="para font-open-sans">Sweet potatoes and spinach round out the healthy options for maintaining good eyes. Both contain beta carotene, and spinach contains lots of vitamin C, lutein, and zeaxanthin. Make the sweet spuds into home-fries with a bit of olive oil, and make spinach into a tasty side or a delectable dip.</p>
                <img class="img-responsive" src="assets/images/img-3.png" alt="Our Tips 2"/><br />
            </div> 
        </div>
    </div>
</section>
<!-- Starting of avoid wearing contact lenses  -->
<section class="avoid-wrapper">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <p class="para font-open-sans">
                    <span class="default-black">Avoid</span>
                    <span class="default-blue"> wearing contact lenses</span>
                    <span class="default-black"> for more than 10 hour. </span>
                    permanent sight damage as well as extreme discomfort to your eyes. Don't wear glasses too long either. Especially 3-D glasses!
                </p>
                <p class="para font-open-sans">Never sleep with your contact lenses on unless specifically instructed to do so. Your eyes need regular supplies of oxygen, and lenses block the flow of oxygen to the eyes, especially during sleep. So doctors recommend a normal period of break for your eyes during the night</p>
                <p class="para font-open-sans">Avoid wearing your contact lenses when swimming. Your lenses could easily slip off your ideas when they come in contact with the surface area of the water. If you are wearing goggles, your contact lenses may be used while swimming.</p>

                <img class="img-responsive" src="assets/images/img-4.png" alt="Our Tips 3"/><br />
            </div> 
        </div>
    </div>
</section>

<!-- Starting of Use allergen -->
<section class="use-allergen-wrapper">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <p class="para font-open-sans">
                    <span class="default-black">Use allergen-reducing </span>
                    <span class="default-blue"> eye drops</span>
                    <span class="default-black">sparingly.</span>
                    Using an allergen-reducing eye drop during allergy season to 'get the red out' and sooth itchiness may help on a limited basis, but chronic daily use can actually make the problem worse.  </p>
                <p class="para font-open-sans">Allergen-reducing eye drops work by constricting the blood flow to the cornea, thereby depriving it of oxygen. So while your eyes don't feel inflamed and itchy anymore, they're actually not getting any oxygen from blood. That's not ideal.</p>
                <p class="para font-open-sans">Using redness-relieving eye drops chronically will cause more redness in your eyes. Your body gets so accustomed to the chemicals in the eye drops that they no longer work effectively.</p>
                <p class="para font-open-sans">Read the labels of eye drops carefully; many drops cannot be used while wearing contacts</p>
                <img class="img-responsive" src="assets/images/img-5.png" alt="Our Tips 4"/><br />
            </div> 
        </div>
    </div>
</section>

<!-- Starting of Use cucumber  -->
<section class="use-cucumber-wrapper">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <p class="para font-open-sans">
                    <span class="default-black">Use cucumber on your eyelids. </span>
                    Press cold cucumber slices gently against eyelids 10 minutes before going to sleep at night to prevent
                    <span class="default-blue"> puffiness</span>
                </p>
                <p class="para font-open-sans">Cucumbers contain as corbic acid and caffeic acid, which prevent water retention. These compounds help explain why cucumbers are used throughout the world to treat inflammation and dermatitis</p>
                <p class="para font-open-sans">Green tea bags may also help prevent puffiness if applied to the eyes. Soak the tea bag in cold water for a few minutes and place over eyes for 15-20 minutes. The tannins in the tea should help reduce inflammation</p>
                <p class="para font-open-sans">Read the labels of eye drops carefully; many drops cannot be used while wearing contacts</p>
                <img class="img-responsive" src="assets/images/img-15.png" alt="Our Tips 5"/><br />
            </div> 
        </div>
    </div>
</section>
<!-- Starting of Wear UV protective   -->
<section class="wear-uv-wrapper">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <p class="para font-open-sans">
                    <span class="default-black">Wear UV protective sunglasses. </span>
                    Get polarized lenses, NOT just darker lenses. The lenses that only make the world darker will just make your pupils dilate and don't do a thing to stop the UV rays    
                </p>
                <p class="para font-open-sans">Prolonged exposure to UV rays can harm your eyesight, protection in youth can help prevent loss of eyesight in later years. Exposure to UV rays has been linked to cataracts, macular degeneration, pingueculae and pterygia, harmful conditions for the eyes. </p>
                <p class="para font-open-sans">Since the damage to eyes from UV rays builds up over a lifetime, it's important to shield children from harmful rays. Make sure your children wear hats and protective glasses when they are out in the sunlight for prolonged periods.</p>
                <p class="para font-open-sans">Be sure to wear sunglasses even if you're in the shade. Even though shade lessens UV and HEV exposure significantly, you're still exposing your eyes to UV rays reflected off of buildings and other structures</p>
                <img class="img-responsive" src="assets/images/img-7.png" alt="Our Tips 6"/><br />
            </div> 
        </div>
    </div>
</section>

<!-- Starting of Try not to spend   -->
<section class="try-wrapper">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <p class="para font-open-sans">
                    <span class="default-black">Try not to spend so much time looking at your</span>
                    <span class="default-blue">computer screen</span>
                    (As a matter of fact, now would be a good time to give your eyes a break, since you're not doing anything too important.) 
                </p>
                <img class="img-responsive" src="assets/images/img-8.png" alt="Our Tips 7"/><br />
                <p class="para font-open-sans">While science hasn't yet proven that looking at computer screens causes permanent eye damage, it may cause eye strain and dry eyes. The glare from computer screens causes muscle fatigue in the eyes, either from being too bright or too dark.
                    People blink less when they're looking at a screen, causing drier eyes. Make a conscious effort to blink every 30 seconds when you're sitting down and looking at your computer screen to combat dry eyes
                </p>
            </div> 
        </div>
    </div>
</section>

<!-- Starting of Wear goggles    -->
<section class="wear-goggles ">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <p class="para font-open-sans">
                    <span class="default-black">Wear goggles when appropriate</span>

                    Be sure to wear goggles or other eye protective wear when working with chemicals or any place with harmful airborne particulates.  
                </p>
                <p class="para font-open-sans">Minimize the transmission of harmful particulate matter into eye region.</p>

            </div> 
        </div>
        <div class="row">
            <div class="col-lg-12">
                <img class="img-responsive" src="assets/images/img-11.png" alt="Our Tips 8"/><br />
            </div> 
            <div class="col-lg-12">
                <img class="img-responsive" src="assets/images/img-12.png" alt="Our Tips 9"/><br />
            </div> 
            <div class="col-lg-12">
                <img class="img-responsive" src="assets/images/img-16.png" alt="Our Tips 10"/><br />
            </div> 
            <div class="col-lg-12">
                <!--<img class="img-responsive" src="assets/images/img-14.png" alt=""/><br />-->
            </div> 
        </div>
    </div>
</section>

<!-- Starting of Exercise your eyes    -->
<section class="Exercise-wrapper ">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <p class="para font-open-sans">
                    <span class="default-blue">Exercise your eyes</span>
                    <span class="default-black">, and also be sure to </span>
                    <span class="default-blue">, relax </span>
                    Try focusing your eyes on objects that are near to you, then objects that are far away. Repeat this process multiple times.  
                </p>
                <p class="para font-open-sans">Sit down, place your elbows on your hips, close your eyes and bring your closed eyes down, resting them on your palms. Keep your palms covering your eyes for 10 seconds. Open your eyes and repeat as necessary</p>
                <p class="para font-open-sans">Stretch your arm out and place your thumb in the hitchhiker position. Focus your vision on your thumb while you slowly bring your outstretched arm closer to your body, until your thumb is about five inches from your face. Slowly zoom the thumb back to its original position, focusing on it with your eyes all the while</p>
                <img class="img-responsive" src="assets/images/img-9.png" alt="Our Tips 11"/><br />
            </div> 
        </div>
    </div>
</section>

<!-- Starting of Exercise your eyes    -->
<section class="Exercise-wrapper ">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <p class="para font-open-sans">
                    <span class="default-black">Do not read in dim light. </span>
                    Reading in dim light can cause eye strain but will not damage your eyes. If your eyes feel tired, stop for a while and take a break.  
                </p>
                <img class="img-responsive" src="assets/images/img-10.png" alt="Our Tips 12"/><br />
                <p class="para font-open-sans">
                    <span class="default-black">Do not look at a bright light directly.</span> 
                    Never focus your eyes on the sun directly, as it can damage your eyes. </p>
            </div> 
        </div>
    </div>
</section>

@include('layouts/makeAppointment')
@stop

