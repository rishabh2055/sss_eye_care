@extends('layouts/defaultLayout')
@section('content')

<!-- Header -->
<header id="head">
    <img src="assets/images/contact.jpg" style="height: 525px;" alt="Contact Us">
</header>
<!-- /Header -->

<section class="breadcrumb-wrapper">
    <div class="container">
        <ol class="breadcrumb">
            <li><a href="{{URL::to('/')}}">Home</a></li>
            <li class="active">Contact</li>
        </ol>
    </div>
</section>
<!-- container -->
<div class="container">

    <div class="row">
        <header class="page-header">
            <h1 class="page-title">Contact us</h1>
        </header>

    </div>
</div>	<!-- /container -->

<div class="contact-us-wrapper contact-us-page" id="contactus">
    <div class="new-overlay">

    </div>
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="col-lg-12">
                    <!--<h2>Contact Us.</h2>-->
                    <p>We don't just say that you are our top priority, we really mean it and that's why we want your comments that will help us serve you better. So please give us your details and comments because we would like to hear from you.</p>
                    <div id="head_msg"></div>
                </div>
                <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">
                    <form method="post" class="form-horizontal" id="contactUs">
                        <div class="row">
                            <div class="form-group">
                                <!--<label for="name" class="col-sm-4 control-label">Full Name</label>-->
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" id="full_name" name="full_name" placeholder="Full Name">  
                                    <label class="focused">Name</label>
                                </div>
                            </div>
                            <div class="form-group">
                                <!--<label for="email" class="col-sm-4 control-label">Email</label>-->
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" id="user_email" name="user_email" placeholder="Your Email">
                                    <label class="focused">Email</label>
                                </div>
                            </div>
                            <div class="form-group">
                                <!--<label for="mobile" class="col-sm-4 control-label">Contact No.</label>-->
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" id="user_mobile" name="user_mobile" placeholder="Your Contact No.">
                                    <label class="focused">Contact</label>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-sm-10">
                                    <textarea placeholder="Type your message here..." class="form-control" name="comment" id="comment" rows="4"></textarea>
                                </div>
                            </div>
                        </div>
                        <br>

                        <button type="submit" class="btn btn-default">Send Message</button>

                    </form>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                    <h3 class="default-red">Registered/Corporate Office</h3>
                    <p class="para font-open-sans">
                        <span class="default-red">Address:<br /> </span><a style="color:#FAE093;" href="https://www.google.co.in/maps/place/Batuk+Krishna+Banerjee+Rd,+Beli+Goan,+Allahabad,+Uttar+Pradesh+211002/@25.4740142,81.8440084,17z/data=!3m1!4b1!4m2!3m1!1s0x399acaf2d39551df:0xb31d41e2382c2f5b?hl=en" target="_blank">4 P.C. Banerjee Road, Allen Ganj, Allahabad</a></p>
                    <p class="para font-open-sans"><span class="default-red">Phone No: </span>9450618211</p>
                    <p class="para font-open-sans"><span class="default-red">Email: </span>sharaneyecare@gmail.com</p>
                </div>

            </div>
        </div>
    </div>
</div>
{{HTML::script('assets/js/contact.js')}}
@stop
