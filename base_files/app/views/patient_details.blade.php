@extends('layouts/defaultLayout')
@section('content')

<header id="head" class="secondary"></header>

<div class="home-wrapper">
    <div class="container">
        <div class="row">
            <h3>Welcome <?php echo ucfirst(Auth::user()->username); ?></h3>
            <!-- Article main content -->
            <article class="col-xs-12 maincontent">
                <header class="page-header">
                    <h1 class="page-title">Patient Information</h1>
                </header>
                <div id="head_msg"></div>
                <div class="col-md-12 col-sm-12">
                    <div class="panel panel-default">
                        <div class="panel-body">

                            <form method="post" class="form-horizontal" id="patientForm">
                                <div class="row">
                                    <div class="col-md-6 col-sm-12">
                                        <div class="form-group">
                                            <label for="name" class="col-sm-4 control-label">Name</label>
                                            <div class="col-sm-8">
                                                <input type="text" class="form-control" id="patient_name" name="patient_name" placeholder="Patient Name" tabindex="1">  
                                                <label class="focused">Name</label>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="name" class="col-sm-4 control-label">Phone No</label>
                                            <div class="col-sm-8">
                                                <input type="text" class="form-control" id="patient_phone" name="patient_phone" placeholder="Patient Phone No" tabindex="3">  
                                                <label class="focused">Phone</label>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="gender" class="col-sm-4 control-label">Gender</label>
                                            <div class="col-sm-8">
                                                <label class="radio-inline">
                                                    <input type="radio" id="male" name="gender" value="male" tabindex="5"> Male
                                                </label>
                                                <label class="radio-inline">
                                                    <input type="radio" id="female" name="gender" value="female" tabindex="6"> Female
                                                </label>
                                                <label for="gender" class="error"></label>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="complaints_duration" class="col-sm-4 control-label">Duration Of Complaints</label>
                                            <div class="col-sm-4">
                                                <select id="complaints_duration_month" name="complaints_duration_month" class="form-control" tabindex="9">
                                                    <option value="">Months</option>
                                                    <option value="1">1</option>
                                                    <option value="2">2</option>
                                                    <option value="3">3</option>
                                                    <option value="4">4</option>
                                                    <option value="5">5</option>
                                                    <option value="6">6</option>
                                                    <option value="7">7</option>
                                                    <option value="8">8</option>
                                                    <option value="9">9</option>
                                                    <option value="10">10</option>
                                                    <option value="11">11</option>
                                                    <option value="12">12</option>
                                                </select>
                                            </div>
                                            <div class="col-sm-4">
                                                <select id="complaints_duration_year" name="complaints_duration_year" class="form-control" tabindex="10">
                                                    <option value="">Year</option>
                                                    <option value="1">1</option>
                                                    <option value="2">2</option>
                                                    <option value="3">3</option>
                                                    <option value="4">4</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="highbp" class="col-sm-4 control-label">High Blood Pressure</label>
                                            <div class="col-sm-8">
                                                <label class="radio-inline">
                                                    <input type="radio" id="yes_highbp" name="highbp" value="yes" tabindex="13"> Yes
                                                </label>
                                                <label class="radio-inline">
                                                    <input type="radio" id="no_highbp" name="highbp" value="no" tabindex="14"> No
                                                </label>
                                                <label for="highbp" class="error"></label>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="lowbp" class="col-sm-4 control-label">Low Blood Pressure</label>
                                            <div class="col-sm-8">
                                                <label class="radio-inline">
                                                    <input type="radio" id="yes_lowbp" name="lowbp" value="yes" tabindex="21"> Yes
                                                </label>
                                                <label class="radio-inline">
                                                    <input type="radio" id="no_lowbp" name="lowbp" value="no" tabindex="22"> No
                                                </label>
                                                <label for="lowbp" class="error"></label>
                                            </div>
                                        </div>
                                    </div> 
                                    <div class="col-md-6 col-sm-12">
                                        <div class="form-group">
                                            <label for="user_email" class="col-sm-4 control-label">Age</label>
                                            <div class="col-sm-8">
                                                <input type="text" class="form-control" id="patient_age" name="patient_age" placeholder="Patient Age" tabindex="2">  
                                                <label class="focused">Age</label>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="user_email" class="col-sm-4 control-label">Email</label>
                                            <div class="col-sm-8">
                                                <input type="text" class="form-control" id="patient_email" name="patient_email" placeholder="Patient Email" tabindex="4">  
                                                <label class="focused">Email</label>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="complaints" class="col-sm-4 control-label">Complaints</label>
                                            <div class="col-sm-8">
                                                <label class="radio-inline">
                                                    <input type="radio" id="left_eye" name="complaints" value="left_eye" tabindex="7"> Left Eye
                                                </label>
                                                <label class="radio-inline">
                                                    <input type="radio" id="left_eye" name="complaints" value="right_eye" tabindex="8"> Right Eye
                                                </label>
                                                <label for="complaints" class="error"></label>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="diabetes" class="col-sm-4 control-label">Diabetes</label>
                                            <div class="col-sm-8">
                                                <label class="radio-inline">
                                                    <input type="radio" id="yes_diabetes" name="diabetes" value="yes" tabindex="11"> Yes
                                                </label>
                                                <label class="radio-inline">
                                                    <input type="radio" id="no_diabetes" name="diabetes" value="no" tabindex="12"> No
                                                </label>
                                                <label for="diabetes" class="error"></label>
                                            </div>
                                        </div>
                                        <div class="form-group" id="diabetes_duration_section">
                                            <label for="since" class="col-sm-4 control-label">Since</label>
                                            <div class="col-sm-4">
                                                <select id="diabetes_duration_month" name="diabetes_duration_month" class="form-control" tabindex="15">
                                                    <option value="">Months</option>
                                                    <option value="1">1</option>
                                                    <option value="2">2</option>
                                                    <option value="3">3</option>
                                                    <option value="4">4</option>
                                                    <option value="5">5</option>
                                                    <option value="6">6</option>
                                                    <option value="7">7</option>
                                                    <option value="8">8</option>
                                                    <option value="9">9</option>
                                                    <option value="10">10</option>
                                                    <option value="11">11</option>
                                                    <option value="12">12</option>
                                                </select>
                                            </div>
                                            <div class="col-sm-4">
                                                <select id="diabetes_duration_year" name="diabetes_duration_year" class="form-control" tabindex="16">
                                                    <option value="">Year</option>
                                                    <option value="1">1</option>
                                                    <option value="2">2</option>
                                                    <option value="3">3</option>
                                                    <option value="4">4</option>
                                                </select>
                                            </div>
                                        </div>                                       
                                        <div class="form-group">
                                            <label for="thyroid" class="col-sm-4 control-label">Thyroid Problems</label>
                                            <div class="col-sm-8">
                                                <label class="radio-inline">
                                                    <input type="radio" id="yes_typhoid" name="typhoid" value="yes" tabindex="19"> Yes
                                                </label>
                                                <label class="radio-inline">
                                                    <input type="radio" id="no_typhoid" name="typhoid" value="no" tabindex="20"> No
                                                </label>
                                                <label for="typhoid" class="error"></label>
                                            </div>
                                        </div>
                                    </div> 
                                </div>

                                <hr>

                                <div class="row">
                                    <div class="col-lg-12 text-right">
                                        <input class="btn btn-default" type="submit" value="Submit">
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>

                </div>

            </article>
            <!-- /Article -->
        </div>
    </div>
</div>
{{HTML::script('assets/js/patient_details.js')}}
@stop