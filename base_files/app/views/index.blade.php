@extends('layouts/defaultLayout')
@section('content')
<!-- Carousel
================================================== -->
<div id="myCarousel" class="carousel slide hidden-xs" data-ride="carousel">
    <!-- Indicators -->
    <ol class="carousel-indicators">
        <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
        <li data-target="#myCarousel" data-slide-to="1"></li>
        <li data-target="#myCarousel" data-slide-to="2"></li>
        <li data-target="#myCarousel" data-slide-to="3"></li>
        <li data-target="#myCarousel" data-slide-to="4"></li>
    </ol>
    <div class="carousel-inner" role="listbox">
        <div class="item active first">
            <img src="assets/images/slide1.jpg" alt="First slide">
            <div class="container">
                <div class="carousel-caption">
                    <h2 class="z2 animated" style="margin-top: 80px;">Welcome To </h2><h2 id="z2" class="z1 animated default-red">SHARAN</h2> <h2  id="z3" class="z3 animated default-blue">SUPER SPECIALITY EYE CARE</h2>
                    <p class="z4" id="z4"><a class="btn btn-lg btn-primary z4 animated" href="{{URL::to('/about')}}" role="button">About Us</a></p>
                </div>
            </div>
        </div>
        <div class="item second">
            <img src="assets/images/slide2.jpg" alt="Second slide">
            <div class="container">
                <div class="carousel-caption">
                    <h2 class="default-blue animated">Our Team.</h2>
                    <p class="animated">Get in touch to book your next eye examination <br/>with your Doctor.</p>
                    <p><a class="btn btn-lg btn-primary book-appointment animated" href="#appointmentWrapper" role="button">Book an Appointment</a></p>
                </div>
            </div>
        </div>
        <div class="item third">
            <img src="assets/images/slide4.JPG" alt="Third slide">
            <div class="container">
                <div class="carousel-caption">
                    <h2 class="animated">Free Eye Check up And Low Cost<br/> <span class="default-blue">Cataract Surgery</span> </h2>
                    <p class="animated">Check Up Will Be On Every Wednesday <br/><a class="default-blue">9 A.M To 11 A.M</a> </p>
                    <p><a class="btn btn-lg btn-primary" href="{{URL::to('/about')}}" role="button">View Details</a></p>
                </div>
            </div>
        </div>
        <div class="item fourth">
            <img src="assets/images/slide5.JPG" alt="Fourth slide">
            <div class="container">
                <div class="carousel-caption">
                    <h2 class="animated"><span class="default-white">Protecting your eyes starts with the food on your plate. Nutrients such as omega-3 fatty acids, lutein, zinc, and<br /> vitamins C and E might help ward off age-related vision problems such as<br /> macular degeneration and cataracts, studies show. </span> </h2>
                    <p><a class="btn btn-lg btn-primary" href="{{URL::to('/our_tips')}}" role="button">View Details</a></p>
                </div>
            </div>
        </div>
        <div class="item fifth">
            <img src="assets/images/slider6.jpg" alt="Fourth slide">
            <div class="container">
                <div class="carousel-caption">
                    <h2 class="animated"><span class="default-blue">Our Super Speciality Services:</span> </h2>
                    <p class="animated"><span class="default-white">ROP (Retinopathy of Prematurity) Screening & Management for new borns & <br /> Post Cataract Surgery Complications</span></p>
                    <p><a class="btn btn-lg btn-primary" href="{{URL::to('/services')}}" role="button">View Details</a></p>
                </div>
            </div>
        </div>
    </div>
    <a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">
        <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
        <span class="sr-only">Previous</span>
    </a>
    <a class="right carousel-control" href="#myCarousel" role="button" data-slide="next">
        <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
        <span class="sr-only">Next</span>
    </a>
</div><!-- /.carousel -->

<!-- Mobile view for top section -->
<div class="visible-xs" id="mobile_view_main_image">
    <h2 class="font-open-sans ">Welcome to <span class="default-red bold">SHARAN</span> <span class="default-blue bold">SUPER SPECIALITY EYE CARE</span></h2>
</div>

<!-- Intro -->

<section class="bg-white welcome-wrapper">
    <div class="container text-center">
        <br> <br>
        <h2 class="font-open-sans hidden-xs">Welcome to <span class="default-red bold">SHARAN</span> <span class="default-blue bold">SUPER SPECIALITY EYE CARE</span></h2>

        <h3 class="font-open-sans">Our Eye Clinic - Focused on Quality</h3>
        <p class="para font-open-sans">
            Thank you for your interest in the SHARAN SUPER SPECIALITY EYE CARE.We use the latest instruments to measure your vision and detect eye disease before it can rob you of your precious sight. Our commitment is to provide compassionate personalized treatment for disorders of the retina & vitreous using the highest quality techniques. We have extensive experience in treating a wide range of retinal diseases. 
            Before we can explain about your condition, it’s important to understand how the eye works when it’s working properly.
            We better serve you last 4 years.</p>
        <h2 class="text-center" style="line-height: 50px; margin-top: 50px; margin-bottom: 50px;">" Free Eye checkup & low cost Cataract Surgery<br/> Every Wednesday 9am to 11am. "</h2>


        <p><a class="btn btn-default" href="{{URL::to('/about')}}" role="button">View details &raquo;</a></p>
    </div>
</section>

<!-- /Intro-->

<!-- Highlights - jumbotron -->
<section class="bg-gray">
    <!--<div class="jumbotron top-space">-->
    <div class="container marketing">

        <div class="row">
            <div class="col-lg-4 text-center">
                <img style="width: 250px; height: 250px;" alt="Generic placeholder image" src="assets/images/expertise.jpg" class="img-circle">
                <h2 class="default-blue">Experience & Expertise</h2>
                <p class="para">With over four years of service and lakhs of patients catered to, SHARAN SUPER SPECIALITY EYE CARE promises to provide you with a reliable and candid medical opinion. </p>
                <p><a class="btn btn-default" href="{{URL::to('/facilities')}}" role="button">View details &raquo;</a></p>
            </div><!-- /.col-lg-4 -->
            <div class="col-lg-4 text-center">
                <img style="width: 250px; height: 250px;" alt="Generic placeholder image" src="assets/images/services.jpg" class="img-circle">
                <h2 class="default-blue">Speciality Services</h2>
                <p class="para">Trained ophthalmologists provide high quality sub-speciality services in the fields of cataract and refractive surgery, glaucoma and retina amongst many others.</p>
                <p><a class="btn btn-default" href="{{URL::to('/services')}}" role="button">View details &raquo;</a></p>
            </div><!-- /.col-lg-4 -->
            <div class="col-lg-4 text-center">
                <img style="width: 250px; height: 250px;" alt="Generic placeholder image" src="assets/images/tips.jpg" class="img-circle">
                <h2 class="default-blue">Our Tips</h2>
                <p class="para">Protecting your eyes starts with the food on your plate. Nutrients such as omega-3 fatty acids, lutein, zinc, and vitamins C and E might help ward off age-related vision problems such as macular degeneration and cataracts, studies show. </p>
                <p><a class="btn btn-default" href="{{URL::to('/our_tips')}}" role="button">View details &raquo;</a></p>
            </div><!-- /.col-lg-4 -->
        </div> <!-- /row  -->

    </div>
    <!--</div>-->
</section>
<!-- /Highlights -->


<!--  Menu Wrapper Starts  -->
<section class="menu-wrapper" id="popular-food">
    <div class="container">
        <div class="row">
            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 text-center">
                <div class="menu-item center-block">
                    <div class="menu-img">
                        <img src="assets/images/abhi.JPG" alt="menu-1"/>
                    </div>
                </div>
                <div class="menu-details">
                    <h3>Dr. Abhishek Sharan <span>(M.B.B.S., M.S. (Gold Medalist))</span></h3>
                    <p>Fellow in Retina</p>
                    <p>Vitreous/Surgery </p>
                    <p>Aravind Eye Hospital, Madurai</p>
                </div>
            </div>
            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 text-center">
                <div class="menu-item center-block">
                    <div class="menu-img">
                        <img src="assets/images/jay.png" alt="menu-2"/>
                    </div>
                </div>
                <div class="menu-details">
                    <h3>Dr. Jayita Sharan <span>(M.B.B.S., DNB (Ophthalmology), FICO)</span></h3>
                    <p>Fellow in Cornea</p>
                    <p>and Micro Surgery</p>
                    <p>Aravind Eye Hospital, Madurai</p>
                </div>
            </div>
        </div>
    </div>
    <div class="new-overlay"></div>
</section>

<div class="testimonials">  
    <div class="container inner">
        <div class="section-title text-center">
            <h2>Image Gallery</h2>
            <span class="icon"><i class="icon-picons-diamond"></i></span>
        </div>
        <ul class="bxslider">
            <li><img src="assets/images/12/100_1629.JPG" /></li>
            <li><img src="assets/images/12/100_1632.JPG" /></li>
            <li><img src="assets/images/photos/100_1609.JPG" /></li>
            <li><img src="assets/images/12/100_1589.JPG" /></li>
            <li><img src="assets/images/photos/100_1618.JPG" /></li>
            <li><img src="assets/images/photos/100_1609.JPG" /></li>
            <li><img src="assets/images/12/100_1629.JPG" /></li>
            <li><img src="assets/images/photos/100_1627.JPG" /></li>
        </ul>
        <!-- /.owlcarousel -->
        <div class="divide50"></div>
        <div class="text-center"> 
            <a class="btn btn-default" style="margin-top: 50px;" href="{{URL::to('photo_gallery')}}">See All Image</a>
        </div>
    </div>
</div>

<script type="text/javascript">
    $(document).ready(function () {
         var height = $(window).height();
        $(".carousel-inner > .item > img").css("height", height);
        $("#mobile_view_main_image").css("height", height);
        $(".carousel .item").css("height", height);
        $('.bxslider').bxSlider({
            slideWidth: 400,
            minSlides: 2,
            maxSlides: 4,
            moveSlides: 1,
            slideMargin: 20
        });

    });
</script>
@include('layouts/makeAppointment')
@stop
