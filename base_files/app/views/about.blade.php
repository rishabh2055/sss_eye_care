
@extends('layouts/defaultLayout')
@section('content')
<header id="head"><img src="assets/images/about-us-banner.jpg" style="height: 525px;" alt="About Us"></header>
<!-- container -->
<section class="breadcrumb-wrapper">
    <div class="container">
        <ol class="breadcrumb">
            <li><a href="{{URL::to('/')}}">Home</a></li>
            <li class="active">About</li>
        </ol>
    </div>
</section>


<div class="container">
    <header class="page-header">
        <h1 class="page-title default-red">About us</h1>
    </header>
    <div class="well">
        <h3 class="font-open-sans default-blue">Retina and Vitreous Surgery</h3>
        <p class="para font-open-sans">Dr. Abhishek Sharan (Retina and Vitreous Surgery ) is experienced in the various surgeries to correct diseases and disorders of the retina  
and macula (the center of the retina). </p>
        <h3 class="font-open-sans default-blue">Cornea Microsurgey</h3>
        <p class="para font-open-sans">Dr. Jayita Sharan is experienced in the various surgeries to correct diseases and disorders of the
    Cornea & Microsurgey.</p>
    </div>
    <h2 class="font-open-sans default-blue">The kinds of problems that affect the retina or macular include:</h2>    
    <br/>

    <div class="row">
        <div class="col-sm-6">
            <br/><br/>
            <br/><br/><br/>            <ul class="about list-unstyled diseases-ul">
                <li data-popbox="pop1"><i class="fa fa-hand-o-right"></i> <a href="javascript:void(0)" value="Diabetic-Retinopathy-compare-internal">Diabetic Retinopathy</a></li>
                <li data-popbox="pop1"><i class="fa fa-hand-o-right"></i> <a href="javascript:void(0)" value="Retioal-tears-vitreous-detacment">Retinal Tears/Viterous Detachment</a></li>
                <li data-popbox="pop1"><i class="fa fa-hand-o-right"></i> <a href="javascript:void(0)" value="Mascular-Epirential-Membranes">Macular Epirential Membranes</a></li>
                <li data-popbox="pop1"><i class="fa fa-hand-o-right"></i> <a href="javascript:void(0)" value="Retinopathy-of-Prematurity">Retinopathy of Prematurity</a> </li>
                <li data-popbox="pop1"><i class="fa fa-hand-o-right"></i> <a href="javascript:void(0)" value="Venous-occlusive-Disease">Venous occlusive Disease</a></li>
                <li data-popbox="pop1"><i class="fa fa-hand-o-right"></i> <a href="javascript:void(0)" value="Age-Related-Mascular-Degeneration">Age-Related Macular Degeneration</a> </li>
                <li data-popbox="pop1"><i class="fa fa-hand-o-right"></i> <a href="javascript:void(0)" value="Mascular Holes">Macular Holes</a></li>
                <li data-popbox="pop1"><i class="fa fa-hand-o-right"></i> <a href="javascript:void(0)" value="Cataract-Surgery-Complications">Cataract Surgery Complications</a></li>
            </ul>
        </div>
        <div class="col-sm-6">
            <div class="dark-bg-wrapper">

                <div class="overlay"></div>

            </div>
        </div>
    </div>
    <hr/>
    <div class="row">
        <div class="col-xs-6">
            <p class="para font-open-sans">The treatment of these issues may require a vitrectomy, which is the surgical removal of the jelly-like substance near the back of the eye. A vitrectomy can be performed to clear blood and scar tissue in the eye that could be related to previous eye surgery.</p>

            <p class="para font-open-sans">In addition to a vitrectomy, <span class="default-red bold">SHARAN</span> <span class="default-blue bold">SUPER SPECIALITY EYE CARE</span> retinal experts my use other techniques to treat you.  These techniques may include:</p>
        </div>
        <div class="col-xs-6">
            <div class="well">
                <ul class="about list-unstyled">
                    <li><i class="fa fa-hand-o-right"></i> Sealing blood vessels with a laser</li>
                    <li><i class="fa fa-hand-o-right"></i> Using a gas bubble to seal the macular hole and to keep the retina in position</li>
                    <li><i class="fa fa-hand-o-right"></i> Using silicone oil to keep the retina in position</li>
                </ul>
            </div>
        </div>
    </div>


    <hr>
    <section class="cornea-wrapper">
        <h2 class="font-open-sans default-blue">Cornea</h2>
        <p class="para font-open-sans">
            The cornea is the transparent front part of the eye that cover the iris, pupil & anterior chamber. The cornea with anterior chamber & lens, refract light, with the Cornea accounting for approximately two third of eye total optical power.
        </p>
        <p class="para font-open-sans">
            The cornea is the eyes outermost layer. It is the clear dome-shaped surface that covers the front of the eye. Cornea is the highly organized group of cells & protein.
        </p>
        <br />
        <p class="para font-open-sans">Some disease & disorder of the cornea are-</p>
        <div class="col-sm-6">
            <br/><br/>
            <br/><br/><br/>
            <ul class="about list-unstyled">
                <li><i class="fa fa-hand-o-right"></i> Conjunctivitis</li>
                <li><i class="fa fa-hand-o-right"></i> Corneal Pystrophies</li>
                <li><i class="fa fa-hand-o-right"></i> Dry Eye</li>
                <li><i class="fa fa-hand-o-right"></i> Keratoconus</li>
                <li><i class="fa fa-hand-o-right"></i> Corneal Infections</li>
                <li><i class="fa fa-hand-o-right"></i> Pterygiun</li>
            </ul>
        </div>
        <div class="col-sm-6">
            <div class="dark-bg-cornea-wrapper">

                <div class="overlay"></div>

            </div>
        </div>
        <br/>
        <hr>
        <h2 class="font-open-sans default-blue">Treatment & Management</h2>
        <h3 class="font-open-sans default-blue">Surgical procedures</h3>
        <p class="para font-open-sans">
            Various refractive eye surgery techniques change the shape of cornea in order to reduce the need for corrective lenses or otherwise improve the refractive state of the eye. In many of the techniques used today, reshaping of the cornea is performed by photoablation using the excimer laser.
        </p>
        <br />
        <p class="para font-open-sans">
            If the corneal stroma develops visually significant opacity, irregularity, or edema, a cornea of a deceased donor can be transplanted. Because there are no blood vessels in the cornea, there are also few problems with rejection of the new cornea. There are also synthetic corneas (Keratoprostheses) in development. Most are merely plastic inserts, but there also those composed of biocompatible synthetic material that encourage tissue ingrowth into the synthetic cornea, thereby promoting biointegration.
        </p>
        <br />
        <p class="para font-open-sans">
            Other method, such as magnetic deformable membranes & optically coherent transcranial magnetic stimulation of the human retina are still in very early stages of research.
        </p>

        <h3 class="font-open-sans default-blue">Non- Surgical Procedures</h3>
        <p class="para font-open-sans">
            Orthokeratology  is a method using specialized hard or rigid gas-perneable contact lenses to transiently reshape the cornea in order to improve the refractive state of eye or reduce the need for eyeglasses & contact lenses.
        </p>
        <br /><hr>
        <h2 class="font-open-sans default-blue">Microsurgery</h2>
        <p class="para font-open-sans">
            Microsurgery surgery is the surgery that is performed on very small structure with the specialized instrument under a microscope. During the recent decade the microscope & instrument used have been remarkably improved. This is true for the microscope itself & it accessories, but even more so for many of our remote controlled instruments, such as rototrephines for perforating grafts, rotokeratomes & rotoknives, & recently for the new automatic lamellarkeratom. Better cutting quality leads to more precise wound edges, to better coaptation & to better optical results. These systems also can be useful in cataract surgery especially in the implantation of new intraocular lenses.
        </p>

    </section>

    <p class="para font-open-sans">We also provide the treatment of cornea, cataract an external  diseases provide medical & surgical evaluation & management for cataract & external diseases problem including-cataract, corneal transplantation, dry eye, contact lenses & excimer laser therapy.</p>

    <p class="para font-open-sans"><span class="default-red bold">SHARAN</span> <span class="default-blue bold">SUPER SPECIALITY EYE CARE</span>  has been serving its patients with the highest degree of patient care. With our innovative, all-in-one approach, Sharan Super Speciality Eye Care is redefining the traditional eye care experience and setting the standard in care. Our patient first approach and our uncompromising promise of better doctors, better staff and better care has helped define Sharan Super Speciality Eye Care as a proactive, growing medical practice at the forefront of the eye health industry.</p>
    <hr>
    <section class="mission-wrapper">
        <h2 class="font-open-sans default-blue">Our Mission</h2>
        <p class="para font-open-sans"><span class="default-red bold">SHARAN</span> <span class="default-blue bold">SUPER SPECIALITY EYE CARE</span>  Professionals are dedicated to meet your needs with compassion and respect. We provide the highest quality eye care to you and your family now and for a lifetime. We value the trust you place in us because at Sharan Super Speciality Eye Care you are our most important patient.</p>
    </section>
    <hr/>
    <section class="timing-wrapper">       
        <div class="row">
            <h2 class="font-open-sans default-blue padL15">Our Doctor's Time</h2>
            <div class="col-lg-4 col-md-4 col-sm-8 col-xs-8">               
                <p class="para font-open-sans"><span class="default-red">SHARAN</span> <span class="default-blue">SUPER SPECIALITY EYE CARE</span></p><br/>
                <p class="para font-open-sans">Morning Time : <a class="default-blue">10 A.M To 11 A.M</a> </p>

                <p class="para font-open-sans">Evening Time : <a class="default-blue">4 P.M To 6 P.M</a> </p><br />
                <p class="para font-open-sans">Address : 4 P.C. Banerjee Road, Allen Ganj, <br />Allahabad - 211002</p>
            </div>
            <div class="col-lg-4 col-md-4 col-sm-8 col-xs-8 text-center">
                <img src="assets/images/diseases/looking-though-hole.gif" alt="See Address">
            </div>
            <div class="col-lg-4 col-md-4 col-sm-8 col-xs-8">
                <p class="para font-open-sans">Evening Tuesday And Thursday : <br /><a class="default-blue">12 P.M To 1 P.M</a> </p>
                <br />
                <div>
                    <!--<p class="para font-open-sans">Free Eye Check up And Low Cost <span class="default-blue">Cataract Surgery</span> </p>-->
                    <!--<p class="para font-open-sans">Check Up Will Be On Every Wednesday <br/><a class="default-blue">9 A.M To 11 A.M</a> </p>-->
                    <p class="para font-open-sans">Address: 29-B, Priti Hospital, Panna Lal Road,<br/> Allahabad(Behind of Company Garden)</p>
                </div>
            </div>
        </div>     


    </section>
    <div id="pop1" class="popbox">
        <img alt="Disease Images">
    </div>
</div>
@include('layouts/makeAppointment')
@stop