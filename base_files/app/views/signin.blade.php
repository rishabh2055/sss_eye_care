
@extends('layouts/defaultLayout')
@section('content')

<!--<section class="signin-wrapper">-->
<!--<header id="head" class="secondary"></header>-->

<!-- container -->
<section class="login-outer-wrapper">
    <div class="container">
        <div class="row">

            <!-- Article main content -->

            <div class="col-md-6 col-sm-8 login-wrapper">
                <div class="panel panel-default signin-wrapper">
                    <div class="panel-body">
                        <h3 class="text-center">Sign in to your account</h3>
                        <!--<p class="text-center text-muted">To add new user, <a href="{{URL::to('signup')}}">Click here</a></p>-->
                        <hr>
                        @if (Session::get('loginError'))
                        <div class="alert alert-danger">{{ Session::get('loginError') }}</div>
                        @endif
                        <form method="post" class="form-horizontal" action="login">
                            <div class="form-group">
                                <!--<label for="name" class="col-sm-4 control-label">Username</label>-->
                                <div class="col-sm-12 left-inner-addon">
                                    <i class="fa fa-user"></i>
                                    <input type="text" class="form-control" id="username" name="username" placeholder="Username">  
                                    <label class="focused">Username</label>
                                    <span class="error">{{ $errors->first('username') }}</span>
                                </div>
                            </div>
                            <div class="form-group left-inner-addon">
                                <!--<label for="password" class="col-sm-4 control-label">Password</label>-->
                                <div class="col-sm-12">
                                    <i class="fa fa-key"></i>
                                    <input type="password" class="form-control" id="password" name="password" placeholder="Password">
                                    <label class="focused">Password</label>
                                    <span class="error">{{ $errors->first('password') }}</span>
                                </div>
                            </div>

                            <hr>

                            <div class="row">
                                <div class="col-lg-8">
                                    <b><a href="">Forgot password?</a></b>
                                </div>
                                <div class="col-lg-4 text-right">
                                    <button class="btn btn-default" type="submit">Sign in</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>

        </div>
    </div>
</section>

@stop