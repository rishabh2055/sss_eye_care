@extends('layouts/defaultLayout')
@section('content')

<header id="head"><img src="assets/images/photo_gallery.jpg" style="height: 525px;" alt="Photo Gallery"></header>
<!-- container -->
<section class="breadcrumb-wrapper">
    <div class="container">
        <ol class="breadcrumb">
            <li><a href="{{URL::to('/')}}">Home</a></li>
            <li class="active">Photo Gallery</li>
        </ol>
    </div>
</section>
<div class="container">
    <header class="page-header">
        <h1 class="page-title default-red">Photo Gallery</h1>
    </header>
    <ul class="row unstyled image-gallery">
        <li class="col-lg-4 col-md-2 col-sm-3 col-xs-4">
            <img class="img-responsive" src="assets/images/12/100_1629.JPG" alt="Photo 1">
        </li>
        <li class="col-lg-4 col-md-4 col-sm-6 col-xs-6">
            <img class="img-responsive" src="assets/images/12/100_1632.JPG" alt="Photo 2">
        </li>
        <li class="col-lg-4 col-md-4 col-sm-6 col-xs-6">
            <img class="img-responsive" src="assets/images/12/100_1633.JPG"  alt="Photo 3">
        </li>
        <li class="col-lg-4 col-md-4 col-sm-6 col-xs-6">
            <img class="img-responsive" src="assets/images/photos/100_1609.JPG"  alt="Photo 4">
        </li>
        <li class="col-lg-4 col-md-4 col-sm-6 col-xs-6">
            <img class="img-responsive" src="assets/images/photos/100_1627.JPG"  alt="Photo 5">
        </li>
        <li class="col-lg-4 col-md-4 col-sm-6 col-xs-6">
            <img class="img-responsive" src="assets/images/photos/100_1585.JPG"  alt="Photo 6">
        </li>       
        <li class="col-lg-4 col-md-4 col-sm-6 col-xs-6">
            <img class="img-responsive" src="assets/images/photos/100_1631.JPG"  alt="Photo 7">
        </li>
        <li class="col-lg-4 col-md-4 col-sm-6 col-xs-6">
            <img class="img-responsive" src="assets/images/photos/100_1618.JPG"  alt="Photo 8">
        </li>
        <li class="col-lg-4 col-md-4 col-sm-6 col-xs-6">
            <img class="img-responsive" src="assets/images/photos/100_1641.JPG"  alt="Photo 9">
        </li>
        <li class="col-lg-4 col-md-4 col-sm-6 col-xs-6">
            <img class="img-responsive" src="assets/images/photos/100_1611.JPG"  alt="Photo 10">
        </li>
        <li class="col-lg-4 col-md-4 col-sm-6 col-xs-6">
            <img class="img-responsive" src="assets/images/12/100_1564.JPG"  alt="Photo 11">
        </li>
        <li class="col-lg-4 col-md-4 col-sm-6 col-xs-6">
            <img class="img-responsive" src="assets/images/12/100_1589.JPG"  alt="Photo 12">
        </li>
    </ul>
</div>
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">         
            <div class="modal-body">                
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<script>
    $(document).ready(function () {
        $('li img').on('click', function () {
            var src = $(this).attr('src');
            var img = '<img src="' + src + '" class="img-responsive"/>';

            //start of new code new code
            var index = $(this).parent('li').index();

            var html = '';
            html += img;
            html += '<div style="height:25px;clear:both;display:block;">';
            html += '<a class="controls next" href="' + (index + 2) + '">next &raquo;</a>';
            html += '<a class="controls previous" href="' + (index) + '">&laquo; prev</a>';
            html += '</div>';

            $('#myModal').modal();
            $('#myModal').on('shown.bs.modal', function () {
                $('#myModal .modal-body').html(html);
                //new code
                $('a.controls').trigger('click');
            })
            $('#myModal').on('hidden.bs.modal', function () {
                $('#myModal .modal-body').html('');
            });




        });
        
        //new code
    $('body').on('click', 'a.controls', function () {
        var index = $(this).attr('href');
        var src = $('ul.row li:nth-child(' + index + ') img').attr('src');

        $('.modal-body img').attr('src', src);

        var newPrevIndex = parseInt(index) - 1;
        var newNextIndex = parseInt(newPrevIndex) + 2;

        if ($(this).hasClass('previous')) {
            $(this).attr('href', newPrevIndex);
            $('a.next').attr('href', newNextIndex);
        } else {
            $(this).attr('href', newNextIndex);
            $('a.previous').attr('href', newPrevIndex);
        }

        var total = $('ul.row li').length + 1;
        //hide next button
        if (total === newNextIndex) {
            $('a.next').hide();
        } else {
            $('a.next').show()
        }
        //hide previous button
        if (newPrevIndex === 0) {
            $('a.previous').hide();
        } else {
            $('a.previous').show()
        }


        return false;
    });

    });



</script>

@include('layouts/makeAppointment')
@stop
