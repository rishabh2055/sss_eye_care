<?php
/**
 * PatientInformationController class
 * Controller uesd for patient appointment get And post functions.
 * @version      0.0.1
 * @since        0.0.1
 * @access       public
 */
class PatientInformationController extends BaseController {
    /**
     * Function used to show view file of patient information form
     * 
     * @access         public
     * @since          v1.0.0
     * @return         response
     */
    public function index(){
        return View::make('patient_details');
    }
    
    //--------------------------------------------------------------------------
    /**
     * Function used to submit patient details 
     * 
     * @access      public
     * @since       v1.0.0
     * @return      response
     */
    public function submitPatientDetails(){
        // default status response
        $this->arrResponse['status'] = ERR_DEFAULT;
        // array to define all posted array data
        $arrPostedData = Input::all();
        if (Patient::submitPatientDetails($arrPostedData)) {
            // details returned from server
            $this->arrResponse['status'] = SUCCESS;
            $this->arrResponse['message'] = "Patient Information Added Successfully !";
        } else {
            // database error
            $this->arrResponse['status'] = ERR_DATABASE;
            $this->arrResponse['message'] = "Database error ! try again later";
        }
        return Response::json($this->arrResponse);
    }
    //--------------------------------------------------------------------------
    /**
     * Function used to get patient details by patient id
     * 
     * @access      public
     * @since       v1.0.0
     * @return      response
     */
    public function patientDetailsByPatientId(){
        // default status response
        $this->arrResponse['status'] = ERR_DEFAULT;
        // array to define all posted array data
        $arrPostedData = Input::all();
        $patientDetails = Patient::getPatientsDetails($arrPostedData);
        if ($patientDetails) {
            // details returned from server
            $this->arrResponse['status'] = SUCCESS;
            $this->arrResponse['patients'] = $patientDetails;
        } else {
            // database error
            $this->arrResponse['status'] = ERR_DATABASE;
            $this->arrResponse['message'] = "Database error ! try again later";
        }
        return Response::json($this->arrResponse);
    }
    //--------------------------------------------------------------------------
    /**
     * Function used to get patient details by patient id
     * 
     * @access      public
     * @since       v1.0.0
     * @return      response
     */
    public function deletePatient(){
        // default status response
        $this->arrResponse['status'] = ERR_DEFAULT;
        // array to define all posted array data
        $arrPostedData = Input::all();
        $patientDetails = Patient::deletePatient($arrPostedData);
        if ($patientDetails) {
            // details returned from server
            $this->arrResponse['status'] = SUCCESS;
            $this->arrResponse['patients'] = $patientDetails;
        } else {
            // database error
            $this->arrResponse['status'] = ERR_DATABASE;
            $this->arrResponse['message'] = "Database error ! try again later";
        }
        return Response::json($this->arrResponse);
    }
    
    //--------------------------------------------------------------------------
    
    /**
     * Function used to show view file of patient information form
     * 
     * @access         public
     * @since          v1.0.0
     * @return         response
     */
    public function patients(){
        // get patient details
        $this->arrPageData['patients'] = Patient::getPatientsDetails(array());
        return View::make('patients', $this->arrPageData);
    }
}

// End of PatientInformationController class
// End of PatientInformationController.php file