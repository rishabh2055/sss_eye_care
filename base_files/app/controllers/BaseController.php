<?php

/**
 * Controller class BaseController.
 * 
 * @package		tric_softec
 * @version		1.0.0
 * @since		1.0.0
 */
class BaseController extends Controller {

    /**
     * Default layout to be used while generating
     * views.
     * 
     * @access	protected
     * @var		string
     */
    protected $layout = 'layouts.defaultLayout';

    /**
     * Array to store response values to be displayed.
     *
     * @access 		protected
     * @var 		array
     * @since 		0.0.1
     * */
    protected $arrResponse = array();
    
    /**
     * Array to store information to be sent to the
     * view.
     * 
     * @access	protected
     * @var		array
     * @since	1.0.0
     */
    protected $arrPageData = array();

    //-------------------------------------------------------------------------

    /**
     * Sets the error details in case of failed validation.
     * 
     * @access	protected
     * @param	array 		$data
     * @param	string		$objErrorMessages
     * @since	1.0.0
     */
    protected function setValidationErrorMessages($data, $objErrorMessages) {
        foreach ($data as $key => $value) {
            $this->arrResponse[$key] = $objErrorMessages->first($key);
        }
    }
    
    //--------------------------------------------------------------------------
    /**
     * Returns doctors time array
     * @access	protected
     * @since	1.0.0
     */
    protected function getDoctorTime(){
        $this->arrPageData['doctorTime'] = DoctorTime::all();
    }

}

// End of BaseController class
// End of BaseController.php file
