<?php

/**
 * Controller class AuthorizationController
 * 
 * @package		sss_eye_care
 * @since		v1.0.0
 * @version		1.0.0
 * @author		Tric Softech Solution
 */
class AuthorizationController extends BaseController {

    /**
     * Function used to load view file of application login file.
     * 
     * @access     public
     * @return     response
     * @since		v1.0.0
     */
    public function showLogin() {
        // show the form
        return View::make('signin');
    }

    //--------------------------------------------------------------------------

    /**
     * Handles request for validating user login 
     * credentials.
     *    
     * @access     public
     * @return     response
     * @since		v1.0.0
     */
    public function doLogin() {
        // validate the info, create rules for the inputs
        $rules = array(
            'username' => 'required',
            'password' => 'required|alphaNum|min:3'
        );

        // run the validation rules on the inputs from the form
        $validator = Validator::make(Input::all(), $rules);

        // if the validator fails, redirect back to the form
        if ($validator->fails()) {
            return Redirect::to('login')
                            ->withErrors($validator)
                            ->withInput(Input::except('password'));
        } else {

            // create our user data for the authentication
            $userdata = array(
                'username' => Input::get('username'),
                'password' => Input::get('password')
            );

            // check for posted user system access
            $checkUserName = User::where('username', '=', array($userdata['username']))->first();
            if (!empty($checkUserName)) {
                // attempt to do the login
                if (Auth::attempt($userdata)) {
                    // redirect home to logged in user
                    return Redirect::to('patient_details');
                } else {

                    // validation not successful, send back to form
                    Session::flash('loginError', "Your username/password is incorrect.");
                    return Redirect::to('login');
                }
            } else {
                // validation not successful, send back to form
                Session::flash('loginError', "Your username/password is incorrect.");
                return Redirect::to('login');
            }
        }
    }

    //--------------------------------------------------------------------------
    /**
     * Handles request for validating new user registration
     * credentials.
     *    
     * @access     public
     * @return     response
     * @since		v1.0.0
     */
    public function registerNewUser() {
        #default response status message
        $this->arrResponse['status'] = ERR_DEFAULT;

        // validate the info, create rules for the inputs
        $rules = array(
            'username' => 'unique:users'
        );

        // run the validation rules on the inputs from the form
        $validator = Validator::make(Input::all(), $rules);
        // print_r($validator);
        // if the validator fails, redirect back to the form
        if ($validator->fails()) {
            $this->arrResponse['status'] = VALIDATION_FAILED;
            $this->arrResponse['error'] = $validator->messages();
        } else {
            // validation passed
            $insertUser = User::insertNewUser(Input::all());
            if ($insertUser) {
                $this->arrResponse['status'] = SUCCESS;
                $this->arrResponse['message'] = "New User Registered Successfully !";
            } else {
                $this->arrResponse['status'] = ERR_DATABASE;
                $this->arrResponse['message'] = "Database error ! try again later";
            }
        }
        return Response::json($this->arrResponse);
    }

    //--------------------------------------------------------------------------

    /**
     * Handles request for logging out user from current session.
     * 
     * @access		public
     * @return		response
     * @since		1.0.0
     */
    public function doLogout() {
        Auth::logout();
        return Redirect::to('login')
                        ->with('flash_notice', 'You are successfully logged out.');
    }

}

//end of class AuthorizationController
//end of file AuthorizationController.php