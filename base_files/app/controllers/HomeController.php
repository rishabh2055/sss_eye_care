<?php

/**
 * HomeController class
 * Controller uesd for login & logout get And post functions.
 * @version      0.0.1
 * @since        0.0.1
 * @access       public
 */
class HomeController extends BaseController {

    /**
     * Displays the home page of the user.
     * 
     * @access		public
     * @return		response
     * @since		1.0.0
     */
    public function index() {
        $this->getDoctorTime();
        return View::make('index', $this->arrPageData);
    }

    //--------------------------------------------------------------------------
    /**
     * Display Signin page
     */
    public function signin() {
        $this->getDoctorTime();
        return View::make('signin', $this->arrPageData);
    }

    //--------------------------------------------------------------------------
    /**
     * Display Signup page
     */
    public function signup() {

        return View::make('signup');
    }

    //--------------------------------------------------------------------------
    /**
     * Display Contact page
     */
    public function contact() {
        $this->getDoctorTime();
        return View::make('contact', $this->arrPageData);
    }

    //--------------------------------------------------------------------------
    /**
     * Display About page
     */
    public function about() {
        $this->getDoctorTime();
        return View::make('about', $this->arrPageData);
    }

    //--------------------------------------------------------------------------
    /**
     * Display Services page
     */
    public function services() {
        $this->getDoctorTime();
        return View::make('services', $this->arrPageData);
    }

    //--------------------------------------------------------------------------
    /**
     * Display Photo Gallery page
     */
    public function photoGallery() {
        $this->getDoctorTime();
        return View::make('photo_gallery', $this->arrPageData);
    }

    //--------------------------------------------------------------------------
    /**
     * Display POur Tips page
     */
    public function ourTips() {
        $this->getDoctorTime();
        return View::make('our_tips', $this->arrPageData);
    }

    //--------------------------------------------------------------------------
    /**
     * Display home page
     */
    public function home() {

        return View::make('home');
    }

    //--------------------------------------------------------------------------
    /**
     * Function used to send email for contact us person details
     * 
     * @access     public
     * @since      v1.0.0
     * @return     response
     */
    public function sendMail() {
        // default status response
        $this->arrResponse['status'] = ERR_DEFAULT;
        // array to define all posted array data
        $arrPostedData = Input::all();
        $sendMail = Mail::send('emails.send_mail', array(
            'full_name' => $arrPostedData['full_name'],
            'user_email' => $arrPostedData['user_email'],
            'user_phone' => $arrPostedData['user_mobile'],
            'messages' => $arrPostedData['comment']
                ), function($message) {
            $message->to('sharaneyecare@gmail.com')->subject('Welcome to Sharan Eye Care!');
        });
        if ($sendMail) {
            // details returned from server
            $this->arrResponse['status'] = SUCCESS;
            $this->arrResponse['meeting'] = "Success !";
        } else {
            // database error
            $this->arrResponse['status'] = ERR_DATABASE;
            $this->arrResponse['message'] = "Database error ! try again later";
        }
        return Response::json($this->arrResponse);
    }

}

// End of HomeController class
// End of HomeController.php file