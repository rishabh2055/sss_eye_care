<?php
/**
 * AppointmentController class
 * Controller uesd for appointment get And post functions.
 * @version      0.0.1
 * @since        0.0.1
 * @access       public
 */
class AppointmentController extends BaseController {

    /**
     * Handle request to submit online appointment details into database 
     * 
     * @access         public
     * @since          v1.0.0
     * @return         response
     */
    public function submitAppointment() {
        // default status response
        $this->arrResponse['status'] = ERR_DEFAULT;
        // array to define all posted array data
        $arrPostedData = Input::all();
        if (Appointment::addNewOnlineAppointmentDetails($arrPostedData)) {
            // details returned from server
            $this->arrResponse['status'] = SUCCESS;
            $this->arrResponse['meeting'] = "Success !";
        } else {
            // database error
            $this->arrResponse['status'] = ERR_DATABASE;
            $this->arrResponse['message'] = "Database error ! try again later";
        }
        return Response::json($this->arrResponse);
    }
    /**
     * Handle request to send message to patient 
     * 
     * @access         public
     * @since          v1.0.0
     * @return         response
     */
    public function sendMessage() {
        // default status response
        $this->arrResponse['status'] = ERR_DEFAULT;
        // array to define all posted array data
        $arrPostedData = Input::all();
        $sendMessage = Appointment::sendMessageToPatient($arrPostedData);
        if ($sendMessage) {
            // details returned from server
            $this->arrResponse['status'] = $sendMessage;
        } else {
            // database error
            $this->arrResponse['status'] = ERR_DATABASE;
            $this->arrResponse['message'] = $sendMessage;
        }
        return Response::json($this->arrResponse);
    }
    
    //--------------------------------------------------------------------------
    /**
     * Function used to show online appointment page
     * 
     * @access      public
     * @since       v1.0.0
     * @return      Response
     */
    public function onlineAppointments(){
        //get appointment details
        $this->arrPageData['appointments'] = Appointment::getAppointments();
        return View::make('online_appointment', $this->arrPageData);
    }
    //--------------------------------------------------------------------------
    /**
     * Function used to show upcoming appointment page
     * 
     * @access      public
     * @since       v1.0.0
     * @return      Response
     */
    public function upcomingAppointments(){
        //get appointment details
        $this->arrPageData['appointments'] = Appointment::getUpcomingAppointments();
        return View::make('upcoming_appointments', $this->arrPageData);
    }
    
}

// End of AppointmentController class
// End of AppointmentController.php file