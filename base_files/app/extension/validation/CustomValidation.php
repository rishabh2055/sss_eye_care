<?php
namespace app\extension\validation;
/**
 * CustomValidtor class to extend the built in Validation class
 * for custom validator methods.
 * 
 * @access		public
 * @package		tms
 * @version		1.0.0
 * @since		v1.0.0
 * @author		Parth Shukla <shuklaparth@hotmail.com>
 */
 use \Illuminate\Validation\Validator;
 use \Illuminate\Database\DatabaseManager;
 class CustomValidator extends  Validator {
 	
	/**
	 * Checks whether the passed value exists in tbl_member_classes.
	 * 
	 * @access	public
	 * @param	$attribute
	 * @param	$value
	 * @param	$parameter
	 * @return	boolean
	 * @since	v1.0.0
	 */
	public function validateMember($attribute,$value,$parameter) {
		$dbResult = \DB::table('members')
					->join('member_classes','member_classes.mc_member_id','=','members.member_id')
					->where('members.member_email','=',$value)
					->where('members.member_active','=',1)
					->select('members.member_id')
					 ->first();
		if($dbResult && isset($dbResult->member_id)){
			return true;
		}
		return false;
	}
	
	//--------------------------------------------------------------------------
	
	/**
	 * Checks if the passed token for resetting the password
	 * is a valid token.
	 * 
	 * @access	public
	 * @param	$attribute
	 * @param	$value
	 * @param	$parameter
	 * @return	boolean
	 * @since	v1.0.0
	 */
	public function validatePasswordToken($attribute,$value,$parameter) {
		$dbResult = \DB::table('members')
						->where('member_verification','=',$value)
						->where('member_active','=',1)
						->select('member_id')
						->first();
		if($dbResult && isset($dbResult->member_id)) {
			return true;
		}
		return false;
	}
	
 }
//end of class CustomValidator
//end of file CustomValidator.php