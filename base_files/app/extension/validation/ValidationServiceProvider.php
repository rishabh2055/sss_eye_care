<?php
namespace app\extension\validation;
/**
 * 
 */
 use \Illuminate\Support\ServiceProvider;
 class ValidationServiceProvider extends ServiceProvider {
 	
	/**
	 * 
	 */
	public function register() {}
	
	//------------------------------------------------------------------------
	
	/**
	 * 
	 */	
	public function boot() {
		$this->app->validator->resolver(function($translator, $data, $rules, $messages){
			return new CustomValidator($translator, $data, $rules, $messages);
		});
	}	
 }
//end of class ValidationServiceProvider
//end of file ValidationServiceProvider.php